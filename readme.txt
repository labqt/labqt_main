This repository is a fresh copy of the original labqt repository with purged history.
See "labqt_original_history.log" for the commit history of the original labqt repository.
Some code has been moved to separate repositories, such as examples and drivers. These are submodules to this repository.

LV version was upgraded from LV 2011 to LV 2014 at 180320.