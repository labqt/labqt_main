﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="11008008">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">GrayFull</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">9341183</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16731648</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,I!!!*Q(C=T:1^DB."%%:L7#1)6E)G)12N[%V7KB3*R"&gt;9:"^AE\K#QZH1-&lt;""!3?Q@!-H3VYX1"&lt;C!B9JUMD$GZ\W\_\;#5BUOTQTXV&gt;&gt;`&gt;T&gt;(J(5HIM]F?:5'RZNS7_[G_\$U_:WO"7[N'&lt;4VVJ\VS5&gt;(4`='&gt;1-$]90]]"G[T=\`H(_5_X6U8&lt;I^`P(`(Z&gt;VH8`-2_X2&gt;L,W0L:0=D9_&amp;OXSTDUSTV`&gt;&gt;_PNR67KV8ZA*]T6KN(@4,KV6%`W`_@8_`Z&gt;4W&lt;T@&lt;8PSQX0D^TFNL_`J7&gt;P_-?\H`+'-[W^LXT5_[Z^XX;DPOAP^&gt;/(G`?!^8J0]'`&lt;3+&amp;N#]C)AAHD&amp;"JL3&lt;1!TX1!TX1!^X2(&gt;X2(&gt;X2(&gt;X1$&gt;X1$&gt;X1$6X2&amp;6X2&amp;6X2&amp;4VV&gt;%%8&gt;%&amp;H69,*AYG#IE'")"E5#&lt;Y#HI!HY!FY?*3!*_!*?!+?A)=5#8A#HI!HY!FY'#9"4]!4]!1]!1_F%EGEDAZ0Q%.Z=8A=(I@(Y8&amp;YG&amp;)=(A@!G=QJ\"1"1RT4O8&amp;Y("[(BVNR?"Q?B]@B=8CQR?&amp;R?"Q?B]@B95B;&amp;5]U&lt;5?(BT*C]"A]"I`"9`"17AQ?A]@A-8A-(K94A]@A-3#-#9XC))ARS%AQ(AQ?AY?,'$Q'D]&amp;D]"A]7'G(,+V-3^.W&gt;(A5(I6(Y6&amp;Y&amp;"Z+C-+D]#A]#I`#1VF2?"1?B5@B58C93B1?B5@B55#53:F?F',+1#6*%21?0OGU;.IF4S3;OEA_P*)0J?4$*PE134Y=ED&gt;&gt;]G:+XC4*CS^Z535PFO2&amp;E0TD*%.,BJ%]C?4"&lt;;)M_6Y1=W*+4)AR-3)'R!82;Y@_Z=4F=CG,R5,G]\F-JV/:4#9S(I^F."L*9$#1CYM,[@6[MGZP[&amp;ULVO_FCPP*^]O&lt;WZ@4[[^@,G]_P@^Q@@PZYX7LL`U5:X&gt;&amp;&gt;8Z66&amp;@H286XVM7P&amp;U66PSMI^&amp;D]L-LG&gt;`7;[\-@X[KCJ.&lt;&lt;9FX\`XAXSB0JLJMV_A/GF*;"!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"F?5F.31QU+!!.-6E.$4%*76Q!!&amp;5!!!!1?!!!!)!!!&amp;3!!!!!?!!!!!2F"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T!!!!!!#)%1#!#!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!)1?Y!K)&gt;6./J+&amp;XL24L06I!!!!-!!!!%!!!!!$K.UTJU+Q*3+4&gt;[V3'*ZQTV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!.K..!M7RXEOR`);+K?KSP1!!!"!&lt;F,R[D_%O3\&gt;*P-^,M;_L!!!!%)HWDTUM,&gt;&gt;1XOKW1!]\2Z9!!!!1-&amp;]M+_':.$$AA)'"Z&lt;Z(0A!!!,U!!5R71U-P17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=TJ"9X2V982P=F*Q9V.F=H:F=CZD&gt;'Q!!!!!!!)!!F:*4%)!!!!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!"!!!!!1!"!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!#!!)!!!!!!"Y!!!!3?*RD9'&gt;A&amp;G!19.2A9'!S9'$A9'!!!!5X!)Y!!!!!!%-!!!%E?*RD9-!%`Y%!3$%S-$"^!.)M;/*A'M;G.E!W&amp;ZM&gt;O/T&amp;)1Y59XI!J*G"G!GK"OI@JDN!`!:&gt;!TM71Q#8#CC2!!!!!$Y!!6:*2&amp;-P17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=TJ"9X2V982P=F*Q9V.F=H:F=CZD&gt;'Q!!!!!!!!!!Q!!!!!"$Q!!!&lt;BYH*.A:'$).,9Q#Q$3T!Q1E*S@EAKCFQ$&amp;H+&amp;CBA=/AWE9`T"-,:2O@M.C?+HZ#-P,R````Q@S(S0%OVV5/"IKZ"HY7]7!!M=&lt;8"AB5JUM+C`!3I[Q($9]!#+"H-VQ@4R!@4+&gt;,CIA`2*!N=?"RI"E8H##^@!=:EBG"+G4!=IL!.6*!'E6)#U#6O_C)A#E29!U4\=0E-X:T1!SKZ-(9G@L!@Z7E$E\'-''S2Q'#EQZQ,`N!)D^'?(O&lt;I\DDC)*)'=\;MQ!5QY?9-J&amp;AB(6"U$KU[``D06!7I?"E1&amp;E"C/$")-D5*E;E!7SBZW"#?QX2A97"B6'&lt;59D2ENI5/!(TPYOLMB]5,Q!!*%=6IM!!!!!$B%"A"5!!!9R-3YQ,D%!!!!!!!!-%1#!#!!!"$%R,D!!!!!!$B%"A"5!!!9R-3YQ,D%!!!!!!!!-%1#!#!!!"$%R,D!!!!!!$B%"A"5!!!9R-3YQ,D%!!!!!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9D&lt;+W'6%KJ2H*ML9:23EF'6GJ.2A!!!!@````_!!!!"A!!!!9!$Q!'!$$!"A$!-!9$!!Q'!Q!-"A0!0!9$]0Q'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!```BA(``_9!@`_'!"^`"A!&amp;`!9!!0!'!!!!"`````Q!!!A$`````````````````````^G:G:G:G:G:G:G:G:G:G&lt;`:G^G&lt;`&lt;`&lt;`:P&lt;W`W`W:G`W&lt;W^P:G^G^P&lt;W^P:P&lt;W:P^G``:P:P^P^G^P&lt;`&lt;`:G&lt;`:P&lt;W:P&lt;W&lt;W^G^G^G^P:G`W&lt;W^P^G`W^P:P:P^P&lt;W:P^G:G:G:G:G:G:G:G:G:G&lt;``````````````````````T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-T!$-T-T-T-T0`-T-T-T-T$]`Q-T-T-T-T`T-T-T-T$]:G&lt;`!T-T-T-`]T-T-T$]:G:G:P]$-T-T0`-T-T0]:G:G:G:G`T-T-T`T-T-TT':G:G:G:P]T-T-`]T-T-]`]:G:G:P`]-T-T0`-T-T00``RG:P```$-T-T`T-T-TT````0````QT-T-`]T-T-]`````````]-T-T0`-T-T00`````````$-T-T`T-T-TT`````````QT-T-`]T-T-]`````````]-T-T0`-T-T00`````````$-T-T`T-T-T````````````]T-`]T-T-TT0```````0```T0`-T-T-T00`````0```T-T`T-T-T-T-]``^P````-T-`]T-T-T-T-TRP````-T-T0`-T-T-T-T-T-```-T-T-T`T-T-T-T-T-T-T-T-T-T-``````````````````````Q!!"!$```````````````````````````````````````````^56&amp;256&amp;256&amp;256&amp;256&amp;256&amp;256&amp;256&amp;256&amp;256&amp;4``V2560^56&amp;4``V4``V4``V25`V4`60``60``6&amp;2560``6&amp;4`60^5`V2560^560^5`V4`60^5`V25`V4`6&amp;25``^560```V25`V25``^5``^560^5`V4``V4``V256&amp;4``V25`V4`6&amp;25`V4`6&amp;4`60^560^560^560^5`V2560``6&amp;4`60^5``^560``60^5`V25`V25``^5`V4`6&amp;25``^56&amp;256&amp;256&amp;256&amp;256&amp;256&amp;256&amp;256&amp;256&amp;256&amp;4`````````````````````````````````````````````&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8``]8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;SIK&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R@``R=8&amp;R=8&amp;R=8&amp;R=8&amp;SL]@`T]+B=8&amp;R=8&amp;R=8&amp;R=8&amp;```&amp;R=8&amp;R=8&amp;R=8&amp;SL]@V256&amp;4]`#I8&amp;R=8&amp;R=8&amp;R=8``]8&amp;R=8&amp;R=8&amp;SL]@V256&amp;256&amp;25`0QK&amp;R=8&amp;R=8&amp;R@``R=8&amp;R=8&amp;R@]@V256&amp;256&amp;256&amp;2560T]&amp;R=8&amp;R=8&amp;```&amp;R=8&amp;R=8&amp;X^`6&amp;256&amp;256&amp;256&amp;25`PQ8&amp;R=8&amp;R=8``]8&amp;R=8&amp;R=8@`T]@V256&amp;256&amp;25`P\_@R=8&amp;R=8&amp;R@``R=8&amp;R=8&amp;R&gt;``0T]`(^56&amp;25`P\_`PZ`&amp;R=8&amp;R=8&amp;```&amp;R=8&amp;R=8&amp;X`]`0T]`0R``0\_`P\_`H]8&amp;R=8&amp;R=8``]8&amp;R=8&amp;R=8@`T]`0T]`0T_`P\_`P\_@R=8&amp;R=8&amp;R@``R=8&amp;R=8&amp;R&gt;``0T]`0T]`0\_`P\_`PZ`&amp;R=8&amp;R=8&amp;```&amp;R=8&amp;R=8&amp;X`]`0T]`0T]`P\_`P\_`H]8&amp;R=8&amp;R=8``]8&amp;R=8&amp;R=8@`T]`0T]`0T_`P\_`P\_@R=8&amp;R=8&amp;R@``R=8&amp;R=8&amp;R&gt;``0T]`0T]`0\_`P\_`PZ`&amp;R=8&amp;R=8&amp;```&amp;R=8&amp;R=8&amp;`T]`0T]`0T]`P\_`P\_`0SML+Q8&amp;R=8``]8&amp;R=8&amp;R=8&amp;X^``0T]`0T_`P\_`0R`L+SML+SM&amp;R@``R=8&amp;R=8&amp;R=8&amp;R&gt;``0T]`0\_`0R`L+SML+SM&amp;R=8&amp;```&amp;R=8&amp;R=8&amp;R=8&amp;R=8@`T]`0R5L+SML+SML"=8&amp;R=8``]8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;X^5L+SML+SML"=8&amp;R=8&amp;R@``R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8L+SML"=8&amp;R=8&amp;R=8&amp;```&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8````````````````````````````````````````````!!!!N1!"2F")5#^"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T/E&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,G.U&lt;!!!!!!!!1!#6%2$1Q!!!!!!!2209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!"16%AQ!!!!'Q!"!!-!!"209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!!!!)!!!!!!!%!!1!!!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!"*5&amp;2)-!!!!!!!!!!!!!-!!!!!!!0S!!!+-(C=L6:,;".2&amp;,VPH-IE7JVJV4:A39S47D16]7_R@NJ2;3X6%B56Q19H7C&amp;;3:/IC$]9B+I&amp;M&gt;+&amp;5&amp;!8X&lt;LI1J=CM1KT[5)%0R$%F1N2%$`54M&lt;\:D+:3&gt;*/.EVA?!TPX00O0?=]"M!&lt;ZOO:(&amp;T4A0#`=.'FA6&gt;7#5#GG90]DT^-`A&amp;:YC-;\/)/]R_:(&amp;GOQ5*:&lt;?47390Q!\@KD`5QP#6^`'@=/J`X937P"INFN5\I&amp;,/]_(3Z/&amp;BFF23AA2]C/?;I[0`$$3H^S!:+%XU+T31(2&amp;L"MELQ902]4"(J7U]TZT.+?D4A*&lt;5[)7:8957E@GG5:.;1),0&gt;+AF9MAH'R]&gt;NE'##'IVD&lt;%5-#1+1$=Q;&amp;US.J$9-C.GQA@%;'/4:90&amp;)`NQQ04M&amp;F5*L*:6(+/)OZ6OG7!OH\:I2V[@"%D(&lt;RPGYT`+(KOI?Z2E1)*G4H0Z=4V/]U%V6-0&lt;7I"#B&amp;M*(="X29,7C-LX!7O0NB7&amp;$"N;394P+109;-HCE12ZM(99L[##RG7"$?TQVE)QF!PWH![@CU9'"Q)8%W81U'1P)U73U8+&amp;73@6MI&gt;V4-M-=M"29/!%8H.0OB\'R-2Q!0GXI$I47C&gt;E#4D#[329G,NO4I[TWZ(&lt;CZ+3`B]\1[96;')&gt;B/WT$LE@$#HTJG9I-OWHO$&lt;M:@83NR,!1BXVQV=6]7US1&lt;6D=$X!5YC[9L9CZ\D1M9O+)O6L:M.P+$%OR*99&gt;'2EJ.7R,Q&lt;!M);:BF5F^7J_GNPWJ@]5"XX(96E9Z2$6!LYPP3,%:+?DUZVH40W$Z6(LK\SQW;I`\`!-MG':&gt;N0N5-B6.^C=CM51[FCC2!Y^]U("+^@\]'UR0QES0F06X5L\;@0.&amp;#5J48X18*5D`"K$D^#I;1QR#O3MOFI?%R@][[+M9EEMF);'`D&lt;:;MY@E#,9?/A;BYW"(::YD+P@MK.QMCIJVMK+I+(-@F6MT257'-+2=&lt;$^9&amp;J5QIFJ"&gt;M(=,IN+'(F;,2[XK.QJDUL9YH+,SNX#]'=.T"@^.[S%]Y\!^+!IS'@9\;M_D8&gt;7&lt;\%)^[E)-^\M;SM%BME(JEJ+H4NXO53,"2J%*.7(]6ZM&amp;O:IY8KUA1B?\/`^BU`98_L7)]&amp;PJM4M`M]&lt;CM_P8TP7%Y\V+]\H]&gt;M]_US?N-HTQLK:GQQ?:*A4(JT5-0+M&amp;\-,()'L-XB[,?VUQ&gt;"O;GI+/@&amp;J4EJ:!4@1M3*^Y`4K![TH4'&amp;NM2&amp;G4_&amp;::QJ@B;YQ:B":0'[XQ\,=RYF5Y10-X&amp;,FOC5C3LL&lt;"HY0]F`89+H58P/#\[!?YTKY0@QI*B\$PYQ&lt;Z8ZFXB5_Z$*&gt;";1S3:[Q&lt;?*$0V@`([TAZDU!!!!!!!1!!!!_!!!!0A!"1E2)5#^"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T/E&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,G.U&lt;!!!!!!!!!!$!!!!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!$3Q!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S%1#!#!!!!!%!#!!Q`````Q!"!!!!!!#,!!!!!Q!/1$$`````"52V&lt;7VZ!&amp;-!]1!!!!!!!!!#'5&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!(%"1!!%!!!Z"9X2V982P=F.F=H:F=A!!)E"1!!%!!2F"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T!!%!!A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)2!)!)!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!)!!!!!!!!!!1!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ%1#!#!!!!!%!"1!(!!!"!!$*QG];!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N="%!A!A!!!!"!!5!"Q!!!1!!S=*P'A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-2!)!)!!!!!1!)!$$`````!!%!!!!!!)M!!!!$!!Z!-0````]&amp;2(6N&lt;8E!5Q$R!!!!!!!!!!):17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!=1&amp;!!!1!!$E&amp;D&gt;(6B&gt;'^S5W6S&gt;G6S!!!C1&amp;!!!1!"'5&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,GRW9WRB=X-!!1!#!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G52!)!)!!!!!1!&amp;!!-!!!%!!!!!!!1!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B%1#!#!!!!!-!$E!Q`````Q6%&gt;7VN?1"4!0%!!!!!!!!!!BF"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"R!5!!"!!!/17.U&gt;7&amp;U&lt;X*4:8*W:8)!!#*!5!!"!!%:17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=Q!"!!)!!!!!!!!!!!!!!!!%!!5!#A!!!!1!!!#5!!!!+!!!!!)!!!1!!!!!$A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%\!!!#&gt;XC=H6&amp;,4M-Q%(W*US]B&amp;'CB+58+CA7,#IE,J#JC3^6?A(Q=6*1KF?.%M/.M\$B/4Q!4/YB&amp;":8QEU??M7@GT4/!-2T`ZJ.7Y[Z9LV_RR":[G?YUEE5A-\(92%MO3CYG;2GF1:\X(]*H(MGJF')6&amp;J,HEUCG'0NT')$TH;:TA"$&lt;D`&gt;M^(@*Q?Z.6@.;V41O:WG23S[],0(5=W]D6G5AO2=(-E#$A#ZC[G#"E=.K&amp;\@EN0&amp;)*ZMC=ZBIII8B,USP6,STJVP(&lt;Y%F[20;`BP,&amp;F)(?*;A2`E-"^4LE"6R1JW6N*D^8V1&lt;&amp;;5DW)J;D]G8O"\%Q4%.:Y*-MT)UE*\5QDU^](#+0L'J-&amp;!+;&lt;!;OZ'@GT-&amp;L;#"]VK8@&lt;^AI7+AF#6S"F'TS/`C"%/Y'.&amp;W=6&amp;6`!*JP9?$!!!!!'5!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$9!.5!!!"2!!]%!!!!!!]!W!$6!!!!7A!0"!!!!!!0!.A!V1!!!'/!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"6!!!!%(A!!!#!!!"5A!!!!!!!!!!!!!!!A!!!!.!!!"!A!!!!&lt;4%F#4A!!!!!!!!&amp;54&amp;:45A!!!!!!!!&amp;I5F242Q!!!!!!!!&amp;]4U*42Q!!!!!!!!'11U.42Q!!!!!!!!'E4%FW;1!!!!!!!!'Y1U^/5!!!!!!!!!(-6%UY-!!!!!!!!!(A2%:%5Q!!!!!!!!(U4%FE=Q!!!!!!!!))6EF$2!!!!!!!!!)=&gt;G6S=Q!!!!1!!!)Q2U.15A!!!!!!!!+535.04A!!!!!!!!+I;7.M.!!!!!!!!!+];7.M/!!!!!!!!!,14%FG=!!!!!!!!!,E2F")9A!!!!!!!!,Y2F"421!!!!!!!!--4%FC:!!!!!!!!!-A1E2)9A!!!!!!!!-U1E2421!!!!!!!!.)6EF55Q!!!!!!!!.=2&amp;2)5!!!!!!!!!.Q466*2!!!!!!!!!/%3%F46!!!!!!!!!/96E.55!!!!!!!!!/M2F2"1A!!!!!!!!0!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!E!!!!!!!!!!$`````!!!!!!!!!,!!!!!!!!!!!0````]!!!!!!!!!R!!!!!!!!!!!`````Q!!!!!!!!$9!!!!!!!!!!$`````!!!!!!!!!/Q!!!!!!!!!!0````]!!!!!!!!"M!!!!!!!!!!!`````Q!!!!!!!!'Y!!!!!!!!!!$`````!!!!!!!!!&gt;Q!!!!!!!!!!0````]!!!!!!!!#*!!!!!!!!!!!`````Q!!!!!!!!*I!!!!!!!!!!4`````!!!!!!!!!XQ!!!!!!!!!"`````]!!!!!!!!$E!!!!!!!!!!)`````Q!!!!!!!!/A!!!!!!!!!!H`````!!!!!!!!!\1!!!!!!!!!#P````]!!!!!!!!$R!!!!!!!!!!!`````Q!!!!!!!!09!!!!!!!!!!$`````!!!!!!!!!_Q!!!!!!!!!!0````]!!!!!!!!%=!!!!!!!!!!!`````Q!!!!!!!!:U!!!!!!!!!!$`````!!!!!!!!#HA!!!!!!!!!!0````]!!!!!!!!,.!!!!!!!!!!!`````Q!!!!!!!!]M!!!!!!!!!!$`````!!!!!!!!$T1!!!!!!!!!!0````]!!!!!!!!0?!!!!!!!!!!!`````Q!!!!!!!!`A!!!!!!!!!!$`````!!!!!!!!$_A!!!!!!!!!!0````]!!!!!!!!4/!!!!!!!!!!!`````Q!!!!!!!".!!!!!!!!!!!$`````!!!!!!!!%UA!!!!!!!!!!0````]!!!!!!!!4&gt;!!!!!!!!!#!`````Q!!!!!!!"3U!!!!!"6"9X2V982P=F*Q9V.F=H:F=CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2F"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!"!!%!!!!!!!!!!!!!!Q!/1$$`````"52V&lt;7VZ!&amp;-!]1!!!!!!!!!#'5&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!(%"1!!%!!!Z"9X2V982P=F.F=H:F=A!!9A$RS=*P'A!!!!):17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=R6"9X2V982P=F*Q9V.F=H:F=CZD&gt;'Q!+E"1!!%!!2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!)!!!!"`````A!!!!!!!!!"%6*Q9V.F=H:F=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"!!A!!!!!!!!!!!!!!#!!!!%E6D;'^4:8*W:8)O&lt;(:D&lt;'&amp;T=Q!!!":"9X2V982P=F.F=H:F=CZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""!!!!!2&amp;3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=V"53$!!!!!D!!%!"1!!!!F3='.4:8*W:8)25H"D5W6S&gt;G6S,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="ActuatorRpcServer.ctl" Type="Class Private Data" URL="ActuatorRpcServer.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
	</Item>
	<Item Name="overrides" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="Whack_Impl.vi" Type="VI" URL="../Whack_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;4!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&lt;'5&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,GRW9WRB=X-!%E&amp;D&gt;(6B&gt;'^S5W6S&gt;G6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!21!I!#E2F9W&amp;Z+#5P=SE!!"&gt;!#A!15'6S=WFT&gt;#BT:7.P&lt;G2T+1!!.E"Q!"Y!!"M:17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=Q!217.U&gt;7&amp;U&lt;X*4:8*W:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!)!!E#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
		</Item>
		<Item Name="SetActuatorSpeed_Impl.vi" Type="VI" URL="../SetActuatorSpeed_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!&lt;'5&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,GRW9WRB=X-!#F.F=H:F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%U!+!!V"9X2V982P=F.Q:76E!"&amp;!!Q!+17.U&gt;7&amp;U&lt;X**:!!!,E"Q!"Y!!"M:17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=Q!*5W6S&gt;G6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!#!!!!!A!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
		</Item>
	</Item>
	<Item Name="overridden" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="ActuatorRpcServer_Create.vi" Type="VI" URL="../ActuatorRpcServer_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!()!!!!$1!W1(!!(A!!'RF"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T!"&amp;"9X2V982P=F.F=H:F=C"J&lt;A!I1(!!(A!!&amp;R6&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X-!"F"B=G6O&gt;!!!&amp;%!Q`````QN4:8*W;7.F4G&amp;N:1!21!=!#U*J&lt;G2":'2S:8.T!!N!"A!%5'^S&gt;!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"1!'!!=4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!%!!!!/%"Q!"Y!!"M:17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=Q!317.U&gt;7&amp;U&lt;X*4:8*W:8)A&lt;X6U!!!71&amp;!!!Q!&amp;!!9!"QFF=H*P=C"P&gt;81!G1$Q!"1!!!!"!!)!!Q!%!!A!#1!*!!E!#1!*!!E!#1!*!!I!#1!*!!E!#1!,!Q!"%!!!%A!!!!I!!!!)!!!!#!!!!!A!!!!+!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!!U&amp;!"5!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
	</Item>
	<Item Name="interfaces" Type="Folder">
		<Item Name="Whack.vi" Type="VI" URL="../Whack.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&lt;'5&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,GRW9WRB=X-!%E&amp;D&gt;(6B&gt;'^S5W6S&gt;G6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!61!I!$U2F9W&amp;Z5G&amp;U;7]I*3^T+1!81!I!%52F9W&amp;Z5X2B=H25;7VF+(-J!$:!=!!?!!!&lt;'5&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,GRW9WRB=X-!%5&amp;D&gt;(6B&gt;'^S5W6S&gt;G6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!A!!!#3!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="SetActuatorSpeed.vi" Type="VI" URL="../SetActuatorSpeed.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;P!!!!#Q!O1(!!(A!!'RF"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T!!F4:8*W:8)A;7Y!%5!$!!J"9X2V982P=EFE!!!41!I!$5&amp;D&gt;(6B&gt;'^S5X"F:71!"!!!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!1!"1!'%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!-%"Q!"Y!!"M:17.U&gt;7&amp;U&lt;X*3='.4:8*W:8)O&lt;(:D&lt;'&amp;T=Q!+5W6S&gt;G6S)'^V&gt;!!!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!)1!]!!5!!!!!1!#!!-!!Q!(!!-!!Q!$!!-!!Q!$!!-!!Q!)!!-!!Q!$!!-!#1-!!2!!!*)!!!!)!!!!#!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!."1!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%L!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!&lt;'5&amp;D&gt;(6B&gt;'^S5H"D5W6S&gt;G6S,GRW9WRB=X-!%E&amp;D&gt;(6B&gt;'^S5W6S&gt;G6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!W1(!!(A!!'RF"9X2V982P=F*Q9V.F=H:F=CZM&gt;G.M98.T!"&amp;"9X2V982P=F.F=H:F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118306832</Property>
		</Item>
	</Item>
</LVClass>
