﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="11008008">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">4013373</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">65421</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,&amp;!!!*Q(C=T:1^DB."%)7@%&gt;+3_A3,((#"3AF^"6_BLO"Q(@I)&amp;")CNU9&lt;EDB!Z#6R!AN)1.L!J#":9\ZJ^XL^M\9*1.LO,5`\P?LK&lt;\P&lt;)Z8W1HKO^;7W/NM[8W+AT60&lt;&lt;ZN"]5`UX@HA\-T8:HAU`X$]K+_K&lt;/?@Z\`50JRNB`ZK&gt;=Z@N5X&lt;LE\ZO"X38M;$8^W$D+X`Y'YS$PXGEN`OVG_&lt;2`TWHO_EX\&lt;V=&gt;*PH[L@(L3G;@&lt;XPWHWX;:E\*Z@=_1?HH`*7/X92`?HW8/0`1+S&gt;2`V^^L&amp;[]V0&lt;8,Z2`"`G^24^S)CEAD##6.HL2-^U2-^U2-^U1-^U!-^U!-^U"X&gt;U2X&gt;U2X&gt;U1X&gt;U!X&gt;U!X&gt;U%N(&amp;\L1B=[O*)MH#S6&amp;EQ**-CB+0B+?B#@B38DYKI1HY5FY%J[%BR1F0!F0QJ0Q*$R-5]+4]#1]#5`#1[F#EK7DQZ0Q5&amp;Y"4]!4]!1]!1^,+O!*!),&amp;AM*"%4!5G-%AY!FY!B['#HA#HI!HY!FYM"8Q"$Q"4]!4]$#F\%I5GK[DQU-:/4Q/D]0D]$A]F*&lt;$Y`!Y0![0Q].S=HA=(A@#7&gt;!J$I+=35[#]]8B=8BYS/&amp;R?"Q?B]@BQ3IHZ'6H/JKOI].D]"A]"I`"9`"11A;0Q70Q'$Q'$W6F]"A]"I`"9`#QF!Q?A]@A-5#-26F?2D&amp;DIJ&amp;E#!90@_7W7$GF+#27OF1PL_KF6,VMKJ&gt;)^8+I(LLK9;I?EOLGKW[K[G;J&lt;I,K0[=+L1KDOIDKZ#Z23TY8R*S9%6.C4)S))4%A_NX5@ZSY8#[V7#QUH]]VG]UUH5YV(I]V'IUU(!YV'!T5\`&gt;VXV\3.[WX_V\[Y?]_X]VG(\_`?P0F\H&lt;W]_\W\@N\&lt;U,OJ/N88XO4[V_^S5X&lt;9_,@RL@*T@LXZ*LHV@L4J)?GV\WOZO;^^"4?D8KGT8/\2X]!D/DO/!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.4</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#!G5F.31QU+!!.-6E.$4%*76Q!!'`!!!!1W!!!!)!!!'^!!!!!7!!!!!2&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=Q!!!!!!C"%!A!A!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!#$D@`Q9KSY2J';T!*"D)1M!!!!$!!!!"!!!!!!#Z4!&amp;P@?1UO@R&amp;$@92RMI^1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!,PN#I&lt;=4\6#L9C.^W9S[\1!!!!1%+;I0U_40B'L;,=MS4Q'Y1!!!"$+:V1I@_/!O"NC09T+&amp;WZY!!!!%$"@,#PBG41QY)#"A?7_2TY!!!%H!!&amp;-6E.$(V*Q9U.M;76O&gt;#ZM&gt;G.M98.T/F*Q9U.M;76O&gt;#ZD&gt;'Q!!!!!!!-!!F:*4%)!!!!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!"!!!!!1!"!!!!!!!%!!!!!!!!!!!!!!!!!!*736"*!!!!!!!!!B24&lt;W.L:824&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q)(!&amp;"53$!!!!!U!!%!"Q!!!!!*35^4&gt;(*F97VT$&amp;.P9WNF&gt;&amp;.U=G6B&lt;224&lt;W.L:824&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!!!!!!1!!!!!!!Q!"!!!!!!9!!!!!!!!!!Q!!!!!#!!)!!!!!!#5!!!!A?*RDY'.A&amp;G!19&amp;2A%'"A9"*A-'"AY)"!!2"E!!!1C!%&amp;!!!!!!!!&lt;A!!!7&gt;YH'.AQ!4`A1")-4)Q-0-";49U=4!.9V-&lt;)*O,T1Z=^G)2:R14#=Z0TEYN#3YJ3EX-V=MJ3]Z*,#Z'5=.J;'3O:Q#%BOI#G&amp;S)S1Q-4"_!.!M1-U'N%92)-&gt;U"YD`I4O'(UBZ)9A#4`42!!!!!!!!O!!&amp;73524(V*Q9U.M;76O&gt;#ZM&gt;G.M98.T/F*Q9U.M;76O&gt;#ZD&gt;'Q!!!!!!!!!!Q!!!!!"L1!!!_"YH,74-5D$1"3'\Z+$.BB)5#E&gt;#AI'%8%Q#+6&gt;J+5J:#AIO$E*;M(*N?"3C!845(!18!526U%IDJ%'+OCC4EY/83JU=T?_&gt;UF+7[KI9%DY\NX\\\`E0[++B/SN:.,L1,DZN&lt;W`MYN]A)F#/+?\,=[I&lt;I6]$GHV:-@16+&gt;[4!GR$3U/61L)A%G&lt;;&gt;U*&amp;(ESLDM85:_#TCQIEE!./-W6[!&amp;DI/S5."5&gt;&lt;&amp;HLYB9V6[F&gt;I!NIU&amp;;W8"&lt;J('F5?4#I&lt;*D-,A8+MC/6&lt;:.B3QB-B+^.UD]U;:B#V+J,2[;AOZ;8;GXQ8*D_:(HM\@\$^[(O^0.#TXCV-E/57A)GWF7$"CV-CUM]VE)DBJF&gt;$_5]G'Q&lt;&lt;,$4F;+%/_SP_:[YSG%-BEK4@]#Y4X=E*]&gt;_N[Q-+XBK74J//JIA\C$]WQ\VH)$+$"=W\WYQ&gt;+6YC^)JWD]8*^\/4V?A&lt;O=8LDBS7RS'+A[@%'$SX;=^HS\D=QHV#[(E%&lt;A*4.#!J]!F9%QAZ"7Y#K1E3=Z#:M&amp;W(E&lt;Y/D%C],/GB*'5/#=OCHLU;XZ\&amp;&gt;;-YG#.`_]H\=\I"!!!!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$"%!A!A!!!1R-3YQ!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$"%!A!A!!!1R-3YQ!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!```````````H:JX0[KKKP_9GG.`KLKLP[K[;H```````````A!!!!9!"A!'!"_!"A"`Y!9"``A'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``Y9"```G!(``BA!@`Q9!"`Q'!!$Q"A!!!!@````]!!!)!````````````````````````````````````````````]!``$`!0]0!0`Q``!0````$Q]0$Q]0$Q]0$Q]0`````Q$`!!]!`Q]!`Q!0]0````]0$Q]0$`]0$Q]0$`]0````$Q]0$Q``$Q$`$Q]!``````````````````````````````````````````````_)C)C)C)C)C)C)C)C)C)D`C)C)C)C)C0_)C)C)C)C)`YC)C)C)C0```YC)C)C)C0_)C)C)C0``````C)C)C)D`C)C)C0````````_)C)C)`YC)C)``````````_)C)C0_)C)C0``````````C)C)D`C)C)D``````````YC)C)`YC)C)``````````_)C)C0_)C)C0``````````C)C)D`C)C)D``````````YC)C)`YC)C)``````````_)C)C0_)C)C0``````````C)C)D`C)C)D``````````YC)C)`YC)C)``````````_)C)C0_)C)C0````````````C)D`C)C)C0````````````_)`YC)C)C)``````````_)C0_)C)C)C)D````````YC)D`C)C)C)C)C0`````YC)C)`YC)C)C)C)C)D``YC)C)C0_)C)C)C)C)C)C)C)C)C)D`````````````````````]!!!1!`````````````````````````````````````````````0T]`0T]`0T]`0T]`0T]`0T]`0T]`0T]`0T]`0T]```]`!!!`0T]!0T]!!$]`!$]!!$]`0Q!`0T]!!$]`0T```T]!0Q!`!$]!0Q!`!$]!0Q!`!$]!0Q!`!$]`0T]`0```0Q!!0T]!!!!`!!!`0Q!`!!!`0Q!!!$]`!$]`0T]```]`!$]!0Q!`!$]!0T]`!$]!0Q!`!$]!0T]`!$]`0T```T]!0Q!`!$]!0Q!`0T]!0Q!!0T]!0Q!`!!!`0T]`0```0T]`0T]`0T]`0T]`0T]`0T]`0T]`0T]`0T]`0T]`````````````````````````````````````````````\;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNP``NL;WNL;WNL;WNL;WNL&lt;]`,;WNL;WNL;WNL;WNL;W``_WNL;WNL;WNL;WNL&lt;]`KT_`PSWNL;WNL;WNL;WNL&lt;``\;WNL;WNL;WNL&lt;]`KT]`0T]`P\]NL;WNL;WNL;WNP``NL;WNL;WNL&lt;]`KT]`0T]`0T]`0\_`,;WNL;WNL;W``_WNL;WNL;W`KT]`0T]`0T]`0T]`0T_`L;WNL;WNL&lt;``\;WNL;WNL;ML0T]`0T]`0T]`0T]`0\_NL;WNL;WNP``NL;WNL;WNKT_`KT]`0T]`0T]`0\_`KSWNL;WNL;W``_WNL;WNL;WL0\_`P[M`0T]`0\_`P\_L,;WNL;WNL&lt;``\;WNL;WNL;M`P\_`P\_L0\_`P\_`P[MNL;WNL;WNP``NL;WNL;WNKT_`P\_`P\_`P\_`P\_`KSWNL;WNL;W``_WNL;WNL;WL0\_`P\_`P\_`P\_`P\_L,;WNL;WNL&lt;``\;WNL;WNL;M`P\_`P\_`P\_`P\_`P[MNL;WNL;WNP``NL;WNL;WNKT_`P\_`P\_`P\_`P\_`KSWNL;WNL;W``_WNL;WNL;WL0\_`P\_`P\_`P\_`P\_L,;WNL;WNL&lt;``\;WNL;WNL&lt;_`P\_`P\_`P\_`P\_`P\_L+SMNL;WNP``NL;WNL;WNL;ML0\_`P\_`P\_`P\_L+SML+SML,;W``_WNL;WNL;WNL;WL0\_`P\_`P\_L+SML+SML,;WNL&lt;``\;WNL;WNL;WNL;WNKT_`P\_`+SML+SML+SWNL;WNP``NL;WNL;WNL;WNL;WNL;M`+SML+SML+SWNL;WNL;W``_WNL;WNL;WNL;WNL;WNL;WNKSML+SWNL;WNL;WNL&lt;``\;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNL;WNP```````````````````````````````````````````Q!!!!)!!A!!!!!"LA!"2F")5"^3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=TJ3='.$&lt;'FF&lt;H1O9X2M!!!!!!!$!!*'5&amp;"*!!!!!!!#&amp;&amp;.P9WNF&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!A=!5&amp;2)-!!!!$1!!1!(!!!!!!F*4V.U=G6B&lt;8--5W^D;W6U5X2S:7&amp;N&amp;&amp;.P9WNF&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!!!!!!!"!!!!!!!$!!%!!!!!"A!!!!!!!!!!!!!!!!%!!!$5!!*52%.$!!!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!!!!!!!1!"!!!!!!!%!!!!!!!!!!!!!!!!!!!!!1!!!"J16%AQ!!!!!!!!!!!!!E2%5%E!!!!!!!!#&amp;&amp;.P9WNF&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!A=!5&amp;2)-!!!!$1!!1!(!!!!!!F*4V.U=G6B&lt;8--5W^D;W6U5X2S:7&amp;N&amp;&amp;.P9WNF&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!!!!!!!"!!!!!!!$!!%!!!!!"A!!!!!!!!!!!!!!!!%!!!!K!!-!!!!!"I1!!"77?*T.7&amp;VM&amp;&amp;550H=[,&lt;/\,=S7&lt;K'7MD_:85CBN5!MU0$&lt;$C)`]F?)SI/S\3Q7+3T:&lt;BM41]&amp;E*2,#CS1]G*$I3U0C!Q]];#)0C!5FYQ]3AAL%"2Z16+)'%!,-DO@?W&gt;G&gt;W&gt;XO6A2EG^R-._@=\^TP@._:G18QP#&gt;/Z&amp;)QK!%2&lt;_0&amp;3AW=CEI!BJM%3(`%$?1_E*I[IM%C99.YE5O232J5+GJ);*(XQJ]9KH_I4Y&gt;TJ%?]AK%69BXOZ.2AH+*/=#_8EK*U:*+UN^T=UAU.YH[3YF[7P(?%`9EIIE&amp;C+FX&gt;431&amp;20&lt;T@#+Q*LQ^EJ$INYYGI9ZN[&gt;"!F.7KG*3=ADMC^!GW*4?.",BZZJ;!7U[&amp;IU?0:J0=2F+)F4%(=UA!A-TCJB8*K:&lt;6BDYJ/:XF/&amp;E/YMQS=72P[A#NH3&lt;FJI[86-T%N$@4*[;J:JKWK'";DQ9V5L*&gt;K"/O+"@+K^9G0A9#*(&amp;'PYKN?%X10^%(S"ZOGHM6&lt;18,K-:O".O)W)H8H2IU*F2O%`!GYA)YQ(L"G\W9B\UAT\.?//3^)G3&lt;=;"%-W2_/.$1U&gt;P@&amp;Y`%@.(.PO\?=&amp;_@&lt;U&gt;MSU!Y(P%JY8AYPUXT:&gt;5RGX*!Q:B#Q!-];9-&gt;6MKD-$1UB$4AGEV&gt;A+E4J'1GT]V/%]`1LG4ZI[B:`B9C@`,&gt;^;^4$I.NH%7VTW26/R.6[R:T;\+J^LF(L^J7&amp;..ADGKBFQ2B:R%&amp;TD;3,+I.)B7NU&amp;ME:Q\G\,+J.IAYL3:/-&gt;8/F65R2\:"%SMDWY-($_&lt;+NCUD7Z[1D'Q@[!_I&lt;$`6LZ-9\,0)6M&amp;W3+K0TIQ`%+)')3D\:3&lt;\0F/H]B(P=LN1.YZ#K/,KLD=CX@(&amp;]8BM3V&gt;`0.+8UQQMW-^U5L5C`1V[ZT0$/X,3OZSCD5]@X?;@T[EK6NH]I^]!U*'\EG6*!=D8R(#_28C5YVDI+7G2ETE7I&lt;WKTP:K:)MAL_X"6S#Y%&lt;*'+7.'K7"'/=@73WS^RN;`W(K@LK1C;[04.BO:&gt;&gt;NMJ$Z['XV6S%9+=5*`%5N]H7=D*^*6$UK2H'`S&lt;/2%H(I4JZC.PMWXE&gt;0%+G;D-ZH7D'CGD`3@S6T9S=REU,Q;GU*2TOI0Y#9MM#G7#,1"3VE$0(9DN2@PA+]#$$-Z/[,&lt;N[/&lt;)ML#H':5;M3")KS7EBZ4",61"J/B(E]X?18O2V?XV[C(J-J]QVY$89#*#2],]&gt;.6$+1*83I%[.\ZI9J/1X%N(@L@&gt;X6YMQ&gt;U'A:^C&gt;&amp;HH"%H!S#!V6S&amp;TDA71XQMR%^$-B"+@D8`1WDGD$U;=6E'QDK0-2$Y0;^3EN*";^$^CFI"\@4]BV&amp;H:W%:U^E9U_A;V6H?EY5,&gt;@&lt;C;(7W,L)N'I_MC=&lt;C/4K\L%&amp;+58H(4#HJN$V,D)-1T$&gt;&gt;J&lt;O:K_\&gt;OY?[Q^89(`O\G_=3%PX'U:3)"ZN&lt;.G?[#\KMVL6+39\NWM++&amp;B&amp;Z%F4C4M?/(=/&gt;='8&gt;Z?TE6G+)DY8Y[:IBNTO`$U9I&gt;&lt;O@LP:1K^9!K]%TFFGKG9TZD1^@4&gt;@IK_GS6?03#-&amp;K],9_TGDI]3TD(KTGRQO8M*L_02_Q;MIJ".XF&lt;IM"R[[@N6QX7[[&lt;MN&gt;XPL.A/D4#);&lt;^M=_&amp;RV^=_J[WTSJBFS(B]J!01HY^&amp;$$DVO+&gt;$!=T)X9128Q9.NHP6D=,CJA/SYZ`*_+?;&amp;_OC&amp;U;X,)R+BDTEI=!/0-9Z4--@7FB[QP,^3H,^5F&lt;Z_#WA4.A[&gt;RYR!ER(%2Y*$B)V^_'8FXWZXP%W64=EZ1OZEP?^'8WTHEH4Q(5!%JJ"?SW0N7=$,\&amp;'3+IS%[Q;ZE*NB[&lt;0YC4QT&lt;"@BFRACU;:@-&gt;+[0&gt;Y&gt;\#!_T[YRFAPTZ.!QR_?ZI''0T_Z!=9X(D9!4:19)#.+4$!,JE$L"%VP$ZXA0UUYA"L,;&amp;BTK&lt;BQP-L_94GV_5H.,_O0)&lt;Z&gt;@6BZ^@7QP.,-#))OOO=L0L9'_&gt;V&lt;(UD,,7`GJY@];?&gt;&amp;;-=8Z7&gt;U?[NE8BH0"9*&lt;]N`\`J?6I5:5D+-_[@@Z"K1L0EA&amp;8G&amp;_M&amp;Y?H;94#S'&amp;J/*7S_-^".9W-*%V&gt;M'$1\MX#I,E($R6,^!8YHIHR(C@+@G^)U4\R9+B02([*2E`&gt;4'$PI_6FND07ZT\Q$\21OM(]?-G&lt;/&lt;7`"PRB1R^^^C-/)30-AO$4RS2`6R=2H^V5.9*CQ2$W%@]4W[6DAEX"Y_H`GZ&gt;(BF*D.RBN2S^&gt;,\8G(C0XP-#&gt;I!!!!%!!!!;1!!!#Y!!5*%3&amp;!@5H"D1WRJ:7ZU,GRW9WRB=X-[5H"D1WRJ:7ZU,G.U&lt;!!!!!!!!!!$!!!!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!%^!!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S%1#!#!!!!!%!#!!Q`````Q!"!!!!!!$^!!!!#!!O1(!!(A!!&amp;B24&lt;W.L:824&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!$&amp;.P9WNF&gt;&amp;.U=G6B&lt;1!!%E!Q`````QF-&lt;W.B&lt;'BP=X1!$U!'!!F-&lt;W.B&lt;&amp;"P=H1!&amp;%!Q`````QJ3:7VP&gt;'6I&lt;X.U!!!21!9!#F*F&lt;7^U:6"P=H1!!""!)1J$&lt;WZO:7.U:71`!!"8!0(*\=+)!!!!!B&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!I1&amp;!!"A!!!!%!!A!$!!1!"2"09GJF9X2"&gt;(2S;7*V&gt;'6T!!!;1&amp;!!!1!'%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!!%!"Q!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)2!)!)!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!=!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!2!)!)!!!!!1!&amp;!!=!!!%!!-HNQRI!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ%1#!#!!!!!%!"1!(!!!"!!$*\=-;!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9R%!A!A!!!!"!!A!-0````]!!1!!!!!!`1!!!!A!,E"Q!"Y!!"955W^D;W6U5X2S:7&amp;N,GRW9WRB=X-!!!R4&lt;W.L:824&gt;(*F97U!!"*!-0````]*4'^D97RI&lt;X.U!!^!"A!*4'^D97R1&lt;X*U!"2!-0````]+5G6N&lt;X2F;'^T&gt;!!!%5!'!!J3:7VP&gt;'61&lt;X*U!!!11#%+1W^O&lt;G6D&gt;'6E0Q!!6Q$RS?X#C!!!!!)25H"D1WRJ:7ZU,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!+%"1!!9!!!!"!!)!!Q!%!!514W*K:7.U182U=GFC&gt;82F=Q!!'E"1!!%!"B&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=Q!"!!=!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:2%!A!A!!!!"!!5!!Q!!!1!!!!!!%1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%2!)!)!!!!#!!O1(!!(A!!&amp;B24&lt;W.L:824&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!$&amp;.P9WNF&gt;&amp;.U=G6B&lt;1!!%E!Q`````QF-&lt;W.B&lt;'BP=X1!$U!'!!F-&lt;W.B&lt;&amp;"P=H1!&amp;%!Q`````QJ3:7VP&gt;'6I&lt;X.U!!!21!9!#F*F&lt;7^U:6"P=H1!!""!)1J$&lt;WZO:7.U:71`!!"8!0(*\=+)!!!!!B&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!I1&amp;!!"A!!!!%!!A!$!!1!"2"09GJF9X2"&gt;(2S;7*V&gt;'6T!!!;1&amp;!!!1!'%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!!%!"Q!!!!%7&amp;&amp;.P9WNF&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!ER-D=O-#YQ,D%H%!!!!!ER-D=O-#YQ,D%H%!!!!!!!!!!!"!!+!"%!!!!%!!!"#Q!!!#A!!!!#!!!%!!!!!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"HQ!!!UFYH*6235\$1"!MRYZR1I!EB(UTKR#(#)E(9"4"#9EI(,BC\!E%4#;S*YAD2^Y&amp;*R\$A2&gt;!:7)7!7*RSZ\O&gt;H6.&gt;4?!663^$O;"]=KB$#[%/F3R]#_LU651_5E#&amp;$[GA&lt;+X_=QHNS]$0TK4C=+):[-@VG7M5/E$]AVR+:81#*1)32-;A[+XG+`*&gt;FM%3I4&lt;Q"'?(B\P&lt;A&amp;E3IV/5)N;IKV?.61/4MY*X&amp;%K&lt;JVUF5CKA9KQ\N6BM]"!"C9M:)O@95#D2XM``4XNU(OGR\&gt;"0A0W8#XK*EL%LGS['O:WYN;6LY1&lt;_MJ(FI9]1D*;="C9;9ANHA[/[28IV;FKE.\+(V3O[:O(@\EZDY!]*J%Z&lt;Q"G-TK&amp;Y^W9MK([#3'&lt;('O&gt;C",+'$7\92-O@FQN^?J&gt;M1VKN,#)X@`P92S^$M&gt;Y4OBX%A8&gt;TZ3JLM.U%B6-J`I.7J9:@ASE]\+Q2Z3,/69\WO;*@D5HN;_:^T],WPJ\--CU^K&gt;&gt;7E38]7E`6'JD!$E-912&amp;T'#79VF[QW3RT'A*+`1M6G2[&lt;="_!?89MY%!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!'`!!!!1W!!!!)!!!'^!!!!!!!!!!!!!!!#!!!!!U!!!%+!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B01F.(!!!!!!!!!:R$1V.(!!!!!!!!!&lt;"-38:J!!!!!!!!!=2$4UZ1!!!!!!!!!&gt;B544AQ!!!!!!!!!?R%2E24!!!!!!!!!A"-372T!!!!!!!!!B2735.%!!!!!!!!!CBW:8*T!!!!"!!!!DR(1V"3!!!!!!!!!K"*1U^/!!!!!!!!!L2J9WQU!!!!!!!!!MBJ9WQY!!!!!!!!!NR$5%-S!!!!!!!!!P"-37:Q!!!!!!!!!Q2'5%BC!!!!!!!!!RB'5&amp;.&amp;!!!!!!!!!SR-37*E!!!!!!!!!U"#2%BC!!!!!!!!!V2#2&amp;.&amp;!!!!!!!!!WB73624!!!!!!!!!XR%6%B1!!!!!!!!!Z".65F%!!!!!!!!![2)36.5!!!!!!!!!\B71V21!!!!!!!!!]R'6%&amp;#!!!!!!!!!_!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"Q!!!!!!!!!!0````]!!!!!!!!!K!!!!!!!!!!!`````Q!!!!!!!!#]!!!!!!!!!!$`````!!!!!!!!!.!!!!!!!!!!!0````]!!!!!!!!!Z!!!!!!!!!!!`````Q!!!!!!!!)1!!!!!!!!!!$`````!!!!!!!!!BA!!!!!!!!!!0````]!!!!!!!!#2!!!!!!!!!!!`````Q!!!!!!!!+Y!!!!!!!!!!$`````!!!!!!!!!OQ!!!!!!!!!"0````]!!!!!!!!%I!!!!!!!!!!(`````Q!!!!!!!!3U!!!!!!!!!!D`````!!!!!!!!"-1!!!!!!!!!#@````]!!!!!!!!%W!!!!!!!!!!+`````Q!!!!!!!!4I!!!!!!!!!!$`````!!!!!!!!"0Q!!!!!!!!!!0````]!!!!!!!!&amp;%!!!!!!!!!!!`````Q!!!!!!!!75!!!!!!!!!!$`````!!!!!!!!"ZA!!!!!!!!!!0````]!!!!!!!!,H!!!!!!!!!!!`````Q!!!!!!!!OE!!!!!!!!!!$`````!!!!!!!!$6A!!!!!!!!!!0````]!!!!!!!!4Y!!!!!!!!!!!`````Q!!!!!!!"0I!!!!!!!!!!$`````!!!!!!!!&amp;"Q!!!!!!!!!!0````]!!!!!!!!5B!!!!!!!!!!!`````Q!!!!!!!"3-!!!!!!!!!!$`````!!!!!!!!'91!!!!!!!!!!0````]!!!!!!!!:D!!!!!!!!!!!`````Q!!!!!!!"G5!!!!!!!!!!$`````!!!!!!!!'=!!!!!!!!!!A0````]!!!!!!!!&lt;:!!!!!!.5H"D1WRJ:7ZU,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!!1!"!!!!!!!%!!!!!!A!,E"Q!"Y!!"955W^D;W6U5X2S:7&amp;N,GRW9WRB=X-!!!R4&lt;W.L:824&gt;(*F97U!!"*!-0````]*4'^D97RI&lt;X.U!!^!"A!*4'^D97R1&lt;X*U!"2!-0````]+5G6N&lt;X2F;'^T&gt;!!!%5!'!!J3:7VP&gt;'61&lt;X*U!!!11#%+1W^O&lt;G6D&gt;'6E0Q!!6Q$RS?X#C!!!!!)25H"D1WRJ:7ZU,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!+%"1!!9!!!!"!!)!!Q!%!!514W*K:7.U182U=GFC&gt;82F=Q!!5A$RS?X$'A!!!!)25H"D1WRJ:7ZU,GRW9WRB=X-.5H"D1WRJ:7ZU,G.U&lt;!!K1&amp;!!!1!'(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"Q!!!!=!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!8`````!!!!!2955W^D;W6U5X2S:7&amp;N,GRW9WRB=X-!!!!!!!!!!!!!!!!!#4%S.SYQ,D!O-3=1!!!!#4%S.SYQ,D!O-3=1!!!!!!!!!!%628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"!!A!!!!!!!!!!!!!!#!!!!&amp;6*F&lt;7^U:5&amp;135*B=W5O&lt;(:D&lt;'&amp;T=Q!!!"^3:7VP&gt;'61=G^D:72V=G6$97RM1G&amp;T:3ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"8!!!!!26&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X.16%AQ!!!!.1!"!!9!!!!*28BF9X6U;7^O$56Y:7.V&gt;'FP&lt;E*B=W5628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T!!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="RpcClient.ctl" Type="Class Private Data" URL="RpcClient.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="RpcClient_Create.vi" Type="VI" URL="../RpcClient_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!!V3='.$&lt;'FF&lt;H1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%5!'!!J3:7VP&gt;'61&lt;X*U!!!I1(!!(A!!&amp;R6&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X-!"F"B=G6O&gt;!!!+E"Q!"Y!!"-25H"D1WRJ:7ZU,GRW9WRB=X-!$&amp;*Q9U.M;76O&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!)!!E$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!)!!!!#A!!!*)!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="CheckConnection.vi" Type="VI" URL="../CheckConnection.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%?!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!!V3='.$&lt;'FF&lt;H1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+E"Q!"Y!!"-25H"D1WRJ:7ZU,GRW9WRB=X-!$&amp;*Q9U.M;76O&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="GetSocketStream.vi" Type="VI" URL="../GetSocketStream.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;Q!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!7&amp;&amp;.P9WNF&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!!!55W^D;W6U5X2S:7&amp;N,GRW9WRB=X-!!$B!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!"N3:7VP&gt;'61=G^D:72V=G6$97RM1G&amp;T:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!%R&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=Q!;5G6N&lt;X2F5(*P9W6E&gt;8*F1W&amp;M&lt;%*B=W5A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="PingService.vi" Type="VI" URL="../PingService.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%:!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!"&amp;3:7VP&gt;'6"5%F#98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!""3:7VP&gt;'6"5%F#98.F)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="Ping.vi" Type="VI" URL="../Ping.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!-0````]+5X2S;7ZH)'^V&gt;!!!+E"Q!"Y!!"-25H"D1WRJ:7ZU,GRW9WRB=X-!$6*Q9U.M;76O&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!%R&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=Q!-5H"D1WRJ:7ZU)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="LookupLocalHostName.vi" Type="VI" URL="../LookupLocalHostName.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#/!!!!"!!%!!!!&amp;E!Q`````QV-&lt;W.B&lt;%BP=X2/97VF!"B!-0````]/5G6N&lt;X2F3'^T&gt;%ZB&lt;75!!&amp;1!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!!!#!Q!!?!!!!!!!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%+!!!!!!%!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">160</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="IsConnected.vi" Type="VI" URL="../IsConnected.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%O!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J$&lt;WZO:7.U:71`!!!K1(!!(A!!%R&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=Q!.5H"D1WRJ:7ZU)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#J!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!!R3='.$&lt;'FF&lt;H1A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="DisconnectRemoteService.vi" Type="VI" URL="../DisconnectRemoteService.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%:!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!"&amp;3:7VP&gt;'6"5%F#98.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!""3:7VP&gt;'6"5%F#98.F)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714384</Property>
		</Item>
		<Item Name="ConnectRemoteService.vi" Type="VI" URL="../ConnectRemoteService.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;S!!!!$!!O1(!!(A!!%R&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=Q!15G6N&lt;X2F16"*1G&amp;T:3"J&lt;A!!"!!!!#Z!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!"&amp;3:7VP&gt;'6"5%F#98.F)'^V&gt;!!91$$`````$F*F&lt;7^U:5BP=X2/97VF!!!21!9!#F*F&lt;7^U:6"P=H1!!"&amp;!!Q!+6'FN:7^V&gt;#"N=Q!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!&amp;E"1!!-!"A!(!!A):8*S&lt;X)A;7Y!!":!5!!$!!9!"Q!)#76S=G^S)'^V&gt;!"^!0!!%!!!!!%!!1!"!!)!!Q!"!!1!!1!&amp;!!%!#1!"!!%!!1!+!Q!"#!!!#A!!!!!!!!!!!!!!!!!!!!U!!!!1!!!!!!!!!!A!!!!!!!!!#!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!%1!!!!!"!!!!!!!!!!!!!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082135056</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%(!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!!V3='.$&lt;'FF&lt;H1A&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!!K1(!!(A!!%R&amp;3='.$&lt;'FF&lt;H1O&lt;(:D&lt;'&amp;T=Q!-5H"D1WRJ:7ZU)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="SetDisconnected.vi" Type="VI" URL="../SetDisconnected.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%?!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%6*Q9U.M;76O&gt;#ZM&gt;G.M98.T!!V3='.$&lt;'FF&lt;H1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+E"Q!"Y!!"-25H"D1WRJ:7ZU,GRW9WRB=X-!$&amp;*Q9U.M;76O&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
	</Item>
</LVClass>
