﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BurgundyFull</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6447714</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">15007601</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,.!!!*Q(C=T:1^DB."%)7@!1ESZ(A$E"-CIEK23+S^A:0.3#L:!TD%TCRRALK#Z2NM!HE&gt;!'HFA!.AE7+.&lt;,ZJNX_8N1F!IM=V0_^66X`4X2[JN"@3-[UPN@(:NFZ4;+QRP`:G?^L?Y'`-M8;HI[@KFS)(85`^3`XXSD(._$,`J@&lt;G&lt;$PVF]NT`H,7T'&lt;,RXT=&amp;OEI9_^8^S2DZ_`&gt;4=;JXRTZKY@_&lt;&amp;^BN6IVP`&amp;LRGLVK%`'&lt;(87L`&lt;`Z]_/`(*\00^.M`0&lt;V[Q:B_P8&lt;0Q$^X4^CT'?\?U(_[=Z=B`[F84X=(F`8GD]4U;8`Q4`NEE&gt;N2]C)IEAH$#VVDL2%TX2%TX2%TX1!TX1!TX1!^X2(&gt;X2(&gt;X2(&gt;X1$&gt;X1$&gt;X1$&lt;U=[%)8ON#:F74Q:+#E;&amp;)A31:&amp;S3HB38A3HI3(2S5]#5`#E`!E0+1IY5FY%J[%*_'BGR+?B#@B38A3(EI6ECQ(/DQ*$_56]!1]!5`!%`!QJ!+?!#!9,#A=&amp;!&amp;$A2H="$Q"4]$$L1+?A#@A#8A#(GQ&amp;0!&amp;0Q"0Q"$RU+&lt;-3B;9^U/'BD"Q?B]@B=8A=(EL,Y8&amp;Y("[(R_&amp;B/$E]$I]$Y1TI&amp;!&gt;"4C=HQ8FQ?"Q?,H*Y("[(R_&amp;R?,$+#HG:G:;G0&gt;$B-8A-(I0(Y$&amp;Y+#'$R_!R?!Q?AY?S-HA-(I0(Y$&amp;Y'%I'D]&amp;D]"AARK!-,[/9U&gt;&amp;)-A3$BV`:,6:7+1K*F5/KGV&gt;V5[JO.N6.J,IZ6"&gt;&gt;&gt;4&amp;6&amp;UFV]F5H687S6#&gt;"^?65I66B6!&gt;2\&gt;QG;M&amp;Z4NQ25W*#$)E"U3&gt;[2,@N_J=4&amp;YO&amp;ZP/Z\O\O.*V/.:F-."Q/.2A-V/`XV?PVV/VWN7WP/4;N=`B&gt;_H2N6`=XXW_`8H_Y&lt;;`X.W_PNN[)X"*00X&gt;',X^U2O_&lt;$BX`.,[.0KZ`DFZR@&lt;\_-OKA[6WJN`EO`1`@2DX2ZLK&lt;IV]YWZE!!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.22</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"U35F.31QU+!!.-6E.$4%*76Q!!',Q!!!27!!!!)!!!'*Q!!!!7!!!!!2&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!!!!!!E"1!A!!!-!!!+!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!!WI9H5(8Z^4&lt;Q'@.QL?LH.!!!!$!!!!"!!!!!!EYDB1ST%@U;2FSYD/?0?(&gt;1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!-O`&amp;/NNQ;R*IN01N,[S(NQ"!!!!`````Q!!!"$^#1AP\55??D_A$7H2?.[`!!!!"!!!!!!!!!#/!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!"`Q!!!!%!!1!!!!!!&amp;A!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!A!#!!!!!!!I!!!!)(C=9_"C9'ZAO-!!R)Q/4!V-'5$7"Q9'$A9/!3!/9!!!;YI&amp;SQ!!!%A!!!%9?*RD9-!%`Y%!3$%S-$"&gt;!N)M;/*A'M;G*M"F,C[\I/,-5$?SQI3"\NY$J*F!=F!V5$GGMU"]!NU=@CA^!UE-!)H`+-E!!!!-!!&amp;73524!!!!!!!$!!!#&gt;!!!!`2YH)V4TWP51"3?3=:OJ*%%$"+EB[KBLB*ECYPOQBZ3.I56+L;U6KR9$`ZA";5?0/T"1S3\U$CM)/B@Y-7\5IKHV+R.02?E"R(%SQ:;+)D&amp;A\#_.UGAAA5(-N^]-_^^]XZ-_B)B^]^8,ET,B-C5%).YZ0&lt;3H&lt;N(#(#3DC`Z!E9^Q`%Q%DB,5_ZPK^SV&gt;0_*:5J;RY!&gt;0G5:Q:4&amp;!G&lt;VB560D8DD14R21]96W)^&gt;3U%#S"!(-UWU_C\U&gt;.!TO.?##Q+Q!T9+S!"(5F=68@L$1FL(;&amp;Z4^+O!21UMSR#!!Q'59'U$&amp;J_2:1JH&gt;L&gt;"A&gt;JQ8*T!@?YA6V!=6(77"K2C9D%4A9HY/[(GBH[P%J'XQ+:"3@&amp;;JYD7:K)=#V(MF&lt;.CZ#EP2'34Z082$]AA,U,`=*Z*(23PTF[&lt;^\@N,)=C_,DA;]%;.5;]#N&amp;?@@#K-%@,8UO@3ZN;'SOBL;T\OT1*9&gt;G&gt;I='=.?KA/&lt;^%/Z_UNGB,F6]-(FMG@,I@M]2)1^#T5%T2X,XR!=?%WS@2Q_2S&gt;4@YK87(C/C&amp;'8TU)X:G(3739`P^&gt;J)#*B%R5,\=W&gt;'?HU&lt;`!B`_[S(]IFJ\;T!9Y/*FK+W%#&gt;\?W2.E9R7,K%WO_4V\]&gt;;.G^$9([*_#N2#8`P'_.-3P-@O#ZRB3QXG&amp;$.W&amp;2M$I2N9Y%$.8ZQ3V&lt;/7T+-!R"O5M@#-ZFXF1\&amp;4&lt;!+.(;-CI0F'1GAV-$(H52`9+E[ANBC2[?S`/%P_0?\"6S/5D)(71]$LA/=!FU!B!&gt;S3UP0@A%6!$+2!*/&amp;,C5,?3?_F1\)K(Z70SS@E-@G!;`ZLV+_YE`NZ`B``!32"Y+1!!!!4!!!!#8C=9W"A9'2E!!)!!"1!!Q!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!!Q5!)!!!!!%-41O-!!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!!Q5!)!!!!!%-41O-!!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0``````````CU9T'^N;\7P92G&amp;LWVLN;^N;,2P``````````Y!!!!'!!!!"A!0!!9!0]!'!0`Q"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``!9$``_'!@``ZA"``Y9!(`]'!!@]"A!!]!9!!!!(`````!!!#!0`````````````````````^X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X@]!$1X1U!$&gt;!!X1$&gt;!!X1X`U.U.U.$&gt;$1X&gt;$&gt;$1X1U.`^$&gt;!!$1!.U!X1!!U.U.$@`1X1X1U.U.$&gt;U.U.$&gt;$1X`U.U.U.$&gt;$1!.$&gt;$1!.U.`^X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X@``````````````````````!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!$&gt;!!!!!!!!!0]!!!!!!!$@``U!!!!!!!$`!!!!!!$@`&gt;X@`1!!!!!!`Q!!!!$@`&gt;X&gt;X&gt;`^!!!!!0]!!!!0`&gt;X&gt;X&gt;X&gt;X`!!!!$`!!!!$`X&gt;X&gt;X&gt;X&gt;`Q!!!!`Q!!!!```&gt;X&gt;X&gt;``]!!!!0]!!!!0```^X&gt;````!!!!$`!!!!$``````````Q!!!!`Q!!!!``````````]!!!!0]!!!!0``````````!!!!$`!!!!$``````````Q!!!!`Q!!!!``````````]!!!!0]!!!!0``````````!!!!$`!!!!$````````````Q!!`Q!!!!$`````````````!0]!!!!!!0``````````!!$`!!!!!!!!```^````]!!!`Q!!!!!!!!$^````]!!!!0]!!!!!!!!!!!``]!!!!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````!!!%!0```````````````````````````````````````````Y'"A9'"A9'"A9'"A9'"A9'"A9'"A9'"A9'"A9'"A@``!!!!A1#"A1#"!!!!A9%!!!#"A1!!A9%!!!#"A1#"``_"!)'"!)'"!)%!A9%!A1#"A9%!A9%!A1#"A1#"!)(``Y%!A9%!!!!!A1!!!)'"!!#"A1!!!!#"!)'"!)%!A@``A1#"A1#"A1#"!)'"!)%!A9'"!)'"!)%!A9%!A1#"``_"!)'"!)'"!)%!A9%!A1!!!)%!A9%!A1!!!)'"!)(``Y'"A9'"A9'"A9'"A9'"A9'"A9'"A9'"A9'"A9'"A@````````````````````````````````````````````]H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S@``S=H*S=H*S=H*S=H*S=H_PIH*S=H*S=H*S=H*S=H*```*S=H*S=H*S=H*S=H_KT]L+T[*S=H*S=H*S=H*S=H``]H*S=H*S=H*S=H_KT]A9'"A;SM_C=H*S=H*S=H*S@``S=H*S=H*S=H_KT]A9'"A9'"A9'ML0IH*S=H*S=H*```*S=H*S=H*[T]A9'"A9'"A9'"A9'"L+QH*S=H*S=H``]H*S=H*S=H`0S"A9'"A9'"A9'"A9(_L#=H*S=H*S@``S=H*S=H*S@]L+T]A9'"A9'"A9(_`P\]*S=H*S=H*```*S=H*S=H*`SML+SM`)'"A9(_`P\_`PQH*S=H*S=H``]H*S=H*S=H`+SML+SML0SM`P\_`P\_`#=H*S=H*S@``S=H*S=H*S@]L+SML+SML0\_`P\_`P\]*S=H*S=H*```*S=H*S=H*`SML+SML+SM`P\_`P\_`PQH*S=H*S=H``]H*S=H*S=H`+SML+SML+T_`P\_`P\_`#=H*S=H*S@``S=H*S=H*S@]L+SML+SML0\_`P\_`P\]*S=H*S=H*```*S=H*S=H*`SML+SML+SM`P\_`P\_`PQH*S=H*S=H``]H*S=H*S=HL+SML+SML+T_`P\_`P[ML+SML#=H*S@``S=H*S=H*S=H`0SML+SML0\_`P[ML0SML+SML+QH*```*S=H*S=H*S=H*`SML+SM`P[ML0SML+SML+QH*S=H``]H*S=H*S=H*S=H*S@]L+SML)'ML+SML+SM*S=H*S@``S=H*S=H*S=H*S=H*S=H`)'ML+SML+SM*S=H*S=H*```*S=H*S=H*S=H*S=H*S=H*S?ML+SM*S=H*S=H*S=H``]H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S@```````````````````````````````````````````]!!!#'!!&amp;'5%B1!!!!!1!#6%2$1Q!!!!%54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!5&amp;2)-!!!!"M!!1!$!!!54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!!!!#!!$`!!!!!1!"!!!!!!!7!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!!,5&amp;2)-!!!!!!!!!!!!!-!!!!!"AQ!!""+?*S^6VNM5W5=`X^H:_7UM(&amp;;[,K'&lt;?XGW1$&gt;T#1+#)I):R!OY&lt;+RI#3CX6IW&gt;&amp;,3&gt;AY@A$U=JD-BRCR!G#\Y.07*"RZ])5JQ5:/T+&amp;Y42;Q][".2U#Q;Z04Y`\\4=WO\DCBR4&lt;YUT@^W@J@`_19A&lt;B*LO2Q=UY#)-`BFOQ;_O%I!JNI%S0_VD)$94@Y'MDB-.&amp;AP&gt;)N8O2SJUW""8'U2WO62O)H2_NP[5D).[]4L'/I2QVD-J](#O"LS&lt;Z7SIH3_4BKN.+P[I6Y]38,=5V,E4_'EEM3'I#SDJ\_.Z)$)D4SP./W+(5II%PX6WS;%75GP"K+M6K7E\&amp;+MC+UPMZ,%QSUB)&lt;-E9-FF=/(#"4P*&lt;S3VM$&amp;79Q[X")"L*:YS/1&amp;:L5^,W6;7YW-ZW+@6\#.(=G.U&gt;JJ5G,J)6E6-R&lt;QD_5?GO7;?NLZE8L]'C[8M"C%M8)^`8VGV7XE@#"$F#D1A(=]+_B(^1SZ'00Y&gt;F!O7%5![GN=1M1O`&gt;WFQP[*STQ&amp;0S=BBSQE99W4Q*BG0)2FE%S0$+Y_+9,-R.A=&lt;-D`66,^R9$#&gt;3;3CS102XI&amp;9/BU^H$LY5CS4C-:DG6AR4Y`,;HA6R9!W9R+")0$Q*K3&gt;G#&gt;B=H)39=$44FW(K3%J;_8ZM@1Z',*QD^PYU;YW@E]A@P*@?`IIBMVL/,&gt;SZ^P+89(+^9O&amp;9\G5_]C^6_Z+&amp;.3R!O6#%E:AO)Q+6RF*NH)R(O!-*-PEL-;=YU\F9EY3=Y&lt;H6O[D2=KFO18+08X[&gt;+&amp;SVVD+Z1ERF;P@U?^1Z:\3LX&amp;LY6/(=O.)"X;+5KX?QBZ$W)0#8W(#8U@BXY$QS_=D7^VCX6?#!$J-HI#I"SA*5UXCTJ\H%\W:*T/:V-'?Q5QCL5AUT')%J[ZH?KH;FP]&amp;083#?EDEZ7RE+_U9S$_`SU?PW.)Q@;4`#K$L^WQU*/,69PPQ_$E-,\DN5Y,$U1,\U,_-4?,M^LE0Y7B_'JLXA7WC#N.%($02"_T]R,&lt;37:?6T"&amp;&gt;6HLLXFNJIJ36_G!!DJ3RR732F19Q[SDUF=FZJ]B+!^DHK.GHH*8?,&lt;&lt;3A.GLX%PA09O&amp;)E0"C`IB;KA?'!3?6$.$=3SP%UH"@ES,Y`#;`D0=9C2Y42+G+1F&amp;SX]ZMN"8HA6,N,Y^`;F%,.[:/*!O)'38"J`&amp;V18B&amp;.JGAX3_12J^G*98-;96FL-Z+FHA4RJ]%6@Z-,[+@1ZBT]-J;K(:B%&lt;X-WBOX\[.U/"J$+)UQD$0I6(Q&amp;W_&lt;EGF_M0V!PP]#$&lt;Z%T;_1MB7M;J41KN89PA[KM&gt;,&amp;CR?R%J\_C!%9S66%JS)'*^59%G5BD@15G`+]^AB.N(RR+.V_D@2UBXID^D2@'&gt;.Q&lt;*JW;X&gt;6``NJ?O^_GF\8.#D%LX';)==_K)!K"'&lt;ZX0PA'_=_/'DM!\ZFB,.,@Y?F^UL:"HT1P,-.UFPH8&amp;*8-40NXG]B;)0.M,N!"C8O"T^9\P?;\A`"?DP8E(*"QWO7_\VZ^[^U^CO:][0F@G`?`3P&gt;@7:G?\QM0F[X!`.&amp;;,6NU$UXZLO&gt;G0=&lt;G&amp;?WD&amp;3Q3V[X"D=5.1K'J%S]&lt;(_BTW^3H_^QPZR_!^LD'7PVH*L^FL@1-HECF4D57W,VXJ,6WI?E&lt;"@7JU.5!9&gt;D.-WW1O&gt;L],PR*H/^DNJ"=O*G/Y'"=]/1/B/"[.&lt;S(VBML6MWF@BZ!#)&amp;MCGR7'==U&amp;&lt;N.[$VY/N.['I:*JYX!E&amp;[0:(#Y!M';MSP.9'1_475L\14XY,GKHU:6_UY@/[_(VR36$DDX]QY#,LP,BVXM7MJ$&lt;[O`O31155"!\B&gt;,C-'!3E&lt;.!'I15=XQ"*][):N^-+"*Q/5&gt;_],!7JR8^#12HJ;_W*T]7IR1O-[$=6T\N$`8N7Z0D]S&amp;,/8Q7@["Q!&lt;/$V4[BGL-34+1BJJC.5C8GJ^`O_BVD/C'$=ZR.A:.-1Y$]5YMJ`CB,%\(*93LHY]+.$&lt;.0U9M1,'HFA]@?@=P&amp;^?,R6N`P=O&gt;%GS8CZ!\-#"DGM1F$=',IF&lt;K.K&amp;,5+(/)(X/\TKV1A4QMT5N^&lt;`^V0&lt;L5TF#KEH%7E])N4_!XX%H@)!!!!%!!!!61!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!'5!!!"V?*RD9'!I&amp;*"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```V=J9O4Y?O1;8.%2(TB4::9]BQ1!:1A:GA!!!!!!!!1!!!!(!!!$TA!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S&amp;!#!!!!!!!%!#!!Q`````Q!"!!!!!!#V!!!!"1!11#%+5WBP&gt;V2I=G6B:!!!(%"Q!!A!!!!#!!!06'BS:7&amp;E5G6G:8*F&lt;G.F!"B!1!!"`````Q!"#F2I=G6B:&amp;*F:H-!!%]!]=T]HQ=!!!!#%62I=G6B:'FO:SZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#"!5!!#!!!!!B"09GJF9X2"&gt;(2S;7*V&gt;'6T!!!;1&amp;!!!1!$%62I=G6B:'FO:SZM&gt;G.M98.T!!%!"!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)5!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!-!!!!!!!!!!1!!!!)!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N="1!A!!!!!!"!!5!"Q!!!1!!T0S@#A!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!5!)!!!!!!!1!&amp;!!=!!!%!!-T]HQI!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D&amp;!#!!!!!!!%!#!!Q`````Q!"!!!!!!#V!!!!"1!11#%+5WBP&gt;V2I=G6B:!!!(%"Q!!A!!!!#!!!06'BS:7&amp;E5G6G:8*F&lt;G.F!"B!1!!"`````Q!"#F2I=G6B:&amp;*F:H-!!%]!]=T]HQ=!!!!#%62I=G6B:'FO:SZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#"!5!!#!!!!!B"09GJF9X2"&gt;(2S;7*V&gt;'6T!!!;1&amp;!!!1!$%62I=G6B:'FO:SZM&gt;G.M98.T!!%!"!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF&amp;!#!!!!!!!%!"1!$!!!"!!!!!!!&amp;!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U921!A!!!!!!&amp;!""!)1J4;'^X6'BS:7&amp;E!!!=1(!!#!!!!!)!!!^5;(*F9723:7:F=G6O9W5!'%"!!!(`````!!%+6'BS:7&amp;E5G6G=Q!!4Q$RT0S@"Q!!!!)26'BS:7&amp;E;7ZH,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!)%"1!!)!!!!#%%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-!!"J!5!!"!!-26'BS:7&amp;E;7ZH,GRW9WRB=X-!!1!%!!!!!!!!!!!!!!!!!!!%!!A!$1!!!!1!!!$V!!!!+!!!!!)!!!1!!!!!,Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&amp;9!!!#IHC=F6(,4M*1%$XFNA+6J[#CA#E&lt;&amp;S\9O$;ZRM1N"0U!3XM,G);3^A)O`2.`S)`2P9H/P?72_!CRET2TT]S=G4E$Y!Q6XL(PRN(S@BQ,VQ&gt;;@)9=A!R14L'"#%1MJJZ!AX-9H`4"M$?R"/DB`@8D*;OKKCE_G9[[Y=),X33J^Y;0QJ085M;4Y6S+J/P*%!\PKR&lt;)6,[(A9'GMX_H+WY2R8."0!:9_S;=*V,%4B1Y/MW:R:/&amp;+Y8DO^+&amp;219&lt;N*\RJB^M^=1FMMDT,&amp;A1DJ$DTSQ;S"1155$C^#EV4\H\&lt;/Y(-.&amp;"!2O"S&amp;],5M,6`U5I19F12"E&amp;P5;&amp;S3?@GCGUA#L&gt;YG_2TH6&amp;&lt;?@C6'^B4`U-U.Q02'@CFFIY/-32&amp;M0#-;GQ.GNF0Z&amp;NJ+%N6&gt;$!S7K986=Q!6W./IFKUF!Z(*"`CK;["UX;)K_*'NLEU\2A8[9)DW%!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W!$6!!!!51!0"!!!!!!0!.A!V1!!!&amp;I!$Q1!!!!!$Q$9!.5!!!"DA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!!!!"35V*$$1I!!UR71U.-1F:8!!!9P!!!"&amp;9!!!!A!!!9H!!!!!!!!!!!!!!!)!!!!$1!!!2)!!!!(5R*1EY!!!!!!!!"&lt;%R75V)!!!!!!!!"A&amp;*55U=!!!!!!!!"F%.$5V1!!!!!!!!"K%R*&gt;GE!!!!!!!!"P%.04F!!!!!!!!!"U&amp;2./$!!!!!!!!!"Z%2'2&amp;-!!!!!!!!"_%R*:(-!!!!!!!!#$&amp;:*1U1!!!!!!!!#)%&gt;$2%E!!!!!!!!#.(:F=H-!!!!%!!!#3&amp;.$5V)!!!!!!!!#L%&gt;$5&amp;)!!!!!!!!#Q%F$4UY!!!!!!!!#V'FD&lt;$1!!!!!!!!#['FD&lt;$A!!!!!!!!#`%R*:H!!!!!!!!!$%%:13')!!!!!!!!$*%:15U5!!!!!!!!$/&amp;:12&amp;!!!!!!!!!$4%R*9G1!!!!!!!!$9%*%3')!!!!!!!!$&gt;%*%5U5!!!!!!!!$C&amp;:*6&amp;-!!!!!!!!$H%253&amp;!!!!!!!!!$M%V6351!!!!!!!!$R%B*5V1!!!!!!!!$W&amp;:$6&amp;!!!!!!!!!$\%:515)!!!!!!!!%!!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(!!!!!!!!!!!`````Q!!!!!!!!#Q!!!!!!!!!!$`````!!!!!!!!!-1!!!!!!!!!!0````]!!!!!!!!!T!!!!!!!!!!!`````Q!!!!!!!!&amp;A!!!!!!!!!!$`````!!!!!!!!!7A!!!!!!!!!!0````]!!!!!!!!"F!!!!!!!!!!!`````Q!!!!!!!!(A!!!!!!!!!!$`````!!!!!!!!!@!!!!!!!!!!!0````]!!!!!!!!%;!!!!!!!!!!%`````Q!!!!!!!!3!!!!!!!!!!!@`````!!!!!!!!"*1!!!!!!!!!#0````]!!!!!!!!%J!!!!!!!!!!*`````Q!!!!!!!!3Y!!!!!!!!!!L`````!!!!!!!!"-A!!!!!!!!!!0````]!!!!!!!!%X!!!!!!!!!!!`````Q!!!!!!!!4U!!!!!!!!!!$`````!!!!!!!!"1A!!!!!!!!!!0````]!!!!!!!!&amp;D!!!!!!!!!!!`````Q!!!!!!!!?1!!!!!!!!!!$`````!!!!!!!!#Z1!!!!!!!!!!0````]!!!!!!!!-)!!!!!!!!!!!`````Q!!!!!!!")Q!!!!!!!!!!$`````!!!!!!!!%DA!!!!!!!!!!0````]!!!!!!!!31!!!!!!!!!!!`````Q!!!!!!!"*1!!!!!!!!!!$`````!!!!!!!!%LQ!!!!!!!!!!0````]!!!!!!!!3R!!!!!!!!!!!`````Q!!!!!!!";9!!!!!!!!!!$`````!!!!!!!!&amp;K!!!!!!!!!!!0````]!!!!!!!!7K!!!!!!!!!!!`````Q!!!!!!!"&lt;5!!!!!!!!!)$`````!!!!!!!!'$!!!!!!$62I=G6B:'FO:SZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!!!!1!"!!!!!!!7!!!!!!5!%%!B#F.I&lt;X&gt;5;(*F971!!"R!=!!)!!!!!A!!$V2I=G6B:&amp;*F:G6S:7ZD:1!91%!!!@````]!!1J5;(*F9723:7:T!!"0!0(-`*](!!!!!B&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!A1&amp;!!!A!!!!)14W*K:7.U182U=GFC&gt;82F=Q!!5A$RT0S@#A!!!!)26'BS:7&amp;E;7ZH,GRW9WRB=X-.6'BS:7&amp;E;7ZH,G.U&lt;!!K1&amp;!!!1!$(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"!!!!!-!!!!!!!!!!1!!!!)!!!!!!!!!!!!"&amp;56Y:7.V&gt;'FP&lt;E*B=W5O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!2!)!)!!!!!!!!!!!!!1!!!!Z-&lt;W^Q:8)O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"-!!!!!26&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X.16%AQ!!!!+A!"!!1!!!V&amp;?'6D&gt;82J&lt;WZ#98.F&amp;56Y:7.V&gt;'FP&lt;E*B=W5O&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Threading.ctl" Type="Class Private Data" URL="Threading.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(N!!!!%Q!&gt;1!I!&amp;H"S&lt;X"P=H2J&lt;WZB&lt;#"H97FO)#B,9SE!!"V!#A!8;7ZU:7&gt;S97QA&gt;'FN:3!I6'EM)'VJ&lt;CE!(U!+!"FE:8*J&gt;G&amp;U;8:F)(2J&lt;75A+&amp;2E,#"N;7YJ!":!5!!$!!!!!1!##6"*2#"H97FO=Q!21!I!#W^V&gt;("V&gt;#"I;7&gt;I!"&amp;!#A!+&lt;X6U=(6U)'RP&gt;Q!!'%"1!!)!"!!&amp;$'^V&gt;("V&gt;#"S97ZH:1!!%%!B#E&amp;O&gt;'F8;7ZE68!!!":!)2&amp;S:7FO;82J97RJ?G5`)#B'+1!81!I!%'RP&lt;X!A=G&amp;U:3"E&gt;#!I=SE!!"&gt;!#A!1=(*P9W6T=S"W98*J97*M:1!!*%"!!!(`````!!I82GFM&gt;'6S:721=G^D:8.T6G&amp;S;7&amp;C&lt;'5!(5!(!"&gt;N;7RM;8.F9W^O:#"U;7VF=C"W97RV:1!,1!I!"7FO=(6U!!V!#A!'&lt;X6U=(6U!!!,1!I!"76S=G^S!"&gt;!#A!1;7ZU:7&gt;S982F:#"F=H*P=A!!%5!+!!NX;7ZE&gt;8!A:'FG:A"=!0%!!!!!!!!!!AZ-&lt;W^Q:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!Q1&amp;!!$!!$!!9!"Q!)!!E!#Q!-!!U!$A!0!"!!%1V1352198*B&lt;76U:8*T!!%!%A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="GetMethodName.vi" Type="VI" URL="../GetMethodName.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$.!!!!#1!;1(!!#!!!!!)!!!RS:7:F=G6O9W5A;7Y!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!":!5!!$!!%!!A!$#'6S=G^S)'FO!!!;1(!!#!!!!!)!!!VS:7:F=G6O9W5A&lt;X6U!"2!-0````]+476U;'^E4G&amp;N:1!!&amp;E"1!!-!!1!#!!-*:8*S&lt;X)A&lt;X6U!#I!]!!&amp;!!!!"!!&amp;!!9!"Q-!!$I!!!A!!!!+!!!!$1!!!!E!!!!.!1!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="StopThreadRef.vi" Type="VI" URL="../StopThreadRef.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$0!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!":!5!!$!!!!!1!##'6S=G^S)'FO!!!=1(!!#!!!!!)!!!^5;(*F9723:7:F=G6O9W5!6!$Q!!Q!!Q!%!!1!"!!%!!1!"!!%!!5!"!!%!!9$!!"Y!!!.#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!A!!!!!!1!(!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="Threading_Create.vi" Type="VI" URL="../Threading_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!"=628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T!!:198*F&lt;H1!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!R5;(*F972J&lt;G=A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!I!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="GetThreadIdx.vi" Type="VI" URL="../GetThreadIdx.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!#E!B"7:P&gt;7ZE!!^!!Q!*6'BS:7&amp;E372Y!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!!1!!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"R!=!!)!!!!!A!!$V2I=G6B:&amp;*F:G6S:7ZD:1!K1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!-6'BS:7&amp;E;7ZH)'FO!!"5!0!!$!!$!!1!"1!'!!=!"Q!(!!=!#!!(!!E!#A-!!(A!!!U)!!!*!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!E!!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714384</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="StoreThreadRefs.vi" Type="VI" URL="../StoreThreadRefs.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"Q!!A!!!!#!!!06'BS:7&amp;E5G6G:8*F&lt;G.F!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!R5;(*F972J&lt;G=A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="SetupViServer.vi" Type="VI" URL="../SetupViServer.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&amp;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!=!!)!!!!!A!!$8*F:G6S:7ZD:3"P&gt;81!+E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!$62I=G6B:'FO:S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!;1(!!#!!!!!)!!!RS:7:F=G6O9W5A;7Y!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!R5;(*F972J&lt;G=A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!I!!!#3!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Release.vi" Type="VI" URL="../Release.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%D!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!!=1(!!#!!!!!)!!!^5;(*F9723:7:F=G6O9W5!+E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!$&amp;2I=G6B:'FO:S"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!*!!!!!!!!!!!!!!#*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">128</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Initialize.vi" Type="VI" URL="../Initialize.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%2!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!$&amp;2I=G6B:'FO:S"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">128</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082405392</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="ThreadSetup.vi" Type="VI" URL="../ThreadSetup.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!#Q!G1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!*4'^P='6S)'FO!!^!!Q!)5(*J&lt;X*J&gt;(E!!".!"!!.4'^P=&amp;*B&gt;'5A7WVT81!%!!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"!!&amp;!!94:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!I1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!+4'^P='6S)'^V&gt;!!!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!)1!]!!5!!!!!1!#!!-!!Q!(!!-!!Q!$!!-!!Q!$!!-!!Q!)!!-!!Q!$!!-!#1-!!2!!!*)!!!!1!!!!%!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!."1!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Loop.vi" Type="VI" URL="../Loop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#1!K1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!-6'BS:7&amp;E;7ZH)'FO!!!%!!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!&amp;E"1!!-!!A!$!!1):8*S&lt;X)A;7Y!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!":!5!!$!!)!!Q!%#76S=G^S)'^V&gt;!#:!0!!&amp;!!!!!%!!1!"!!%!"1!"!!%!!1!"!!%!!1!"!!%!"A!"!!%!!1!"!!=$!!%1!!!)!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!$15!&amp;1!!!!!!!!!!!!!!!!!!!1!!!!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16810176</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074536976</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="overrides" Type="Folder">
		<Item Name="Work.vi" Type="VI" URL="../Work.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B4&gt;'^Q4'^P=!!!+E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!$62I=G6B:'FO:S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!=1(!!#!!!!!)!!!^5;(*F9723:7:F=G6O9W5!+E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!$&amp;2I=G6B:'FO:S"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!)!!E#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="StoreThreadRefs_Impl.vi" Type="VI" URL="../StoreThreadRefs_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"Q!!A!!!!#!!!06'BS:7&amp;E5G6G:8*F&lt;G.F!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!R5;(*F972J&lt;G=A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">269230592</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="SetupViServer_Impl.vi" Type="VI" URL="../SetupViServer_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&amp;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"J!=!!)!!!!!A!!$8*F:G6S:7ZD:3"P&gt;81!+E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!$62I=G6B:'FO:S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!;1(!!#!!!!!)!!!RS:7:F=G6O9W5A;7Y!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!R5;(*F972J&lt;G=A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!I!!!#3!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="ThreadSetup_Impl.vi" Type="VI" URL="../ThreadSetup_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!#Q!G1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!*4'^P='6S)'FO!!^!!Q!)5(*J&lt;X*J&gt;(E!!".!"!!.4'^P=&amp;*B&gt;'5A7WVT81!%!!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"!!&amp;!!94:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!I1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!+4'^P='6S)'^V&gt;!!!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!)1!]!!5!!!!!1!#!!-!!Q!(!!-!!Q!$!!-!!Q!$!!-!!Q!)!!-!!Q!$!!-!#1)!!2!!!*!!!!!1!!!!%!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!."1!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1073741952</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="InitLoop.vi" Type="VI" URL="../InitLoop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%(!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!!K1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!-6'BS:7&amp;E;7ZH)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Release_Impl.vi" Type="VI" URL="../Release_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"Q!!A!!!!#!!!06'BS:7&amp;E5G6G:8*F&lt;G.F!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!R5;(*F972J&lt;G=A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="DynamicMethod.vi" Type="VI" URL="../DynamicMethod.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B4&gt;'^Q4'^P=!!!+E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!$62I=G6B:'FO:S"P&gt;81!#E!B"%FO;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!(%"Q!!A!!!!#!!!0476U;'^E5G6G:8*F&lt;G.F!"R!=!!)!!!!!A!!$V2I=G6B:&amp;*F:G6S:7ZD:1!K1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!-6'BS:7&amp;E;7ZH)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!=!#!!*!!I!#Q)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!)!!!!#!!!!!A!!!!)!!!!E!!!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1073741952</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%(!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!":!5!!$!!!!!1!##'6S=G^S)'FO!!!K1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!-6'BS:7&amp;E;7ZH)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
		<Item Name="NrOfThreads.vi" Type="VI" URL="../NrOfThreads.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%C!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!!Q!,4H*0:F2I=G6B:(-!+E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!$62I=G6B:'FO:S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!-6'BS:7&amp;E;7ZH)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="Stop.vi" Type="VI" URL="../Stop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!J5;(*F971A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!R!)1&gt;4&gt;'^Q17RM!"J!=!!)!!!!!A!!$&amp;2I=G6B:&amp;2P5X2P=!!!*E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!#62I=G6B:#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!)!!!!EA!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">128</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
		<Item Name="Start.vi" Type="VI" URL="../Start.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'$!!!!$1!K1(!!(A!!%R&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=Q!-6'BS:7&amp;E;7ZH)'FO!!!51(!!#!!!!!)!!!:.:82I&lt;W1!!!^!!Q!)5(*J&lt;X*J&gt;(E!!".!"!!.4'^P=&amp;*B&gt;'5A7WVT81!%!!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!&amp;E"1!!-!"1!'!!=):8*S&lt;X)A;7Y!!#J!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!V5;(*F972J&lt;G=A&lt;X6U!":!=!!)!!!!!A!!#62I=G6B:&amp;*F:A!71&amp;!!!Q!&amp;!!9!"QFF=H*P=C"P&gt;81!B!$Q!"1!!!!"!!)!!Q!%!!A!"!!%!!1!"!!%!!1!"!!%!!E!#A!%!!1!"!!,!Q!"%!!!EA!!!!A!!!!)!!!!#!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#.!!!!#1!!!!!!!!!!!!!!!!!!!!U&amp;!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">128</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="StopAll.vi" Type="VI" URL="../StopAll.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%"!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!?!!!4%62I=G6B:'FO:SZM&gt;G.M98.T!!J5;(*F971A&lt;X6U!!!71&amp;!!!Q!!!!%!!ABF=H*P=C"J&lt;A!!*E"Q!"Y!!"-26'BS:7&amp;E;7ZH,GRW9WRB=X-!#62I=G6B:#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">128</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
</LVClass>
