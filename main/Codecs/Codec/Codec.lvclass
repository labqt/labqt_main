﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="11008008">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">RedFrame</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">12124142</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">2792191</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,5!!!*Q(C=T:3^&lt;F."%)60A))'*,]"2(G$+?CI8+7R+&amp;T15=ULB)J1)@-))U7#WP)&lt;O++@6\$35JE3[7+S@0&gt;[9_=[C5U"5P:G\M]ZM\/@&gt;T=L&gt;?WJ^%4F5$P:W^:_&gt;W^PZ=:N[\?@`&gt;P[Z9&lt;@3\IW`\&lt;`I@(X]B^K&amp;XP&lt;LC`N]^7-GE&lt;X_&lt;AN5C^D[V&gt;X*W0D&lt;^VVRKY`[PGLWX[TL&lt;";L5:X_$6DN&lt;L8*[.:\@7L`@$]JO=XT7QW[]``;,4R_:GTLP88&lt;\4W&lt;\C\[^^FH-SW^KX^-_KZNXX;$@&gt;/P^=/&lt;G`/A9_(`QH_&lt;Z//V"Z%2"*"/'&amp;KL:,IC:\IC:\IC2\IA2\IA2\IA?\IDO\IDO\IDG\IBG\IBG\IBNZ&gt;[%)8ON#:F74Q:+#E;&amp;)A31:&amp;S3XB38A3HI3(4S5]#5`#E`!E0+1IY5FY%J[%*_'BGR+?B#@B38A3(EJV*.F&gt;[0!E0*28Q"0Q"$Q"4]$$E!JY!I"AM+"Q5!1-"7&lt;Q%P!%0!%0LQJY!J[!*_!*?,!6]!1]!5`!%`$1J:O6['D;#RU?SMDB=8A=(I@(Y;'U("[(R_&amp;R?"Q?BJ0$Y`!Y%-[!4H%1Z(2S%JQ0B]@BY3'(R_&amp;R?"Q?BQ?L7S(P:K;F;3^U?!Q?A]@A-8A-(EL)Y$&amp;Y$"[$R_#BL!Q?A]@A-8A-(I;3Q70Q'$Q'C$%IQ]MI:H1UEAT"Y/'PWSX7L6*U*.:&gt;5NW]KJN3&gt;&lt;/J&lt;C,6T;'[[+K,K&lt;J)KJ/P/KGKE[5[#;I`4B6;&amp;5:V%.8/&lt;;+7X"@%H*A3%_+-'".$YJA9N&amp;X`=?*SO&gt;2CM&gt;"]0N&gt;U/N6E-N(:W:H'Y\''Q['/DY]V'!RUX6ZQL&gt;P2^&lt;HUE@&gt;0&gt;P(N]_G&lt;FZ04NU@NM`VO^=U"=H6?3H.:SP&gt;8J8R^2K@(4$\R\HEJ\V_8]O'KF0-\IGV@@J@SYW=JPS\8&gt;8LHUE-Y'`6)[_&gt;GDPY!9+^,Q1!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.10</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"C'5F.31QU+!!.-6E.$4%*76Q!!&amp;&amp;1!!!1S!!!!)!!!&amp;$1!!!!3!!!!!1V$&lt;W2F9SZM&gt;G.M98.T!!!!!!#)%1#!#!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!*+LIR1)=NR.FF&gt;L&gt;@Z2:[Y!!!!-!!!!%!!!!!"24,5F*8.[3,5U%&amp;="!N_&gt;V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!:IS,U*",?5S&lt;JNK6:63=L1!!!"!UO^,JN]Z^E!(R8FBVAV?)!!!!%(%WZ%$^XE4=J&lt;2D0#S$&lt;0)!!!!1-&amp;]M+_':.$$AA)'"Z&lt;Z(0A!!!+5!!5R71U-81W^E:7-O&lt;(:D&lt;'&amp;T=TJ$&lt;W2F9SZD&gt;'Q!!!!!!!)!!F:*4%)!!!!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!"!!!!!1!"!!!!!!!+!!!!!!!!!!!!!!!!!!-!!!!!!!!#!!)!!!!!!#)!!!!C?*RDY'&gt;A&amp;G!19,2A!**-!AQ'$!)=!B!-*1%9_Q&amp;G!!!!!!"$!!!"*(C=9W$!"0_"!%AR-D!QP103&lt;'DC9"L'JD:!.B?&lt;(&lt;DMR3\/#"2D?A"EM!!R%]*@D#$O(3"_A[%$C^E!FKAICQ!!!!!G!!&amp;73524&amp;U.P:'6D,GRW9WRB=X-[1W^E:7-O9X2M!!!!!!!!!!-!!!!!!01!!!'1?*T\Q-$!E'FM97&lt;!S-$!T!!"S@EJK3#["CDG$"5T0(!94-0YBW&amp;KI84T'R&lt;$3]V(7&amp;Y_`P``0Z$`'#(?\;,#U6!BT]$@+A95/.\AQAC2[G22?1&amp;7=I4FM/%"%!HE&amp;%-FA:JYOHV57$I&lt;L:D"0)(/2DM1#]TP&lt;(1#M_X!&lt;$=AW=81X1E3[_+U/-D@SA9U91@)J-.!XJ1$`.P!LB&gt;GB,OHG_/YIUA#S$G/'D0!F)-(G(,ZA/9S)$8J^X`'.`]:$5#Y(MD897"EE!3;J1;E1:;Q-T#"X=T)Q-,QBY':E9N2%/I,`-$:X]56G1]+&lt;Q"^2UM@!!!!$B%"A"5!!!9R-3YQ,D%!!!!!!!!-%1#!#!!!"$%R,D!!!!!!$B%"A"5!!!9R-3YQ,D%!!!!!!!!-%1#!#!!!"$%R,D!!!!!!$B%"A"5!!!9R-3YQ,D%!!!!!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!99TH-'*3F%BC%J:!9F+53''-ZT"A!!!!@````````````Z````[X```[$@``[!.``_!!```!!0``W!/``^Y0P``@P\``X`_``^``P``@`\``X`_``^``P```````Z`^```P^```_^````Z``````````````````Q!!!A$`````````````````````]!!!!!!!!!!!!!!!!!!!$`!!$`!!`Q$`]!``!0]!!!`Q!0!0$Q$Q]!]0!!]!]!!0]!$Q!!]!]0!0$`!0!!!!$`!!]!]0!0$Q$Q]!$Q$Q!!`Q!!`Q!0]!``!0`Q$`!!!0]!!!!!!!!!!!!!!!!!!!$``````````````````````W:G:G:G:G:G:G:G:G:G:P^G:G:G:G:G!':G:G:G:G&lt;`:G:G:G:G$=X1:G:G:G:G`W:G:G:G$=!!$&gt;"G:G:G:P^G:G:G$=!!!!!.U':G:G&lt;`:G:G&lt;=!!!!!!!!X7:G:G`W:G:GT!!!!!!!!0VG:G:P^G:G:MX=!!!!!0`]:G:G&lt;`:G:G&lt;.X&gt;Q!!0```':G:G`W:G:GT&gt;X&gt;X.````RG:G:P^G:G:MX&gt;X&gt;X````]:G:G&lt;`:G:G&lt;.X&gt;X&gt;`````':G:G`W:G:GT&gt;X&gt;X@````RG:G:P^G:G:MX&gt;X&gt;X````]:G:G&lt;`:G:G&lt;.X&gt;X&gt;`````':G:G`W:G:GX&gt;X&gt;X@```^X`^G:P^G:G:GT.X&gt;X``^X0```W&lt;`:G:G:G&lt;.X&gt;`^X0```W:G`W:G:G:G:MX&gt;U0````:G:P^G:G:G:G:GQ0````:G:G&lt;`:G:G:G:G:G:P``:G:G:G`W:G:G:G:G:G:G:G:G:G:P`````````````````````Q!!"!$```````````````````````````````````````````]E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#4``S1E*#4``S1E*0``*#4```]E*0```S1E``]E*#1E*0``*#1E`S1E`S4`*#4`*0]E*0]E`S1E*0]E*0]E*#1E``]E*#4`*#1E*0]E*0]E`S1E`S4``S1E`S1E*#1E*#4``S1E*0]E*0]E`S1E`S4`*#4`*0]E*#4`*#4`*#1E*0``*#1E*0``*#1E``]E*0```S1E````*#4``S1E*#1E``]E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#4`````````````````````````````````````````````H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=``_=H*S=H*S=H*S=H*S=H!!!H*S=H*S=H*S=H*S=H*T``ZS=H*S=H*S=H*S=H!$[4`L[!*S=H*S=H*S=H*S=H0``H*S=H*S=H*S=H!$[4S1E*#4[_A#=H*S=H*S=H*S=``_=H*S=H*S=H!$[4S1E*#1E*#1E_PI!H*S=H*S=H*T``ZS=H*S=H*T[4S1E*#1E*#1E*#1E*0L[H*S=H*S=H0``H*S=H*S=H%^0*#1E*#1E*#1E*#1E`0K=H*S=H*S=``_=H*S=H*S=4`L[4S1E*#1E*#1E`0T]4ZS=H*S=H*T``ZS=H*S=H*R0_PL[_E]E*#1E`0T]`0R0H*S=H*S=H0``H*S=H*S=H%`[_PL[_PJ0_PT]`0T]`%_=H*S=H*S=``_=H*S=H*S=4`L[_PL[_PL]`0T]`0T]4ZS=H*S=H*T``ZS=H*S=H*R0_PL[_PL[_PT]`0T]`0R0H*S=H*S=H0``H*S=H*S=H%`[_PL[_PL[`0T]`0T]`%_=H*S=H*S=``_=H*S=H*S=4`L[_PL[_PL]`0T]`0T]4ZS=H*S=H*T``ZS=H*S=H*R0_PL[_PL[_PT]`0T]`0R0H*S=H*S=H0``H*S=H*S=H0L[_PL[_PL[`0T]`0T]_PKML+S=H*S=``_=H*S=H*S=H%^0_PL[_PL]`0T]_PJ0L+SML+SMH*T``ZS=H*S=H*S=H*R0_PL[_PT]_PJ0L+SML+SMH*S=H0``H*S=H*S=H*S=H*S=4`L[_PIEL+SML+SML*S=H*S=``_=H*S=H*S=H*S=H*S=H%]EL+SML+SML*S=H*S=H*T``ZS=H*S=H*S=H*S=H*S=H*S=L+SML*S=H*S=H*S=H0``H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=````````````````````````````````````````````!!!!!A!#!!!!!!#&gt;!!&amp;'5%B1&amp;U.P:'6D,GRW9WRB=X-[1W^E:7-O9X2M!!!!!!!"!!*52%.$!!!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!!!!!!!1!"!!!!!!!+!!!!!!!!!!!!!!!!!!!!!1!!!%&amp;16%AQ!!!!!!!!!!!!!Q!!!!!!!`!!!!KF?*T&amp;6EFI%V%9`F_;B.&gt;A^;6N&lt;)/W7:CYNUD&amp;6IN\2^.7L5MN,A=V/(5"N&gt;J&amp;X%5=B2\%1[%(Q9/88DXUY&amp;'2)-B=^#+OB"Y^C&amp;[+AE\'`]VENES&gt;#AJGY*%-``&gt;PX`=^!B"ZS/I$2&lt;CO!G(4_'7(#B&amp;*)1$Z*AKF$_MD0Y$5RIE+GWA@?R]IEI5KT*'5283F/!J@-62\K+W!4^$"JD!UT/+9+;,#0%GJCX9,"39]7CC-BMS557BA&gt;UER=%")@+.XZ1'M"P)3@E;&lt;3"')G!I'Z@4OX.F_7?"P+ZNI8%^:K1)4F;J"I&lt;!9-W,J:XJ+%C:NJ-Z-#:BS#5R/4NKAK!&amp;;J,?R"D'E$6'&gt;*/S$K5&lt;-E&amp;"9I7-C/A&lt;L&gt;*JVR%2RD0@/1?81'E&amp;"*-)OFC&lt;G5"/G&lt;JI2&gt;F+&amp;7K'QB=&lt;JF01O6,6(@AQ%C0Q3YED&amp;%;J.;E`);B+/^H!K&gt;%1VMC%I*../7#`_\&amp;6BG;Q%DI+VZDY9U_E)GH3MYX2MU_GI&amp;%=:W(S-T=+('-SH'TJ/DQQ.^Q]G"YYHDZX/$1UFTQW?OJ!&lt;\E^+O?'=F[HVIB*PYWPAR8321!S#M!@;H6M@A)G*#&gt;Q%HD:U!U,LB)+&amp;C_L4&lt;,!W,^ELZ&amp;8N&amp;7\%&amp;9L@^ZXA;]SU"RT#X7Q,NQ7&amp;'W8F0&lt;G%O`L@#\&gt;66"KOFQE87G%&gt;:(V%W';!&lt;/&amp;C0-"/;08"L%(-$;&gt;Q%&gt;/+G/TMQFUL+MSN8)YN5_\Y_(CZ=NMNZ19*-:7L`&gt;2_=O7_UN[1-%QZF#MB(6AJR?_.,VBD/&gt;&lt;A[[]QVZ]VB3I_3H3\F:LR*S!:"E/NI9Y"K@^9'1X9;K?OE+LNJ4@IGL0=.3QE&amp;B,&gt;P%R.;7C8=]\RBHJ=TN%_!WCYN4`K2UC$6R(HP19*YJ/"FFE.-FBG%0Z:;D0V?Y0A$&amp;MS"S&amp;T#'S&lt;6$BM=N/WS1784=T/8$;Z_/^N=GEGGT2$#D&lt;[30[SRS9J2,6!MQ`GCM=G+;T49N&lt;RM]F6LUV3:CU`GVSTFO]R#V$$,'_V;9D!7I&gt;:&gt;C%J7)^QS8V%4R6BG6O&lt;NTA*7:W%G.MLT8^QKY@%E4.H,J8R-%=&amp;'267,22C*P`TI1);91%/VLA&gt;U`!TGD$;)-7+:$ZB&amp;+61,S@VE"1`7&lt;KUSSR.]^T?5%HDI8D/(PLX73M4^I#X$@@NV\&gt;GT&amp;CD%^HI&gt;-Z--]\&amp;E+1?EO)B6AH*W]V`#,6G2-(F('\@'T0=(LRTG#]*!XM=\K$PHY^1@G0TRQA-X;Z^M?L?ZZE#T4_'N&amp;=1.&lt;]!NB5&lt;O;&amp;#4/SI@MK[O+BJ&amp;^X+(O!6A\@.@0K!4O&gt;@7`]?]TMMJ0QS]#&amp;U5,C@I07`!&amp;G?Z-9!!!!%!!!!0!!!!#9!!5*%3&amp;!81W^E:7-O&lt;(:D&lt;'&amp;T=TJ$&lt;W2F9SZD&gt;'Q!!!!!!!!!!Q!!!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!N9!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=B%!A!A!!!!"!!A!-0````]!!1!!!!!!:1!!!!-!#E!B"52V&lt;7VZ!$U!]=ETD_U!!!!#$5.P:'6D,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!%E"1!!%!!!6$&lt;W2F9Q!71&amp;!!!1!"$5.P:'6D,GRW9WRB=X-!!1!#!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=B%!A!A!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!A!!!!!!!!!"!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!2!)!)!!!!!1!&amp;!!=!!!%!!-ETD_]!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ%1#!#!!!!!%!"1!(!!!"!!$*-Y`P!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9R%!A!A!!!!"!!A!-0````]!!1!!!!!!:1!!!!-!#E!B"52V&lt;7VZ!$U!]=ETD_U!!!!#$5.P:'6D,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!%E"1!!%!!!6$&lt;W2F9Q!71&amp;!!!1!"$5.P:'6D,GRW9WRB=X-!!1!#!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G52!)!)!!!!!1!&amp;!!-!!!%!!!!!!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B%1#!#!!!!!-!#E!B"52V&lt;7VZ!$U!]=ETD_U!!!!#$5.P:'6D,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!%E"1!!%!!!6$&lt;W2F9Q!71&amp;!!!1!"$5.P:'6D,GRW9WRB=X-!!1!#!!!!!!!!!!!!!!1!"1!3!!!!"!!!!)5!!!!I!!!!!A!!"!!!!!!3!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!5-!!!*:?*S65-N/QF!5H0;W!L5A)DY"+2M8,NCQ='63AX(B2O)870IQG*+;^J&lt;ITD`QO`Q99`Q$J\&gt;.D'&amp;"T+2.ZX4//8-'Q"#7/T+P]_8S&amp;:@Y`JC]@Q,1G^-E#0VRP0*D,]O[&gt;`/HU*&gt;85K;,?3\$&lt;/T,'"VX"AUQF23XKPFLP&lt;F2MK,D8(6IAWG=:T*-H32SF-2Z4B=L4Y:/Y%E0*A%,!3=:%#3CIJC1V0(!,ZO6'82MI9&lt;7(R^HCD5W\,$A=YIIF'Y.)II@58@@2()PSU+92'BTEO#G*FIC$S)9'/(C@R(:+-SU93N4OU+_"*8R(81K%QS%:_E]A.@QR60+'QX=5/JA(Q@U5?"1:6.#6&amp;CP`0YZ5CCTUX"=:&lt;-J`^+71;7"QIN+FRYUGD4),7RD$VW=I)=_KXU#@!&lt;E0:Q7OXY!)!2KA!!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W!$6!!!!51!0"!!!!!!0!.A!V1!!!&amp;I!$Q1!!!!!$Q$9!.5!!!"DA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!!!!"35V*$$1I!!UR71U.-1F:8!!!56!!!"$)!!!!A!!!5.!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%^#5U=!!!!!!!!"H%.$5U=!!!!!!!!"M%R*&gt;GE!!!!!!!!"R%.04F!!!!!!!!!"W&amp;2./$!!!!!!!!!"\%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!!!!!#+(:F=H-!!!!%!!!#0%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$1!!!!!!!!#S'FD&lt;$A!!!!!!!!#X%.11T)!!!!!!!!#]%R*:H!!!!!!!!!$"%:13')!!!!!!!!$'%:15U5!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'!!!!!!!!!!!`````Q!!!!!!!!#E!!!!!!!!!!$`````!!!!!!!!!,A!!!!!!!!!!0````]!!!!!!!!!T!!!!!!!!!!!`````Q!!!!!!!!$A!!!!!!!!!!$`````!!!!!!!!!9Q!!!!!!!!!!0````]!!!!!!!!"F!!!!!!!!!!!`````Q!!!!!!!!']!!!!!!!!!!$`````!!!!!!!!!A1!!!!!!!!!!0````]!!!!!!!!#-!!!!!!!!!!%`````Q!!!!!!!!-I!!!!!!!!!!@`````!!!!!!!!!TQ!!!!!!!!!#0````]!!!!!!!!$4!!!!!!!!!!*`````Q!!!!!!!!.A!!!!!!!!!!L`````!!!!!!!!!X!!!!!!!!!!!0````]!!!!!!!!$B!!!!!!!!!!!`````Q!!!!!!!!/9!!!!!!!!!!$`````!!!!!!!!""Q!!!!!!!!!!0````]!!!!!!!!')!!!!!!!!!!!`````Q!!!!!!!!IE!!!!!!!!!!$`````!!!!!!!!#CQ!!!!!!!!!!0````]!!!!!!!!+U!!!!!!!!!!!`````Q!!!!!!!!\%!!!!!!!!!!$`````!!!!!!!!$MQ!!!!!!!!!!0````]!!!!!!!!/_!!!!!!!!!!!`````Q!!!!!!!!^A!!!!!!!!!!$`````!!!!!!!!$WA!!!!!!!!!!0````]!!!!!!!!32!!!!!!!!!!!`````Q!!!!!!!"*-!!!!!!!!!!$`````!!!!!!!!%F1!!!!!!!!!!0````]!!!!!!!!3A!!!!!!!!!#!`````Q!!!!!!!"0)!!!!!!F$&lt;W2F9SZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!1V$&lt;W2F9SZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!!!!!!!"!!%!!!!!!!I!!!!!!Q!+1#%&amp;2(6N&lt;8E!01$RS4/0\1!!!!).1W^E:7-O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!31&amp;!!!1!!"5.P:'6D!%I!]=ETD_]!!!!#$5.P:'6D,GRW9WRB=X-*1W^E:7-O9X2M!#J!5!!"!!%&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!#!!!!!A!!!!$`````!!!!!!!"&amp;56Y:7.V&gt;'FP&lt;E*B=W5O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!*!)!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"8!!!!!26&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X.16%AQ!!!!.1!"!!9!!!!*28BF9X6U;7^O$56Y:7.V&gt;'FP&lt;E*B=W5628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T!!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Codec.ctl" Type="Class Private Data" URL="Codec.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
	</Item>
	<Item Name="overrides" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="Decode_Impl.vi" Type="VI" URL="../Decode_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!?!!!0$5.P:'6D,GRW9WRB=X-!#5.P:'6D)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!H%5FO=(6U5X2S:7&amp;N,GRW&lt;'FC%UFO=(6U5X2S:7&amp;N,GRW9WRB=X-!#UFO=(6U5X2S:7&amp;N!#*!=!!?!!!0$5.P:'6D,GRW9WRB=X-!#%.P:'6D)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!1!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
		</Item>
		<Item Name="Encode_Impl.vi" Type="VI" URL="../Encode_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%N!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!?!!!0$5.P:'6D,GRW9WRB=X-!#5.P:'6D)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!7&amp;%^V&gt;("V&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!!!,37ZQ&gt;824&gt;(*F97U!)E"Q!"Y!!!].1W^E:7-O&lt;(:D&lt;'&amp;T=Q!)1W^E:7-A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="Codec_Create.vi" Type="VI" URL="../Codec_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!?!!!0$5.P:'6D,GRW9WRB=X-!#5.P:'6D)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#B!=!!?!!!8&amp;56Y:7.V&gt;'FP&lt;E*B=W5O&lt;(:D&lt;'&amp;T=Q!'5'&amp;S:7ZU!!!C1(!!(A!!$QV$&lt;W2F9SZM&gt;G.M98.T!!B$&lt;W2F9S"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="Decode.vi" Type="VI" URL="../Decode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!?!!!0$5.P:'6D,GRW9WRB=X-!#5.P:'6D)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$R!=!!?!!!H%5FO=(6U5X2S:7&amp;N,GRW&lt;'FC%UFO=(6U5X2S:7&amp;N,GRW9WRB=X-!#UFO=(6U5X2S:7&amp;N!#*!=!!?!!!0$5.P:'6D,GRW9WRB=X-!#%.P:'6D)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Encode.vi" Type="VI" URL="../Encode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%P!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#*!=!!?!!!0$5.P:'6D,GRW9WRB=X-!#5.P:'6D)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!7&amp;%^V&gt;("V&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!!!-4X6U=(6U5X2S:7&amp;N!!!C1(!!(A!!$QV$&lt;W2F9SZM&gt;G.M98.T!!B$&lt;W2F9S"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
		</Item>
	</Item>
</LVClass>
