﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="11008008">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BurgundyFull</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">10197915</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">13290186</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,L!!!*Q(C=T:3R&lt;B."%)&lt;H#")J)C%`!=AN$&gt;+U3$2_"@M2ZB6=ZBIE0U"!GC*&amp;#BL,0!"S%TK++7ENR!N9F-9[W8SXNW@HH-2O1-J?ZM\_`^G:T\O&lt;%UHD8/3Z&lt;%_.U&gt;&amp;2_X63*U;VW/KD6'&amp;X3V;&lt;V-ZPYI[@-\;.0NI?TN`@GJ&lt;ZVP(&lt;`C@Y4YUX2]?BPVI&gt;]V@6OKJ7D`GY.6)H9_^H^S"DZ_`&gt;*O018X@]T8W`WF@9&lt;$&lt;L"`S=M&gt;E][J.2&lt;9\[W8Z[@N8RKWIWGX88@\X?_@T-72L&gt;`6MX`BXX=0^4RGCWN__&gt;HX8(P?]T\LA0_JVR]HDT(CB0`R0]XS&amp;330UC)I*QQAC6WNI'?K!(?K!(?K!\OK-\OK-\OK-&lt;OK%&lt;OK%&lt;OK%LOK)LOK)LOK+H#VX1"6X17:7A?&gt;!I+"I5#**"E?!7]!1]!5`!QV=*?!+?A#@A#8B)E9!HY!FY!J[!BWE3]!1]!5`!%`"1+J&amp;%ON$B#8AI,Q[0Q_0Q/$Q/$SX&amp;Y8%!H':/9;=)'/+9TA?(R_&amp;R?0AI$I`$Y`!Y0!Y0NDA]$I`$Y`!Y0%R*K_+*JL\1Y;'-'$Q'D]&amp;D]"A]F";$R_!R?!Q?AY&gt;W9P!90!;%U&gt;!I$I)9EYQ%YYP"9`$Q%)0(Y$&amp;Y$"[$"SPNE+76K7HK#RU?B5@B58A5(I7(%K,Q+$Q+D]+D]&amp;"7&amp;"[&amp;2_&amp;2?"1?7IH#I`!I0!K)UJ4WIB24*CJ*CK$Q]*&gt;/C[:&gt;]E3C[2,*BV@SI:2]W#1@)MG(1`+G3^Z-S:ME?@%F,[LER:+]#**`H'2IS4#3GUC?8#@+EPO#G".49E+-C3%R)0J%LZ\[DR/8S[5M&amp;AO:T_=SH5ZF-JH)?$S7Y8!IA]&amp;!_PW_^(I^;=&gt;LLG95\8OJZ00VYM@VT@GH'Z^]`0,B`/LTV_^8V\8?_CH/&lt;IPSYGV2PLUISNOT*H[`,-LK@5'BR_*8?&lt;H^5\\C_?,HN\+YJ.;\IKX^..[.]ES;ZW[.`A)AJ-`D!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.5</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"W=5F.31QU+!!.-6E.$4%*76Q!!'9!!!!1=!!!!)!!!'7!!!!!=!!!!!2&gt;4:82U;7ZH=UBB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!!!)A2!)!)!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!UUUO:\S7CE/C_WFY)*MU%A!!!!Q!!!!1!!!!!(=@/G5=3HR-BK5_^6ME:P45(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!!UY:CN$3O=4:S34]4KPOO2!!!!%-*:XZD&lt;#2+W?9&amp;;H3R&amp;&amp;C-!!!!16O:.[$S2[DOFY'F&amp;PO!0!1!!!"!Q8SQLY:EU-/#!A9(FPE=_!!!"JQ!"4&amp;:$1SN4:82U;7ZH=UBB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=TJ4:82U;7ZH=UBB&lt;G2M:8)O9X2M!!!!!!!%!!*735R#!!!!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!F:*1U-!!!!!!2209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!"16%AQ!!!!'Q!"!!-!!"209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!!!!)!!1!!!!%!!1!!!!!!"1!!!!!!!!!!!!!!!!!#6EF131!!!!!!!!)32GFM:6.U=G6B&lt;3ZM&gt;G.M98.T!A=!5&amp;2)-!!!!$!!!1!(!!!!!!F*4V.U=G6B&lt;8-+2GFM:6.U=G6B&lt;2*';7RF5X2S:7&amp;N,GRW9WRB=X-!!!!!!!!!!1!!!!!!"!!"!!!!!!9!!!!!!!!!!F:*5%E!!!!!!B2.:7VP=HF4&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q)(!&amp;"53$!!!!!U!!%!"Q!!!!!*35^4&gt;(*F97VT$%VF&lt;7^S?6.U=G6B&lt;22.:7VP=HF4&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!!!!!!1!!!!!!"!!"!!!!!!9!!!!!!!!!!Q!!!!!#!!)!!!!!!"Y!!!!9?*RDY'*A&amp;G!19.2A9'!S9'$AA%)'!!F)!+E!!!!!!%9!!!%E?*RD9-!%`Y%!3$%S-$$^!.)M;/*A'M;G.E!W&amp;ZM&gt;O/T&amp;)1Y59XI#J*G"G!GKBA-CR81(C$_A;_#(UBZ)9A#FDCDV!!!!!!![!!&amp;73524+V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T/F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZD&gt;'Q!!!!!!!!!!Q!!!!!"4Q!!!J"YH,P"S-#1;7RB*M$%Q-$-!!(*_3GJ),I'+/9-&amp;4-]="B-Q`C(I@2F+.X]BK@&lt;257AOW%#U-"/&amp;R5/)%]'3,-!;9F/&amp;J58X#"&amp;2XB!_C9TAN4,!'55A#IEA,1+E"9"KQ3:!71$;:ZO(R5"E!G&gt;0#IP1&amp;;U(O"P61#:!F1$-J;H_1!,.H8.2W1/-S4$X=6C?+HZ#-N,\````Q0ZDR(C1#-Y'CLE'@B&lt;R9!#RRN='#&amp;3).&gt;#T'%Z&lt;(A!2!)ZGV(]C?STYU"D1$)P/'%_:'9CWX]G20A0K'\+!@ZN*X;!X-PP?B$E8[D9!&lt;$@0S0]V]VRX&amp;(E"]B\DBI'D#$+Y1'9ZX+$%&gt;7H1/L4^`_-SY(U41:'BP.17BSI4!&gt;)NQ0J_U"[0:2G"0J2$51$V&lt;%T-)($BJ'"B?%*YWP'4YQ`I5'*(TD\O\AC]U(J$1!+&gt;Z'P!!!!!!Q2!)!J!!!%-4%O-!!!!!!-%1#!#!!!"$%R,D!!!!!!$"%!A#E!!!1R-3YQ!!!!!!Q2!)!)!!!%-4%O-!!!!!!-%1#!+1!!"$%R,D!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'&gt;X&gt;3&gt;I1C7I:G)F;W&amp;#*3FO=C5H9!!!!(`````A!!!!9!!!!'!!]!"A!QQ!9!Q$!'!Q!-"A-!$!9$Q$Q'!`$]"A0``!9$``Q'!``]"A0``!9$``Q'!``]"A0``Y9"```G!(``BA!@@Q9!"@Q'!!$Q"A!!!!@````]!!!)!``````````````````````X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;`^X``@`^``X``@X^X^X``@`@X&gt;X^X&gt;`&gt;X^X^`^`@X&gt;X`X@`&gt;`^X@X&gt;`&gt;`@X`X^`^`^X&gt;`@X&gt;X^X@X@X^X^`&gt;`@`@`^X``&gt;`&gt;X^X^`&gt;`&gt;``X`X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;```````````````````````-T-T-T-T-T-T-T-T-T-T`T-T-T-T-T-T-T-T-T-T-`]T-T-T-T-`@`-T-T-T-T0`-T-T-T-`&gt;X&gt;`]T-T-T-T`T-T-T-`&gt;X&gt;X&gt;X`T-T-T-`]T-T-`&gt;X&gt;X&gt;X&gt;X@`-T-T0`-T-T.X&gt;X&gt;X&gt;X&gt;X`T-T-T`T-T-T@`&gt;X&gt;X&gt;X``=T-T-`]T-T-X``^X&gt;X```X-T-T0`-T-T.````X````^T-T-T`T-T-T@`````````=T-T-`]T-T-X`````````X-T-T0`-T-T.`````````^T-T-T`T-T-T@`````````=T-T-`]T-T-X`````````X-T-T0`-T-T0````````````T-T`T-T-T.X```````X````-`]T-T-T-X`````X````-T0`-T-T-T-T@``X````]T-T`T-T-T-T-T.X````]T-T-`]T-T-T-T-T-T``]T-T-T0`-T-T-T-T-T-T-T-T-T-T`````````````````````]!!!1!````````````````````````````````````````````6F:76F:76F:76F:76F:76F:76F:76F:76F:76F:7``^76P```V&lt;```^7````6P```V&lt;`6P^76P^76P```V&lt;``V&lt;`6F:76P^76F:7`V:76P^76P^7``^7`V&lt;`6F:76P``6F&lt;``V:7``^76F&lt;`6F:7`V:7`V&lt;`6P``6P^7``^7``^76F:7`V&lt;`6F:76P^76F&lt;`6F&lt;`6P^76P^7`V:7`V&lt;``V&lt;```^76P```V:7`V:76P^76P^7`V:7`V:7````6P``6F:76F:76F:76F:76F:76F:76F:76F:76F:76F:7`````````````````````````````````````````````SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+```+SML+SML+SML+SML+SPY_#ML+SML+SML+SML+SML``]L+SML+SML+SML+SPY`0L]`0AL+SML+SML+SML+SP``SML+SML+SML+SPY`0J76F:7`0TY+SML+SML+SML+```+SML+SML+SPY`0J76F:76F:76PT]_#ML+SML+SML``]L+SML+SML`0J76F:76F:76F:76F&lt;]`#ML+SML+SP``SML+SML+SP[_F:76F:76F:76F:76P\]+SML+SML+```+SML+SML+`L]`0J76F:76F:76P\_`PIL+SML+SML``]L+SML+SML_PT]`0T[6F:76P\_`P\__CML+SML+SP``SML+SML+SP[`0T]`0T]_PT_`P\_`P\[+SML+SML+```+SML+SML+`L]`0T]`0T]`P\_`P\_`PIL+SML+SML``]L+SML+SML_PT]`0T]`0T_`P\_`P\__CML+SML+SP``SML+SML+SP[`0T]`0T]`0\_`P\_`P\[+SML+SML+```+SML+SML+`L]`0T]`0T]`P\_`P\_`PIL+SML+SML``]L+SML+SML_PT]`0T]`0T_`P\_`P\__CML+SML+SP``SML+SML+SP]`0T]`0T]`0\_`P\_`PT]L+SM+SML+```+SML+SML+SP[_PT]`0T]`P\_`PT]_KSML+SML#ML``]L+SML+SML+SML_PT]`0T_`PT]_KSML+SML#ML+SP``SML+SML+SML+SML+`L]`0T]6KSML+SML+QL+SML+```+SML+SML+SML+SML+SP[6KSML+SML+QL+SML+SML``]L+SML+SML+SML+SML+SML+[SML+QL+SML+SML+SP``SML+SML+SML+SML+SML+SML+SML+SML+SML+SML+````````````````````````````````````````````Q!!!LI!!5:13&amp;!L5W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-[5W6U&gt;'FO:X.)97ZE&lt;'6S,G.U&lt;!!!!!!!"1!#6%2$1Q!!!!!!!2209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!"16%AQ!!!!'Q!"!!-!!"209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!!!!)!!!!!!!%!!1!!!!!!"1!!!!!!!!!!!!!!!!!!!!%!!!!,5&amp;2)-!!!!!!!!!!!!!*'5&amp;"*!!!!!!!!!B*';7RF5X2S:7&amp;N,GRW9WRB=X-#"Q"16%AQ!!!!-!!"!!=!!!!!#5F05X2S:7&amp;N=QJ';7RF5X2S:7&amp;N%E:J&lt;'64&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!!!!!!!!"!!!!!!!%!!%!!!!!"A!!!!!!!!!!!!!!!!%!!!#.!!*'5&amp;"*!!!!!!!#&amp;%VF&lt;7^S?6.U=G6B&lt;3ZM&gt;G.M98.T!A=!5&amp;2)-!!!!$1!!1!(!!!!!!F*4V.U=G6B&lt;8--476N&lt;X*Z5X2S:7&amp;N&amp;%VF&lt;7^S?6.U=G6B&lt;3ZM&gt;G.M98.T!!!!!!!"!!!!!!!%!!%!!!!!"A!!!!!!!!!!!!!!!!%!!!#&gt;!!*%2&amp;"*!!!!!!!#%E:J&lt;'64&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q)(!&amp;"53$!!!!!Q!!%!"Q!!!!!*35^4&gt;(*F97VT#E:J&lt;'64&gt;(*F97U32GFM:6.U=G6B&lt;3ZM&gt;G.M98.T!!!!!!!!!!%!!!!!!!1!!1!!!!!'!!!!!!!!!!!!!!!!!1!!!#I!!E2%5%E!!!!!!!)5476N&lt;X*Z5X2S:7&amp;N,GRW9WRB=X-#"Q"16%AQ!!!!.!!"!!=!!!!!#5F05X2S:7&amp;N=QR.:7VP=HF4&gt;(*F97U5476N&lt;X*Z5X2S:7&amp;N,GRW9WRB=X-!!!!!!!%!!!!!!!1!!1!!!!!'!!!!!!!!!!!!!!!!!1!!!#I!!Q!!!!!%"A!!#G2YH+V78WA4&gt;RT``MZL_37W_%P`;!/.S=*6:6K)&amp;L5L[FQ^2;MLERC=1JH(=GKBJJ*%G7R4*&amp;%JEO&amp;7[=/AASHWN1^^=)^D&amp;((=C[DY9"^#]6G[S&gt;Q']X,\`C[Z8#\880NA$I\D]PN]P^`\`0H&gt;!@D0M#[B"&amp;&gt;V)/QN8BT4Q:`5#-"],Y8+DS8)@U![AE3(`44"&amp;I13[&gt;;B*;FNID&amp;Z!P\!J=&lt;0RD:Y4M[T26T;T)*9S;`$OK3W)4!E&amp;:EUWSV..&amp;EF!R"C"6)30J@#`^"#@BS\18Y,0Q&gt;[31G)`)%IZK/@+3EV,`'\PFY;.%P[&gt;'#SVJK7CJOR)L&lt;_T3QJ&lt;#6299^6%L$E&amp;JC&lt;G\."A4*IETF'0W*)&amp;)$U#6M^-'WS&amp;MJ)R7UGRG^CM%_@V5=/FS&lt;Z\"R5$WW8.%1C\+P+%X/I"&gt;0X,QM\LU/(6"SE1&lt;K9@.H5?DT`%!C1`"09C&amp;*]19V@D-O]3G#93W%CWF#.HA(#YHA&gt;V_($P#;=!&gt;(C_#&amp;-GFK)FB:\O";(4#V]]A1$7YT*&amp;=31R@FI[-$9J5R745@'TU;_(&amp;-SG=D&amp;^/BF*;N'EEJ7==OU6^;#OTE(P*HJ%/A%%?:!K;6](':G:J!'0.P1@1D&gt;)"7LO)$Z.'K6^K4.(_^K]`=R]C@`?_)=Z\"H1+BR\6L&lt;N4P1N1&amp;70Z0$N4P@PWNXI:GOVLE72O!H3(EY=(=::,M7VQ0-QIA(JB]RVWJ&gt;CZA2R+27&gt;OV(MM;=NO89/NN/45X6WX;A;FO2%-OWRDPD(&lt;@NAL%%T_&amp;CD7W4+!&gt;WCP".9QF\&amp;,!(JX_.28_X:62Z.DTE&gt;/J*&lt;Q%C!J4&gt;OC[O:L/DK8/:QUIK/;;G[Q4"I5/G6VK06OZA@K\Q`$"2,I;(?-0WSO-\-P1V(WX9E3(D.9#"`+VK-CE+&lt;G^]YY[+C-=N/,6C6,[NCQL`X&lt;9V;RQ6J(?QZR4UH!9\-'P-Q!BG9!LG?&gt;K/T86(&lt;+TZ(,(*P@`9Z*?,41*S=.9D!D&gt;=M=EB[CYE0$!X8&lt;(*9:_\6B_PW.RSRS:H^@++T526!H&gt;Y`I:/(JZHRF/9AO.G?!3/)SUICKT&amp;T%D%D&amp;?1BH[H1_]VX/54+TCUO:+&gt;FE`6#_0J+`&amp;M7F5OO#7Z,WN&gt;W[7CAP5L)G^%-_Q$S90&gt;"W7(_`DK.KQX#$',H\]/.XI&lt;@F&gt;DU&gt;&lt;L:9O+Z&lt;]Z$16/A`8RM925R."H$CLO.+2C9*65_!_.DKG.C0D?251)C&gt;DL3=10.54QV^IHKS&amp;C:"ECGGD1.VT4C#Y]OE4ZHMO0]J,G'RW`^^VZM^R#[\O/RC8:]&amp;L!$O))VX4IF!_U`=K/]$W=(K%(W42KA0P$?DJ.X][`K(\]T2_L)P($Z4(Z5`IR4,P_"\C&gt;_]M!!!!!!!1!!!"$!!!!/A!"1E2)5#N4:82U;7ZH=UBB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=TJ4:82U;7ZH=UBB&lt;G2M:8)O9X2M!!!!!!!!!!-!!!!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!1\!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)2!)!)!!!!!1!)!$$`````!!%!!!!!!.-!!!!%!#J!=!!?!!!5%E:J&lt;'64&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!#E:J&lt;'64&gt;(*F97U!!#Z!=!!?!!!7&amp;%VF&lt;7^S?6.U=G6B&lt;3ZM&gt;G.M98.T!!!-476N&lt;X*Z5X2S:7&amp;N!!"4!0(*-Z$S!!!!!B&gt;4:82U;7ZH=UBB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!?1&amp;!!!A!!!!%05W6U&gt;'FO:X.)97ZE&lt;'6S!#"!5!!"!!)85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!!1!$!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=B%!A!A!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!Q!!!!!!!!!"!!!!!A!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ%1#!#!!!!!%!"1!(!!!"!!$*-Z$U!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N="%!A!A!!!!"!!5!"Q!!!1!!S4/1^!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-2!)!)!!!!!1!)!$$`````!!%!!!!!!,M!!!!%!#J!=!!?!!!5%E:J&lt;'64&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!#E:J&lt;'64&gt;(*F97U!!#Z!=!!?!!!7&amp;%VF&lt;7^S?6.U=G6B&lt;3ZM&gt;G.M98.T!!!-476N&lt;X*Z5X2S:7&amp;N!!"4!0(*-Z$S!!!!!B&gt;4:82U;7ZH=UBB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!?1&amp;!!!A!!!!%05W6U&gt;'FO:X.)97ZE&lt;'6S!!A!5!!"!!)!!1!$!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G52!)!)!!!!!1!&amp;!!-!!!%!!!!!!!A!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B%1#!#!!!!!1!+E"Q!"Y!!"132GFM:6.U=G6B&lt;3ZM&gt;G.M98.T!!!+2GFM:6.U=G6B&lt;1!!,E"Q!"Y!!"95476N&lt;X*Z5X2S:7&amp;N,GRW9WRB=X-!!!R.:7VP=HF4&gt;(*F97U!!&amp;-!]=ETE0)!!!!#&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"Z!5!!#!!!!!1^4:82U;7ZH=UBB&lt;G2M:8)!#!"1!!%!!A!"!!-!!!!"&amp;"*';7RF5X2S:7&amp;N,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!"&amp;B2.:7VP=HF4&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!1!"A!.!!!!"!!!!+5!!!!I!!!!!A!!"!!!!!!0!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!6U!!!,`?*S&gt;5=N/QE!50?U5?9C#5%&amp;"3&amp;=O7,$B"WJ)C"ODE&lt;WRU#GJ'3BJ"[)\0](@]H-U@I#XQR!-2ED-47&lt;GHDHX&gt;3[!.DLOH%\9F5%I_&amp;$'X*NWR8)MP#1"#BM1[+[9.@O'4[0Y::N&lt;`!E$1XS]^^Y_!:DV):=SH%W3;W`G#R[P9_T&lt;U2-@SSMJYX#UE$TJDK6!W\W$37&amp;';3M-$SLFV[[5V7U]T&gt;CBD!&lt;-6F]M%MFD*QI=28&lt;G=&lt;DU*(&gt;]4XL)E+%!H\*&lt;M-BBWE50"]DBE6Z&amp;1N,OMO1X`OTT5N5L\+G8&gt;\.AA:AAZ\[S[&amp;[O!"Y&amp;+&amp;-]IVJ(/'9,0S$2&gt;[Q)$H;O"@X`,O*9D6L'#9JKI!K4T\Y7I)1K+54D:UA&lt;/ARIB3Q-C/$A&amp;$69SOL%7*OF\4?S_4F4NF,?Q,F7=^`W,%#XFE:6['&lt;56B[(M.&amp;!5_-8^'KCF&lt;9,]ROB'+M2!!!!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!'9!!!!1=!!!!)!!!'7!!!!!!!!!!!!!!!#!!!!!U!!!%#!!!!"N-35*/!!!!!!!!!62-6F.3!!!!!!!!!7B36&amp;.(!!!!!!!!!8R01F.(!!!!!!!!!:"$1V.(!!!!!!!!!;2-38:J!!!!!!!!!&lt;B$4UZ1!!!!!!!!!=R544AQ!!!!!!!!!?"%2E24!!!!!!!!!@2-372T!!!!!!!!!AB735.%!!!!!!!!!BRW:8*T!!!!"!!!!D"(1V"3!!!!!!!!!J2*1U^/!!!!!!!!!KBJ9WQU!!!!!!!!!LRJ9WQY!!!!!!!!!N"-37:Q!!!!!!!!!O2'5%BC!!!!!!!!!PB'5&amp;.&amp;!!!!!!!!!QR-37*E!!!!!!!!!S"#2%BC!!!!!!!!!T2#2&amp;.&amp;!!!!!!!!!UB73624!!!!!!!!!VR%6%B1!!!!!!!!!X".65F%!!!!!!!!!Y2)36.5!!!!!!!!!ZB71V21!!!!!!!!![R'6%&amp;#!!!!!!!!!]!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!0````]!!!!!!!!!L!!!!!!!!!!!`````Q!!!!!!!!$!!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!+5!!!!!!!!!!$`````!!!!!!!!!JQ!!!!!!!!!!0````]!!!!!!!!#Q!!!!!!!!!!!`````Q!!!!!!!!--!!!!!!!!!!$`````!!!!!!!!!UQ!!!!!!!!!"0````]!!!!!!!!%I!!!!!!!!!!(`````Q!!!!!!!!3Q!!!!!!!!!!D`````!!!!!!!!"-!!!!!!!!!!#@````]!!!!!!!!%U!!!!!!!!!!+`````Q!!!!!!!!4A!!!!!!!!!!$`````!!!!!!!!"0!!!!!!!!!!!0````]!!!!!!!!&amp;"!!!!!!!!!!!`````Q!!!!!!!!7)!!!!!!!!!!$`````!!!!!!!!"YQ!!!!!!!!!!0````]!!!!!!!!,E!!!!!!!!!!!`````Q!!!!!!!!Z1!!!!!!!!!!$`````!!!!!!!!%FQ!!!!!!!!!!0````]!!!!!!!!3:!!!!!!!!!!!`````Q!!!!!!!"+E!!!!!!!!!!$`````!!!!!!!!%QQ!!!!!!!!!!0````]!!!!!!!!4&amp;!!!!!!!!!!!`````Q!!!!!!!"&gt;5!!!!!!!!!!$`````!!!!!!!!&amp;VQ!!!!!!!!!!0````]!!!!!!!!8:!!!!!!!!!!!`````Q!!!!!!!"?1!!!!!!!!!)$`````!!!!!!!!'01!!!!!%V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2&gt;4:82U;7ZH=UBB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!!!!%!!1!!!!!!"1!!!!!%!#J!=!!?!!!5%E:J&lt;'64&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!#E:J&lt;'64&gt;(*F97U!!#Z!=!!?!!!7&amp;%VF&lt;7^S?6.U=G6B&lt;3ZM&gt;G.M98.T!!!-476N&lt;X*Z5X2S:7&amp;N!!"4!0(*-Z$S!!!!!B&gt;4:82U;7ZH=UBB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!?1&amp;!!!A!!!!%05W6U&gt;'FO:X.)97ZE&lt;'6S!&amp;Y!]=ETE01!!!!#&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T%V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZD&gt;'Q!+E"1!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!$!!!!!0``````````!!!!!2132GFM:6.U=G6B&lt;3ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!295476N&lt;X*Z5X2S:7&amp;N,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!"&amp;56Y:7.V&gt;'FP&lt;E*B=W5O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!*!)!!!!!!!!!!!!!!!1!!!""4:82U;7ZH=SZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"8!!!!!26&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X.16%AQ!!!!.1!"!!9!!!!*28BF9X6U;7^O$56Y:7.V&gt;'FP&lt;E*B=W5628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T!!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="SettingsHandler.ctl" Type="Class Private Data" URL="SettingsHandler.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="ClearSettings_Impl.vi" Type="VI" URL="../ClearSettings_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!".4:82U;7ZH=UBB&lt;G2M:8)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="ReadSettingsFromFile_Impl.vi" Type="VI" URL="../ReadSettingsFromFile_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!".4:82U;7ZH=UBB&lt;G2M:8)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="SaveSettingsToFile_Impl.vi" Type="VI" URL="../SaveSettingsToFile_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!".4:82U;7ZH=UBB&lt;G2M:8)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="SettingsHandler_Create.vi" Type="VI" URL="../SettingsHandler_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'!!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!!R4:82U;7ZH=S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!"95476N&lt;X*Z5X2S:7&amp;N,GRW9WRB=X-!!!R.:7VP=HF4&gt;(*F97U!!#J!=!!?!!!5%E:J&lt;'64&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!#E:J&lt;'64&gt;(*F97U!!#Z!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!!N4:82U;7ZH=S"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!)!!!!#A!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!".4:82U;7ZH=UBB&lt;G2M:8)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!*!!!!!!!!!!!!!!#*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
		<Item Name="GetFileStream.vi" Type="VI" URL="../GetFileStream.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&lt;!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!5%E:J&lt;'64&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!%E:J&lt;'64&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!W1(!!(A!!'2&gt;4:82U;7ZH=UBB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!35W6U&gt;'FO:X.)97ZE&lt;'6S)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="GetMemoryStream.vi" Type="VI" URL="../GetMemoryStream.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!7&amp;%VF&lt;7^S?6.U=G6B&lt;3ZM&gt;G.M98.T!!!5476N&lt;X*Z5X2S:7&amp;N,GRW9WRB=X-!!$:!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!".4:82U;7ZH=UBB&lt;G2M:8)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972416</Property>
		</Item>
		<Item Name="ReadSettings.vi" Type="VI" URL="../ReadSettings.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!".4:82U;7ZH=UBB&lt;G2M:8)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="SaveSettings.vi" Type="VI" URL="../SaveSettings.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!".4:82U;7ZH=UBB&lt;G2M:8)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541072</Property>
		</Item>
		<Item Name="ClearSettings.vi" Type="VI" URL="../ClearSettings.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!:&amp;V.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=CZM&gt;G.M98.T!".4:82U;7ZH=UBB&lt;G2M:8)A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.E"Q!"Y!!"E85W6U&gt;'FO:X.)97ZE&lt;'6S,GRW9WRB=X-!%F.F&gt;(2J&lt;G&gt;T3'&amp;O:'RF=C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
</LVClass>
