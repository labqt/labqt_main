﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BurgundyFull</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">10026888</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">15007601</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,K!!!*Q(C=T:3R&lt;BJ"%)&lt;H\"1OIE1]17QE+WWM;;.56/YJX+7;.U#ECK_E&gt;D@/'R#;N+&amp;*_GF4IMAP1"N&amp;OBTZ&lt;FH!BT%UC?2&gt;"J&lt;`HZX^W&amp;V/*,54E7?S/.1O^\&lt;'*ZK_@CXS9,(SE\DM[U'3.`.8M=H90@`SE@EN@V5T_`PZ$\88?^OW@X[_TT_P"F6V`JC0WS#V-D:_&gt;L=SVP\'87:M_Y/78T`UKUW&amp;OKY(/`S=5&gt;?0_G25^6Y`WU`0LVJ_65UGE`&lt;_$Q:LHZ]Z3;V^@I/F@]`&gt;0P_5=4H:W!`OT[$F0P2J^^S&gt;@KM&gt;P.Y]"]L$@Y,`WU1+;2Z%2""/'+(37)N!$`2!$`2!$X2(&gt;X2(&gt;X2(&gt;X2$.X2$.X2$.X2&amp;6X2&amp;6X2&amp;6`45U16&gt;U!7&gt;81E7$R9+CA9&amp;AG21*(A,?!+?A#@AY;M%0!&amp;0Q"0Q"$SE3-!4]!1]!5`!QT1*?!+?A#@A#8AIF5AC&gt;82Y!B\+C]0D]$A]$I`$QZ,C]$A!TG*/9;=)'/+9TM$B=8A=(I&lt;C]$A]$I`$Y`"AC]0D]$A]$I`$QZ3U+ZZIGIY/$W8%Y$&amp;Y$"[$R_#BN"A]"I`"9`!90#QH"I`"9U!9#RL&amp;12"DEJ&amp;A@$&amp;Y$"Y_R/!R?!Q?A]@AQ5IH:'FH'JKGI]/D]#A]#I`#I`"11B1?B5@B58A5(MK+QK0Q+$Q+D],$5K,Q+$Q+DQ+C,-LSIB24*CJ*CK$Q]%KX2&gt;-J?3,2V%8SZ:6]+36@.MG830,FE(TIEA^4]C&amp;*XHT*GSJZMS2PAO1@*RF;-ITE2320&lt;B*FTPO-G"*D9E1-C4\2)\J%JZH[DR0H]\H-:D/:4K=S(I^F."L*=$C5@L]PP6Z0ONWO&gt;$I&gt;7&lt;64_L)6K_&gt;3S@DWZOLCZH0`\.08(R=XY`\:\@DK3[/P`"2(XYLSZ%V2HDYPSP)9D8D`ICA`P#P+DX628O_)R?+O@08H&gt;`HSVVVZ`0.\+&gt;@-?VOM;D_.:[-=S@*TP5&gt;`!?Z-ITI!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.14</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"O*5F.31QU+!!.-6E.$4%*76Q!!&amp;R!!!!2Z!!!!)!!!&amp;P!!!!!:!!!!!22&amp;&gt;G6O&gt;%BB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!!!!!!!*!5!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!C0UD_AC/S%+4E(F'F[Q0]A!!!!Q!!!!1!!!!!-\]WUW6Y&lt;J/NX)2TA-1*C`5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#A7M;,U-5,2)!R#&amp;@,K"&gt;5!1!!!0````]!!!!1+``'BI)9.-!UL*3VBG=V(!!!!!1!!!!!!!!!DA!"4&amp;:$1Q!!!!)!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!F:*1U-!!!!!!2209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!"16%AQ!!!!'Q!"!!-!!"209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!!!!)!!@]!!!!"!!%!!!!!!!Y!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!)!!A!!!!!!+1!!!$2YH'0A9W"O9,D!!-3-1-T5Q01$S0\!)-!"%I71#";%"!"NK1X@!!!!!!!!3Q!!!2BYH'.AQ!4`A1")-4)Q-.U!UCRIYG!;RK9GQ'5O,LOAYMR1.\*#2"G"9ER\A!QGE"R5$1@9/QR-&amp;Y$Y",IZ,/A#1!!!C6MI.!!!!!!-!!&amp;73524!!!!!!!$!!!"G1!!!IRYH/NA:'$).,9Q_Q#EG9&amp;9B+'")4E`*:7,!=BHA)!3*A9Y=)&lt;3BA=/A_FA2AC`_1V0NYO+1(/.CA14@[M)5+4&lt;2U7EUU?&amp;J:.&amp;Z169R2'?Q^U?/==&gt;&lt;5#]&lt;A[A_(%8&amp;1Y1"UCTA/D`A2EA6:O"T!#A;2Q.&amp;=I-`#UM9/OC$B^P-)&amp;;"D-S[D!$-^2NQ7(.&lt;TC!?G4!VD;K!.X?(;+CU$F2!]DK&lt;.1"E2-.1+)'VE&lt;]5Q\TNZI$&gt;@57M(2O"-HV&amp;A%:9'E(FGYP&amp;B2X`W$EHX+1@^N*@J=4H3&gt;?AA+&amp;@^PBZC-==&gt;'()7%']4](U';"AQ^:OBMP!-6[*Y*)I""0:QC(R(%8$BURI#L'%ZV!LX&lt;SQ%+%Y\!T6$`9_35K!JUGI&amp;"B!9G#V83T(8@1/!%+)Q?2'7!KQY-22&amp;6YA(E&amp;(5$?$B!"=N&amp;BBA"IP'ER9!&gt;J1'T$Q-CQ%UDL!7EZI-[81,K#%3)_"UBL!'G1!^A:))(,S-$"%-)9R&lt;C5=1XD:M:&gt;D!=:DT(C-*YIY/TPYIL-B[5P!#_#?"E!!!!!!!!4!!!!#8C=9W"A9'2E!!)!!"1!!Q!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!!Q5!)!!!!!%-41O-!!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!!Q5!)!!!!!%-41O-!!!!!!/&amp;!'!*Q!!"D%U,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"GJ*:-:+L63'&lt;ON5REKJ6):KK7&lt;'!!!!"`````Y!!!!'!!!!"A!,!!9!)-!'!)!Q"A)!$!9!!!Q'!9!Y"A(A_!9"_`A'!@`Y"A(`_!9"``A'!@`Y"A(`_!9$``_'!(`XZA!`XY9!$X]'!!(]"A!!]!9!!!!(`````!!!#!0`````````````````````YC)C)C)C)C)C)C)C)C)C0_)`Y_0C0C0C0D`C0C0_)D`C0C0DY_0D`DY_0DYDYC)`YD`D`_0`Y_0_0DY_)`YC0_)_)_0DY_0C0DY_0C0C)D`C0_0DY_0DYDY`YD`D`C)`YC)C)C)C)C)C)C)C)C)C0``````````````````````!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!0T`!!!!!!!!$`!!!!!!!0S)C0]!!!!!!!`Q!!!!!0S)C)C)`Q!!!!!0]!!!!0S)C)C)C)D`!!!!$`!!!!$-C)C)C)C)`Q!!!!`Q!!!!T`S)C)C)``Q!!!!0]!!!!-```)C)```]!!!!$`!!!!$0```]`````!!!!!`Q!!!!T`````````Q!!!!0]!!!!-`````````]!!!!$`!!!!$0`````````!!!!!`Q!!!!T`````````Q!!!!0]!!!!-`````````]!!!!$`!!!!$````````````Q!!`Q!!!!$-```````]````!0]!!!!!!-`````]````!!$`!!!!!!!!T``Y````]!!!`Q!!!!!!!!$)````]!!!!0]!!!!!!!!!!!``]!!!!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````!!!%!0```````````````````````````````````````````UJ+3EJ+3EJ+3EJ+3EJ+3EJ+3EJ+3EJ+3EJ+3EJ+3P``3EL``UL`3P^+3P^+3P^+3P^+``^+3P^+3P``3EJ+``^+3P^+3P^+`UL`3P^+``^+`UL`3P^+`UJ+`UJ+3EL``UJ+``^+````3P```UL`3P``3P^+`UL`3EL``UJ+3P``3EL`3EL`3P^+`UL`3P^+3P^+`UL`3P^+3P^+3EJ+``^+3P``3P^+`UL`3P^+`UJ+`UL``UJ+``^+``^+3EL``UJ+3EJ+3EJ+3EJ+3EJ+3EJ+3EJ+3EJ+3EJ+3EJ+3P````````````````````````````````````````````]H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S@``S=H*S=H*S=H*S=H*S=H*35H*S=H*S=H*S=H*S=H*```*S=H*S=H*S=H*S=H*@RV`0QF*S=H*S=H*S=H*S=H``]H*S=H*S=H*S=H*@RV3EJ+3PT]*3=H*S=H*S=H*S@``S=H*S=H*S=H*@RV3EJ+3EJ+3EL]`#5H*S=H*S=H*```*S=H*S=H*`RV3EJ+3EJ+3EJ+3EJ+`0QH*S=H*S=H``]H*S=H*S=H&gt;86+3EJ+3EJ+3EJ+3EL_`#=H*S=H*S@``S=H*S=H*S&gt;V`0RV3EJ+3EJ+3EL_`PZV*S=H*S=H*```*S=H*S=H*X8]`0T]&gt;5J+3EL_`P\_`H5H*S=H*S=H``]H*S=H*S=H&gt;@T]`0T]`(8]`P\_`P\_&gt;3=H*S=H*S@``S=H*S=H*S&gt;V`0T]`0T]`0\_`P\_`PZV*S=H*S=H*```*S=H*S=H*X8]`0T]`0T]`P\_`P\_`H5H*S=H*S=H``]H*S=H*S=H&gt;@T]`0T]`0T_`P\_`P\_&gt;3=H*S=H*S@``S=H*S=H*S&gt;V`0T]`0T]`0\_`P\_`PZV*S=H*S=H*```*S=H*S=H*X8]`0T]`0T]`P\_`P\_`H5H*S=H*S=H``]H*S=H*S=H`0T]`0T]`0T_`P\_`P\]`+SML#=H*S@``S=H*S=H*S=H&gt;88]`0T]`0\_`P\]`(7ML+SML+QH*```*S=H*S=H*S=H*X8]`0T]`P\]`(7ML+SML+QH*S=H``]H*S=H*S=H*S=H*S&gt;V`0T]`%KML+SML+SM*S=H*S@``S=H*S=H*S=H*S=H*S=H&gt;5KML+SML+SM*S=H*S=H*```*S=H*S=H*S=H*S=H*S=H*S?ML+SM*S=H*S=H*S=H``]H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S=H*S@```````````````````````````````````````````]!!!!#!!)!!!!!!)9!!5:13&amp;!!!!!"!!*52%.$!!!!!2209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!"16%AQ!!!!'Q!"!!-!!"209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!!!!)!!0]!!!!"!!%!!!!!!!Y!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!#N16%AQ!!!!!!!!!!!!!Q!!!!!%Y!!!$**YH,68&lt;WQ4:2B`XFMXLL/4[RRDV=VWT7U1NS6F#X_=!Z(&gt;U)VFC.V%_4$7\6K2,*P:OO%HC+%;&amp;I?14$:%G]#8+@C"R#EE'AH"BEDO#YG*(_2$1W)U2O+@2#+*OZ\0?^@LX@8;+V^IE[:LXN`T0/`PT^-6Q0-"6]&gt;EY+A-B,O0&lt;`JFK"1F!J"K9S(\;(Y0O#(S(Z!;(Z&amp;B*TP%X7%SJ&amp;Y'DSAVMS&amp;B$P\#U]JZ:10]1-;YOXCUAP.BM5I:VC9E70,W]7G/PVT0TZ8L:&lt;X1Q*UE'?9VXP]P?T)RC2UBM:'_?NN)"ID1[()FAC^(*K)*HH\K&lt;G.^;EWX$*QA65XR[1V9%8P@5%MS,34)&gt;/EF!5NOB*76&amp;10EV5$.[BD&lt;%%/#!+3$;8(!6!N3QT3@&lt;F5RF3I'_X4I@12`:I(/4E(ZU#=%C5-IYN\/8JFC&gt;:S]MS$OE!QV@(I8[W0PCD_2KHW*KU#!J![SSB6FFO+^!V1'^7QV+M&amp;,J+G4='(]-ST$-QG*'1(+M*,"@N&gt;B1:8#J5P2J5KR7Z8#,=RR9'CR5%),Q:5+.H30TUT(IV/"S6BA&lt;$QS02VY;_L.W5A]'B!D]9B&gt;J?W#Z.N+';$.6)@!/H$".4BM:HQ3FJ?8E12].;!\%,K?4_&gt;Q8CR^!S:SL)M'?\3LQ&gt;\TS*\Q90!.SG"4*W-V&lt;INBX(9ULJ@,(]NCX-W0Q,B&lt;U%^(]YQ,-&lt;A)2RR-O&amp;5$'=&lt;&amp;]Q"@1]Q"MQURR]T'25Q--5&gt;+'`&gt;:GX%J.M_YCYO,_=&lt;NT"H829BGX-2N:66:J@;^J`Q/*_#YS&lt;YC[I'&gt;'KF&lt;`]9?Z\!(Z&lt;^-Z\]\ZV&lt;BML`0;N?_B\#LJW=W/B&amp;`+4)BDE?H]K4!=15=N_FV;$I!QI/K0&gt;H0-54(;9CY=C(N\^/MI6X@%K14[GA$FC$B"5&amp;:"8&gt;*&lt;`""M"NDTJY6&amp;TZ0QWD*L,S@FR8[_.!1L(B7_EU-[)EJUR0$K)GZI,Z_9_4GD#5X_IC7X#Q^ANS=,:3&lt;9:C(39=-@'4,T4SCED$MA0H9FJNZ\*05_TDFZB.\&lt;O&lt;V8E[Z3?:E+*K?\Z8@-"]T;HI9&amp;4?%KAB3C."/\7C\;&gt;CCKF#OKX".F.&lt;&lt;6TV"&amp;4JT)ZUJ+%/A!L1!O1=0455DYCP2G&amp;W1\Q3J&lt;B/@$G.V7PFJ9'!(")M2_ZA-+=XAFIX_!P"G+LV_\8J:IO^2M0:?Y6C@WW^UPYH&amp;HL/GJ2&lt;+I1P]_NU5&lt;\(PVW`.O2`78/^#VW&gt;0),%82+G-I]QKPS+T\8"!:8;.TORS17;@QBM./`M\Y-I37[FOJHUTU:E#6P^59Z:A^7RYP#D&lt;*GD5L`&lt;T,LNN0U01&gt;DY&gt;-5#N/(A8D$AI=N(W(@ME\)&amp;"-WP];@,QIFSSL\";]-.OW&amp;N#&amp;-2_DP.PZN-_][5"1F#PT`^(-4E0GO5=U?1MJUM-`S=C(BG_%+7!&amp;B.^'-MG`Z++W7&gt;&gt;Z(_KW&gt;V@=FGFAI`X2U:@\?X:(^A\?DA[&amp;L?,_:5GJEE8$[\-6O!&gt;6N#6&lt;&amp;,U*2+#E%\A0S]7I_'KG9:X."IKK+P$+.+!K2N\Z_9-3`=+@7LHVO#Z&gt;WNO&gt;:S[^=OJ1K@V8Q&amp;MG"=5JQ.=$QZT4):V1H@V&gt;;[8@N7TP7Q0FU1J]'OEFEWS^V-`ZHYHJ0JTS-2NO%37_(._NOZ`9N[H0Q!!!!1!!!".!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!"!I!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=B1!A!!!!!!"!!A!-0````]!!1!!!!!!S!!!!!5!+%"Q!"Y!!!Z-97*73568)%^C;G6D&gt;!!/4'&amp;C6EF&amp;6S"09GJF9X1!!":!=!!3!!%!!!J&amp;&gt;G6O&gt;&amp;&amp;V:86F!!!71(!!#!!!!!)!!!F5;(*F9723:79!4A$RS4/0S1!!!!)528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!(%"1!!)!!1!#$%6W:7ZU3'&amp;O:'RF=A!!(E"1!!%!!R2&amp;&gt;G6O&gt;%BB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!!!1!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=B1!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!Q!!!!!!!!!"!!!!!A!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ&amp;!#!!!!!!!%!"1!(!!!"!!$*-Y`,!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N="1!A!!!!!!"!!5!"Q!!!1!!S4/0SQ!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-5!)!!!!!!!1!)!$$`````!!%!!!!!!-A!!!!&amp;!#B!=!!?!!!/4'&amp;C6EF&amp;6S"09GJF9X1!$ERB9F:*26=A4W*K:7.U!!!71(!!%A!"!!!+28:F&lt;H22&gt;76V:1!!&amp;E"Q!!A!!!!#!!!*6'BS:7&amp;E5G6G!%Y!]=ETD]E!!!!#&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"R!5!!#!!%!!AR&amp;&gt;G6O&gt;%BB&lt;G2M:8)!!"Z!5!!"!!-528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-!!!%!"!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF&amp;!#!!!!!!!%!"1!$!!!"!!!!!!!)!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U921!A!!!!!!&amp;!#B!=!!?!!!/4'&amp;C6EF&amp;6S"09GJF9X1!$ERB9F:*26=A4W*K:7.U!!!71(!!%A!"!!!+28:F&lt;H22&gt;76V:1!!&amp;E"Q!!A!!!!#!!!*6'BS:7&amp;E5G6G!%Y!]=ETD]E!!!!#&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"R!5!!#!!%!!AR&amp;&gt;G6O&gt;%BB&lt;G2M:8)!!"Z!5!!"!!-528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-!!!%!"!!!!!!!!!!!!!!!!!!!!!!!"!!(!"%!!!!%!!!!RA!!!#A!!!!#!!!%!!!!!")!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!":!!!!M6YH*V1WUL$1"3=\:L?D,877GMPEC=2(`L3(YB)25']&amp;&amp;%@X39&lt;L93W**PCIX`A&gt;R6`2P`!EUW]55J"$LP,G:W&gt;H4-!&gt;L&amp;P4WB([5Q-&lt;EZ\N^&lt;&amp;Y%E[;KZ(D8A6-+$9G]K2OIJE*"-U$S!$&amp;+Y@!SH=PP2QDI^:^X57YV8.0B%DVZ&gt;"RZ][PAD$;C*[K&amp;1Q(%2+BBV(_7D:F[4$E$&amp;`PQ(ON.L&lt;1L8S(T"7/C!F"NY_]K.1S=!;?Z:G7J.A/"6+7KZ1!A96CH!"^KY&lt;HL&lt;I)I?#H10X`!@E\2=_\KM%E'-0:6,H+""XF5?O2W?;$%R]JQ(\8RG5%'?Q2K?J:VDH[NGF(W,52"HV"2HN;8:F[=4ET%!WXMBP(P&gt;E&lt;Q8(*'_BCCW&gt;AI%;D@^62FLTS-`.NKYE/I:[;G::`%5Y_H&gt;'#_G)."AJ=5+SZ'Y$G^B"!UV#GF3-6A9N1BJI;T&lt;!0Q%&lt;:)VK!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!&amp;R!!!!2Z!!!!)!!!&amp;P!!!!!!!!!!!!!!!#!!!!!U!!!%;!!!!"Z-35*/!!!!!!!!!8B-6F.3!!!!!!!!!9R36&amp;.(!!!!!!!!!;"$1V.5!!!!!!!!!&lt;2-38:J!!!!!!!!!=B$4UZ1!!!!!!!!!&gt;R544AQ!!!!!!!!!@"%2E24!!!!!!!!!A2-372T!!!!!!!!!BB735.%!!!!!!!!!CR(1U2*!!!!!!!!!E"W:8*T!!!!"!!!!F241V.3!!!!!!!!!LB(1V"3!!!!!!!!!MR*1U^/!!!!!!!!!O"J9WQU!!!!!!!!!P2J9WQY!!!!!!!!!QB$5%-S!!!!!!!!!RR-37:Q!!!!!!!!!T"'5%BC!!!!!!!!!U2'5&amp;.&amp;!!!!!!!!!VB75%21!!!!!!!!!WR-37*E!!!!!!!!!Y"#2%BC!!!!!!!!!Z2#2&amp;.&amp;!!!!!!!!![B73624!!!!!!!!!\R%6%B1!!!!!!!!!^".65F%!!!!!!!!!_2)36.5!!!!!!!!!`B71V21!!!!!!!!"!R'6%&amp;#!!!!!!!!"#!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!0````]!!!!!!!!!N!!!!!!!!!!!`````Q!!!!!!!!$)!!!!!!!!!!$`````!!!!!!!!!.!!!!!!!!!!!0````]!!!!!!!!":!!!!!!!!!!!`````Q!!!!!!!!&amp;M!!!!!!!!!!$`````!!!!!!!!!:Q!!!!!!!!!!0````]!!!!!!!!"\!!!!!!!!!!!`````Q!!!!!!!!(]!!!!!!!!!!$`````!!!!!!!!!ZQ!!!!!!!!!"0````]!!!!!!!!$N!!!!!!!!!!(`````Q!!!!!!!!0)!!!!!!!!!!D`````!!!!!!!!!^A!!!!!!!!!#@````]!!!!!!!!$\!!!!!!!!!!+`````Q!!!!!!!!0]!!!!!!!!!!$`````!!!!!!!!""!!!!!!!!!!!0````]!!!!!!!!%+!!!!!!!!!!!`````Q!!!!!!!!1]!!!!!!!!!!$`````!!!!!!!!"-!!!!!!!!!!!0````]!!!!!!!!'R!!!!!!!!!!!`````Q!!!!!!!!L)!!!!!!!!!!$`````!!!!!!!!#N!!!!!!!!!!!0````]!!!!!!!!,8!!!!!!!!!!!`````Q!!!!!!!""!!!!!!!!!!!$`````!!!!!!!!%%A!!!!!!!!!!0````]!!!!!!!!15!!!!!!!!!!!`````Q!!!!!!!""A!!!!!!!!!!$`````!!!!!!!!%-A!!!!!!!!!!0````]!!!!!!!!1U!!!!!!!!!!!`````Q!!!!!!!"4A!!!!!!!!!!$`````!!!!!!!!&amp;/A!!!!!!!!!!0````]!!!!!!!!5]!!!!!!!!!!!`````Q!!!!!!!"5=!!!!!!!!!)$`````!!!!!!!!&amp;I1!!!!!%%6W:7ZU3'&amp;O:'RF=CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!22&amp;&gt;G6O&gt;%BB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!!!!%!!1!!!!!!$A!!!!!&amp;!#B!=!!?!!!/4'&amp;C6EF&amp;6S"09GJF9X1!$ERB9F:*26=A4W*K:7.U!!!71(!!%A!"!!!+28:F&lt;H22&gt;76V:1!!&amp;E"Q!!A!!!!#!!!*6'BS:7&amp;E5G6G!%Y!]=ETD]E!!!!#&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"R!5!!#!!%!!AR&amp;&gt;G6O&gt;%BB&lt;G2M:8)!!&amp;A!]=ETD]M!!!!#&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T%%6W:7ZU3'&amp;O:'RF=CZD&gt;'Q!+E"1!!%!!RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!1!!!!$!!!!!!!!!!%!!!!#!!!!!!!!!!!!!!!!!2&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!*!)!!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!",!!!!!2&amp;5;(*F972J&lt;G=O&lt;(:D&lt;'&amp;T=V"53$!!!!!N!!%!"A!!!!F&amp;?'6D&gt;82J&lt;WY*6'BS:7&amp;E;7ZH%62I=G6B:'FO:SZM&gt;G.M98.T!!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="EventHandler.ctl" Type="Class Private Data" URL="EventHandler.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="handlers" Type="Folder">
		<Item Name="HandleAnalogInEvent.vi" Type="VI" URL="../HandleAnalogInEvent.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;C!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!W1(!!(A!!&amp;R6&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X-!&amp;56Y:7.V&gt;'FP&lt;E*B=W5O&lt;(:D&lt;'&amp;T=Q!.1!9!"UZV&lt;76S;7-!-%"Q!"Y!!"9528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-!!!^&amp;&gt;G6O&gt;%BB&lt;G2M:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!)!!E#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
		</Item>
		<Item Name="HandleStateMachineEvent.vi" Type="VI" URL="../HandleStateMachineEvent.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!O1(!!(A!!&amp;R6&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X-!$56Y:7.V&gt;'FP&lt;E*B=W5!$5!'!!&gt;/&gt;7VF=GFD!$"!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!028:F&lt;H2)97ZE&lt;'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!#!!!!!A!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">128</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074274816</Property>
		</Item>
		<Item Name="HandleAlarmEvent.vi" Type="VI" URL="../HandleAlarmEvent.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!O1(!!(A!!&amp;R6&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X-!$56Y:7.V&gt;'FP&lt;E*B=W5!-%"Q!"Y!!"9528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-!!!^&amp;&gt;G6O&gt;%BB&lt;G2M:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="EventTypes.ctl" Type="VI" URL="../EventTypes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="overridden" Type="Folder">
		<Item Name="Work.vi" Type="VI" URL="../Work.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B4&gt;'^Q4'^P=!!!-E"Q!"Y!!"9528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-!!""&amp;&gt;G6O&gt;%BB&lt;G2M:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"R!=!!)!!!!!A!!$V2I=G6B:&amp;*F:G6S:7ZD:1!Q1(!!(A!!&amp;B2&amp;&gt;G6O&gt;%BB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!!$U6W:7ZU3'&amp;O:'RF=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1124073600</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="InitLoop.vi" Type="VI" URL="../InitLoop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%@!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!(A!!&amp;B2&amp;&gt;G6O&gt;%BB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!!$U6W:7ZU3'&amp;O:'RF=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="EventHandler_Create.vi" Type="VI" URL="../EventHandler_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!I1(!!(A!!&amp;R6&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X-!"F"B=G6O&gt;!!!-%"Q!"Y!!"9528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-!!!^&amp;&gt;G6O&gt;%BB&lt;G2M:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">8</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="GetEventQueue.vi" Type="VI" URL="../GetEventQueue.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#B!=!!?!!!/4'&amp;C6EF&amp;6S"09GJF9X1!$ERB9F:*26=A4W*K:7.U!!!71(!!%A!"!!5+28:F&lt;H22&gt;76V:1!!-E"Q!"Y!!"9528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-!!""&amp;&gt;G6O&gt;%BB&lt;G2M:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$"!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!028:F&lt;H2)97ZE&lt;'6S)'FO!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">33554432</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="HandleEvent.vi" Type="VI" URL="../HandleEvent.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%@!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!(A!!&amp;B2&amp;&gt;G6O&gt;%BB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!!$U6W:7ZU3'&amp;O:'RF=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342713872</Property>
		</Item>
		<Item Name="StartThread.vi" Type="VI" URL="../StartThread.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!41!1!$5RP&lt;X"3982F)&amp;NN=VU!$U!$!!B1=GFP=GFU?1!!-%"Q!"Y!!"9528:F&lt;H2)97ZE&lt;'6S,GRW9WRB=X-!!!^&amp;&gt;G6O&gt;%BB&lt;G2M:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!)!!E$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!)!!!!#!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
		<Item Name="StopThread.vi" Type="VI" URL="../StopThread.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%@!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!(A!!&amp;B2&amp;&gt;G6O&gt;%BB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!!$U6W:7ZU3'&amp;O:'RF=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342976528</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%@!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!(A!!&amp;B2&amp;&gt;G6O&gt;%BB&lt;G2M:8)O&lt;(:D&lt;'&amp;T=Q!!$U6W:7ZU3'&amp;O:'RF=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
		<Item Name="RegisterEventSource.vi" Type="VI" URL="../RegisterEventSource.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!128:F&lt;H2)97ZE&lt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71#%137ZD&lt;(6E:5.I;7RE=G6O0Q!!,E"Q!"Y!!"=628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T!!V&amp;?'6D&gt;82J&lt;WZ#98.F!$"!=!!?!!!7&amp;%6W:7ZU3'&amp;O:'RF=CZM&gt;G.M98.T!!!028:F&lt;H2)97ZE&lt;'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!")!!!#3!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
</LVClass>
