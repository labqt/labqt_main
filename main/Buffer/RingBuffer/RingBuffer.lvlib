﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Example" Type="Folder">
		<Item Name="Double1DTest.vi" Type="VI" URL="../Double1DTest.vi"/>
		<Item Name="Waveform1DTest.vi" Type="VI" URL="../Waveform1DTest.vi"/>
		<Item Name="Double2DTest.vi" Type="VI" URL="../Double2DTest.vi"/>
	</Item>
	<Item Name="API" Type="Folder">
		<Item Name="GetBufferStatus.vi" Type="VI" URL="../GetBufferStatus.vi"/>
		<Item Name="SetBufferReads.vi" Type="VI" URL="../SetBufferReads.vi"/>
		<Item Name="SetBufferWrites.vi" Type="VI" URL="../SetBufferWrites.vi"/>
		<Item Name="Init.vi" Type="VI" URL="../Init.vi"/>
		<Item Name="Insert.vi" Type="VI" URL="../Insert.vi"/>
		<Item Name="Flatten.vi" Type="VI" URL="../Flatten.vi"/>
		<Item Name="Read.vi" Type="VI" URL="../Read.vi"/>
		<Item Name="GetFirstLast.vi" Type="VI" URL="../GetFirstLast.vi"/>
		<Item Name="ResetBuffer.vi" Type="VI" URL="../ResetBuffer.vi"/>
	</Item>
	<Item Name="Polymorphic" Type="Folder">
		<Item Name="Double1DInit.vi" Type="VI" URL="../Double1DInit.vi"/>
		<Item Name="Double2DInit.vi" Type="VI" URL="../Double2DInit.vi"/>
		<Item Name="Waveform1DInit.vi" Type="VI" URL="../Waveform1DInit.vi"/>
		<Item Name="Double1DInsert.vi" Type="VI" URL="../Double1DInsert.vi"/>
		<Item Name="Double2DInsert.vi" Type="VI" URL="../Double2DInsert.vi"/>
		<Item Name="Waveform1DInsert.vi" Type="VI" URL="../Waveform1DInsert.vi"/>
		<Item Name="Double1DRead.vi" Type="VI" URL="../Double1DRead.vi"/>
		<Item Name="Double2DRead.vi" Type="VI" URL="../Double2DRead.vi"/>
		<Item Name="Waveform1DRead.vi" Type="VI" URL="../Waveform1DRead.vi"/>
		<Item Name="Double1DFlatten.vi" Type="VI" URL="../Double1DFlatten.vi"/>
		<Item Name="Double2DFlatten.vi" Type="VI" URL="../Double2DFlatten.vi"/>
		<Item Name="Waveform1DFlatten.vi" Type="VI" URL="../Waveform1DFlatten.vi"/>
	</Item>
	<Item Name="DataType" Type="Folder">
		<Item Name="BufferControl.ctl" Type="VI" URL="../BufferControl.ctl"/>
		<Item Name="BufferDvr.ctl" Type="VI" URL="../BufferDvr.ctl"/>
	</Item>
</Library>
