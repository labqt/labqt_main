﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Examples" Type="Folder">
		<Item Name="DotNotation" Type="Folder">
			<Item Name="ClusterDotNotationExample.vi" Type="VI" URL="../Example Code/DotNotation/ClusterDotNotationExample.vi"/>
		</Item>
		<Item Name="Excel" Type="Folder">
			<Item Name="Configuration" Type="Folder">
				<Item Name="CalibrationResults.xlsx" Type="Document" URL="../Example Code/Configuration/CalibrationResults.xlsx"/>
				<Item Name="CalibrationSetup.xlsx" Type="Document" URL="../Example Code/Configuration/CalibrationSetup.xlsx"/>
			</Item>
			<Item Name="CalibrationResultsCluster.ctl" Type="VI" URL="../Example Code/Excel/CalibrationResultsCluster.ctl"/>
			<Item Name="CalibrationSetupCluster.ctl" Type="VI" URL="../Example Code/Excel/CalibrationSetupCluster.ctl"/>
			<Item Name="ExcelReadAndWriteFromCluster.vi" Type="VI" URL="../Example Code/Excel/ExcelReadAndWriteFromCluster.vi"/>
		</Item>
		<Item Name="IniFile" Type="Folder">
			<Item Name="Configuration" Type="Folder">
				<Item Name="ConfigExample.ini" Type="Document" URL="../Example Code/Ini/ConfigExample.ini"/>
			</Item>
			<Item Name="IniFileExampleCluster.ctl" Type="VI" URL="../Example Code/Ini/IniFileExampleCluster.ctl"/>
			<Item Name="IniFilePopulateClusterExample.vi" Type="VI" URL="../Example Code/Ini/IniFilePopulateClusterExample.vi"/>
		</Item>
	</Item>
	<Item Name="Find By Name" Type="Folder">
		<Item Name="GetControlByNameFromClusterStruct.vi" Type="VI" URL="../GetControlByNameFromClusterStruct.vi"/>
		<Item Name="GetControlFromCluster.vi" Type="VI" URL="../GetControlFromCluster.vi"/>
	</Item>
	<Item Name="Member Population" Type="Folder">
		<Item Name="PopulateClusterFromExcel.vi" Type="VI" URL="../PopulateClusterFromExcel.vi"/>
		<Item Name="PopulateClusterFromIni.vi" Type="VI" URL="../PopulateClusterFromIni.vi"/>
		<Item Name="PopulateExcelFromCluster.vi" Type="VI" URL="../PopulateExcelFromCluster.vi"/>
	</Item>
</Library>
