﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueFrame</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">9464978</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">7641720</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">11316396</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">2</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeBaseTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+W!!!*Q(C=T:3^D2MR%)8@W1&lt;MT&amp;!&amp;.N4#J!;=K!!FSC[?&amp;B2M9)&gt;K96I1V)'3+W";%/!+")=8,(&lt;^,58^XUE/$0B)T@[].RR_)IG63PME@&gt;$DP&gt;&lt;=&lt;)_0@&gt;]XN@.5&lt;`M9`+&lt;:_]X_UOR%@M0YZHR]_:5YD$]:&gt;TX_2&lt;`P^`Y^`HPN_7;\^+@47`ZUV;Z7U^&gt;]X!(J,/0I6`=CY_!@X6X'J&gt;_?_&gt;WVPTJ7[,KO@='P'6XXKE`'KLPJ6`PN_;MTPTS?LX`&lt;(PTB&lt;^;-U`VL&gt;`[*?\H`R7B72`PK`,2H\L6@31]P^]`HH=:XY'@`HZPU)!XXPE]C##&gt;-A^5H?K)H?K)H?K)(?K!(?K!(?K!\OK-\OK-\OK-&lt;OK%&lt;OK%&lt;OK'8DCZUI1O&gt;65EG4S:+CC9&amp;EG21F&amp;Q3HI1HY5FY?&amp;8#E`!E0!F0QE/+%J[%*_&amp;*?")?BCHB38A3HI1HY;&amp;5)=H3U?&amp;*?#CPA#@A#8A#HI#(+28Q"!$":%(BI!A9#MTA)?!*?!)?(B8Q"$Q"4]!4]'!LY!FY!J[!*_"B3&amp;G6+$2$2Y?(-H*Y("[(R_&amp;R?#ANB]@B=8A=(I?([?4Q/$Q/B$/B5RQ%/9/="/@&amp;Y8&amp;YO-HB=8A=(I@(Y=%K/_2F:1;;I;0$9`!90!;0Q70Q5%)'D]&amp;D]"A]"A^F:@!90!;0Q70Q-*5-(I0(Y$&amp;!D%G:8E9R9[#2:!A'$\^S7KTM5B13+VWKBV@V5+I?.N6$J(IY6$&gt;&gt;&gt;4.6.UFV]658687R6"&gt;"^=_J1KP#K%[C/HB)V*&lt;LBFA43W*"T)E:-3('R'A9_I]4N^ON.JO.VOOVFMOF&amp;IO&amp;ZP/Z:L/:*J/*RO/R2K/2^OUL@&gt;=?BO`3]0#D\\P81K@N`:0U_&lt;@UP;V,`V@RCQG?^98\R`Z*$]03@4ON_A;_D8KHX@WQ2H]!XO+&gt;'1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.4</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#!D5F.31QU+!!.-6E.$4%*76Q!!']A!!!2&lt;!!!!)!!!'[A!!!!&lt;!!!!!2:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!!!*!5!)!!!$!!!!A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!DJ*F)B,QIU?&lt;264:,,]:*!!!!!Q!!!!1!!!!!*L:5&lt;&lt;F$/Z"F]=Q@MX527,5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!$@=Q$M^PJ\3JEEI)D0N]O%!1!!!0````]!!!!1`S&lt;J\IAT6(ZC\`EA\I:7&amp;A!!!!1!!!!!!!!!DA!"4&amp;:$1Q!!!!)!!F:*4%)!!!!!5&amp;2)-!!!!!5!!1!"!!!!!!)!!F:*1U-!!!!!!2209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!"16%AQ!!!!'Q!"!!-!!"209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!!!!)!!@]!!!!"!!%!!!!!!!1!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!)!!A!!!!!!+A!!!#:YH'0A:7"O9,D!!-3-$EQ.4"F!VA='"AY'$A%/"CA-9!!!D6-&amp;ZA!!!!!!31!!!2BYH'.AQ!4`A1")-4)Q-,U!UCRIYG!;RK9GQ'5O,LOAYMR1.\,#B)(OXA/EG5"S5$5#%#GG'U"]!NU=@CA^!UE-!+.$+05!!!!!!!!-!!&amp;73524!!!!!!!$!!!$&gt;Q!!"O2YH)V6TWM412C&gt;D&lt;NRKZ&amp;&gt;)=C+/13&gt;1^695FV+CTGE.M7I23O77FJ2Q2^5564MI5A0F=U+[Z!7I1@&amp;@]#42`(A)8&amp;L5^'4HN24+5C$(B4";XT@TK[*WKI,G\=T_XXP?W`GG]UDD&lt;&amp;,_\K\T$BD[R4'EGS;H&lt;NW`M)GBD'4V_MY_XHVB&gt;B:]1-]I=CR]TEB#NRUJLA6-^QE:M1!4XI$8065PB*%T#&gt;]5&lt;R3[]X23/C9LR7Y4A/A3NAY0EZ2SQ'@#&lt;[EG*Z%!1^R'+7"+D!F5R/5ML)RI$:*T4?1H4AZ\(QO)GA%Q16I/!5.?4THA.W#?1LG=O6?F;91VOV5[,&amp;&lt;X&amp;$&amp;&amp;$`L@D&gt;+A@CO]F%6'6GRR2PC&gt;HW$6'E(VN])X8XJP4*+*&amp;JM&amp;&gt;N[XBOFP8AO$X!&lt;3?X!&gt;G##&gt;(MZUKKWE6&lt;+&gt;CO'?Q??K$&lt;R'#7.7.K=+N8,#&amp;2$2?[_-79/MC!M#^5N[_AN1+4&lt;*SGS5HY7JD*."HYYJ*CN.BK.I"+:0KZC/N=U`,P:6'!W(:F.`^.M'EH*MNTH6L-&lt;7]U_9'O:N3!VD9JGC^H5;G;(QX&gt;3?AJGL#;$?4CE_*P:1Q3KF_#2L%+F(C0_,]Z]]@3:-&gt;]0_XI1"0LUZ!YG$8&gt;72PX;N"UW?;2KV'&gt;P7&gt;4XZBK&gt;'48X3FP5I=PLQ`\-)X1Q;-!"0E2^C7=\[$:T`W:D\LHB\K%&amp;(C1,NH3;&amp;8E6MEMJ7IKMC#-DAZM\#WK&gt;ZIQHT_NXF;!K$[NHZ$HV&amp;E1&lt;L@RWSOQ22E`6]YUS(7HK&amp;O_65V.X,B*6N/]S\Y0\Q:D:44FR9&lt;2O#?WJB5L)1`%D#*N^*R??Y^5=XD&lt;E&gt;NHF)CGX94,\KYFEUY4FU6=$*J+SO"G+N`Z$P.55&lt;U&lt;CL8_,(W/LC4&gt;$]1@_&amp;#^\C=24(`X+BJ1HCU_J0YT_;FVP4F;=_4QV&amp;HW8AD\21767FV2R_QM/1@E?`1I[.U/[63PIG3W)5B:&lt;W^3:V`W_M07'C7##GZY&gt;H$!F/G%C8MOXZ`'FLO74:A$DYRL":*%7*(`^%5:0[1&gt;MJXUWS/2X@2&gt;&lt;`&lt;K)/]=5&gt;B562I#0A:?"IT%Z@AC=!([.S@F/E(5!/X";&amp;';R4]#&lt;?/L2Z0C_*PG?!&gt;O"*(Q^CQ7V&amp;+;T#?W7^E*\L&lt;X6XGN,WE&gt;N$6H`&gt;@5&gt;+`3XDK0`L2]#9LUX!!!!!"-!!!!*?*RD9'"A:'1!!A!!&amp;!!$!!!!!!Y5!9!H!!!'-41O-#YR!!!!!!!!$"1!A!!!!!1R.#YQ!!!!!!Y5!9!H!!!'-41O-#YR!!!!!!!!$"1!A!!!!!1R.#YQ!!!!!!Y5!9!H!!!'-41O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!(````Z````_@````H````Z````_@````H````Y!!!!!!!!!!!!!!!!!!]!!!!`Q!!!``!!!``]!!0``!!$``Q!!``]!!0``!!$``Q!!``]!!0``!!$``Q!!``]!!0``Y!"```A!(``A!!@`Q!!"`Q!!!$Q!!!!!!!!!!!!!!!)!X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X@`^`@X^``X@X``@`^``X&gt;X^X@X^`@X@X^X^X^X@X@X&gt;`^X^`@X``&gt;`&gt;`&gt;`^X``&gt;X@X&gt;X^`&gt;`&gt;`@X@X@X&gt;`&gt;`&gt;X^X&gt;`@X@X@X^X^X``@X@X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X-T-T-T-T-T-T-T-T-T-T&gt;T-T-T-T-T-T-T-T-T-T-X=T-T-T-T-```-T-T-T-T.X-T-T-T-`^X&gt;`]T-T-T-T&gt;T-T-T-`^X&gt;X&gt;X`T-T-T-X=T-T-`^X&gt;X&gt;X&gt;X@`-T-T.X-T-T0`&gt;X&gt;X&gt;X&gt;X`T-T-T&gt;T-T-T``^X&gt;X&gt;X``]T-T-X=T-T-````X&gt;X````-T-T.X-T-T0``````````T-T-T&gt;T-T-T``````````]T-T-X=T-T-```````````-T-T.X-T-T0``````````T-T-T&gt;T-T-T``````````]T-T-X=T-T-```````````-T-T.X-T-T0````````````T-T&gt;T-T-T0`````````````-X=T-T-T-```````````-T.X-T-T-T-T```X````]T-T&gt;T-T-T-T-T0X````]T-T-X=T-T-T-T-T-T``]T-T-T.X-T-T-T-T-T-T-T-T-T-T&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;U!!!1!8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\8&amp;T```^\`XP`?`^\````?XP`?````XP```^\````?XN=80^\?XP`?`^\`XP`?XP`?`^\?`^\?`^\?XP`?XP`?VR=``^\?`^\`XP`?````XN\`XN\`XN\``^\?````XN\8&amp;T`?XN\?`^\`XN\`XN\`XP`?XP`?XP`?XN\`XN\`XN=80^\?XN\`XP`?XP`?XP`?`^\?`^\?````XP`?XP`?VR=?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=80DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_&amp;R=_0DY_0DY_0DY_0DY_0B150DY_0DY_0DY_0DY_0DY8&amp;TY_0DY_0DY_0DY_0B1L0SML&amp;$Y_0DY_0DY_0DY_0B=80DY_0DY_0DY_0B1L0R\?XN\L+R1_0DY_0DY_0DY_&amp;R=_0DY_0DY_0B1L0R\?XN\?XN\?[SM50DY_0DY_0DY8&amp;TY_0DY_0DYL0R\?XN\?XN\?XN\?XOML0DY_0DY_0B=80DY_0DY_0D]`(N\?XN\?XN\?XN\?`[M_0DY_0DY_&amp;R=_0DY_0DY_0SML0R\?XN\?XN\?`\_`PTY_0DY_0DY8&amp;TY_0DY_0DY`+SML+T]?XN\?`\_`P\_`0DY_0DY_0B=80DY_0DY_0D]L+SML+SM`+T_`P\_`P\]_0DY_0DY_&amp;R=_0DY_0DY_0SML+SML+SM`P\_`P\_`PTY_0DY_0DY8&amp;TY_0DY_0DY`+SML+SML+T_`P\_`P\_`0DY_0DY_0B=80DY_0DY_0D]L+SML+SML0\_`P\_`P\]_0DY_0DY_&amp;R=_0DY_0DY_0SML+SML+SM`P\_`P\_`PTY_0DY_0DY8&amp;TY_0DY_0DY`+SML+SML+T_`P\_`P\_`0DY_0DY_0B=80DY_0DY_0CML+SML+SML0\_`P\_`KSML+SM_0DY_&amp;R=_0DY_0DY_0D]`+SML+SM`P\_`KSM`+SML+SML0DY8&amp;TY_0DY_0DY_0DY`+SML+T_`KSM`+SML+SML0DY_0B=80DY_0DY_0DY_0DY_0SML+SM?[SML+SML+TY_0DY_&amp;R=_0DY_0DY_0DY_0DY_0D]?[SML+SML+TY_0DY_0DY8&amp;TY_0DY_0DY_0DY_0DY_0DY_+SML+TY_0DY_0DY_0B=80DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8!!!!)9!!5:13&amp;!!!!!"!!*52%.$!!!!!2209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!"16%AQ!!!!'Q!"!!-!!"209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!!!!)!!0]!!!!"!!%!!!!!!!1!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!"&gt;16%AQ!!!!!!!!!!!!!Q!!!!!(&amp;Q!!&amp;RJYH-V9;WA56R1_&gt;T**:D=0:Z.M.KN*&gt;L0/&lt;B+43+J#&lt;&lt;1P(3U_C)_9VAKFROR;5W)DGYVJ%;)`NI%5J)BN@B3%^I^1+"5KJ95KC%:47+K7UN:KWT8U!=7I_2'.7#@4=W&gt;W&gt;G&gt;W:R].%&gt;TA-)TXX(0O/&gt;`XH4-$Y+DEKZB:'*+!]0@Q:L-%6H_%!)SV="$\_9;"\S4`!KFQ%AF?YTLZ']QMK:;AW"`R=;XC#%TB;PG#P!LOEU&amp;_!J=7]%\=T#L"!H`%9&gt;MI2(HB6,5QEK`N;I-;`CC::89+LBHO;,A0(5+YA6ZN,717C&amp;D(MG(0VK[X!G'"0L7U=%ZF3YM%P"AJ#1L2?NQ28:^8NG3;C)&gt;:L7U*O'5$H$Z^/G&amp;E5YV]3BALU9:Y!-BSJCG$4:E9K?E8IMW+D67R14`,.4_C;`9YD:U;*:O7CR%?4&gt;(O\&gt;C2K;VG*\VA;L&gt;0AAIBOI:T=B0_[[2E7`AL)%$'8O@EM`*\V.\74KOAL#X$1HD&lt;#._"^RU36)5DT'ZAK;^]K#$V=%MJ![O693%NQXKF$":RB)&gt;%(9ZHLI/&lt;!&lt;&amp;/:-=].7N\"`J$A;#\&lt;[_\O\?LP^^^).BTM#M5=0O\1FWJ66IE2JR0UQR1BQJ!Q!%MK95@^"HPAZ-H4W)3]*IQL5:4BR#.WZ84\-(0];T\%^GD8B0:K](M?6]&amp;\SY1(_RYA_&lt;2W]99U?N)I.?.[,8RS=%:U/O:(`4S8L6O_(]A."!-T+N0V7,%W6!3I/%++93&lt;'=!JK%9[1"&gt;CGKLA3A9&lt;,^I=.A#[%0V5;8YS!&gt;K8#OB#T6==U-GVR*,5RUOCQJIF2)6V_+L]P4S*Y)93_8=Y#Z`JQ/X(/K%`NV*]_2&amp;:#"'F,HF;8:IU/)OH8"O.?.[6"=]&amp;-4TT7`;]'?A/P2A+"8PW$)1#`5E1RNC&lt;-8&lt;R1=GGW"/EW13F'=_+5&gt;&gt;'$:AU#Q;K`5&amp;D;T&gt;14&lt;Y.)'-;=QJ.]%!KG`Z-:2/,@V/9G'RM_CO*4@1XH3B&gt;?D9N-W&amp;4HM9G6G(4N(*^2+^EE8*&gt;G?$80Q:_;?%;_(6L`LP$J"G:RG%3LG5AROVE-O&amp;[!!H'-^D=3394WISDT&lt;8M:,K&lt;1C:KGU3GU&gt;(2Z/YQF:Z-E#^0UUYR*)`$4:B3S+2#=BM7"@UJ&amp;"G5J_%4W'EI!GF,WRP%,&amp;7A0,+]&amp;/Q&lt;/.$?N&gt;^5[RC$VB6*:"7CM5W),F"^=&gt;28*3,$!V9]]C`8@]5D$QR`&lt;(/JY;M*G@G7WM@ORX8XFX4X&amp;TGHR:8QMVLV=V$V=UZF+QM_R1^[G"=`G,RHU1]3J5B(4,PC:\&gt;74NGGF00BQY@I%[^K]M*V=!2",.!H/PC3ZZ,9;M/5,A&amp;`6L;3Z`6MP?A^R+C%:8X$?&lt;&amp;&amp;WZ'D'B$+%1C$])U#")NWFBJ4)$2C$)&gt;S5#Y7Q6#[.&gt;BX)"!-P@.S6__!*KG:%,&amp;6)L8_3,%TC*+[2DB6+YSMI&amp;Z,F6-X+O(F+QNP3A4\.?P%'=[KSX5B"F=&amp;XBRST7CZ$I?]3VPXRPQ83[2/L7#?MKO&lt;;/[LI22X/H0G$/[%6Q5J$$V%HHP-J2+E&amp;*?YF36V^-J\9D8;QXF5Q#1PJ:3OIV@DUDC?-"K0'AWD2.-;ZW,JX+0JTDW;&lt;E-UC%;=4*S$/D4G11EGJD%\'A5^'HN5+/&lt;\BJH%VP7Y^3N#N"90'F._N?D.RI;7KJ[E!3X\D&lt;X1$CWQ'SYEQ3#V':,'?(?Q;.X"$F]E&lt;&amp;7%*TF=%O]/FFBXW+,X:WL4&amp;/]/FFBXW',U=S`&gt;]:LR?*U'"7A%@[)\J/`8X_FTPE`.?1(38XF(/#_26HSPYS%Z:@L:B3R,^/0Y\(*8A5S7W18:(Z^&lt;-P%&gt;@3R065Q'#G!$@+#ZO'^-49,%+[BAYB/^9+YQ'7]WQ;AB8,-XL2^VW3K_IW;LE)JFB_])+4B7:K?(%*RAN:&gt;6;L?6:1\NVB%868A5%V6Z!E7V(%9-IAKT;56V+%&gt;2,&gt;&amp;%F4&lt;:H$16Z&amp;QVF4Q?477?+%X.?[)UF:WLJO;&lt;;#JHU&amp;2OTJJK-&gt;@5J8!MO[:;T44V3-,76"_,5D3V2O`0V+9Y26.LD(\3;GK*C;9OAQ_T;_LH*JJKU416*WM&lt;Z8ZSRAT4&gt;@HDFF1=&gt;CN3B_J](%*N]TV5WV/(;A&lt;^W/&gt;\K+Z-&lt;2'MYG?/1\8$J%=Y=RCKY7PTI&gt;L[``N%*\YMCR&amp;&amp;@/1PZ7FZ!JYR@D;Y&lt;^IH#":R89Z@.)JW_0@XL_`J$7Q0\%V^,ZY2)V60#6'#W^/N&amp;_0746#BJ7!GEPK#_U$^ZG$28E/;Q2\0W'3[NLJ,F\'3$^6U&amp;@G'-;?7&gt;JU+=4=O$8$U!R$^J[YKRF8P6FT_N+(D\`@.6E0MRX5)IJRJ!&lt;]/1TEMA6V=7X;/XU!LQ7XAVP%HG&amp;F#*+DE4H$XRH[+@[]?WRSX$&amp;]FPZ(,QE=OLOI`"&gt;#:ZQ!!!!!%!!!!&gt;Q!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!'5!!!"V?*RD9'!I&amp;*"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```V=J9O4Y?O1;8.%2(TB4::9]BQ1!:1A:GA!!!!!!!!1!!!!(!!!%&gt;!!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S&amp;!#!!!!!!!%!#!!Q`````Q!"!!!!!!$G!!!!#!!31(!!)AN5:'VT2GFM:6*F:A!)!$$`````!"J!1!!"`````Q!"$6"S&lt;X"F=H2Z4G&amp;N:8-!"!"4!"R!1!!"`````Q!$$F"S&lt;X"F=H2Z6G&amp;M&gt;76T!!!31$$`````#5&gt;S&lt;X6Q4G&amp;N:1"9!0(/K3B4!!!!!B:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#2!5!!%!!!!!A!%!!514W*K:7.U182U=GFC&gt;82F=Q!!)%"1!!%!"B:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!"!!=!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S&amp;!#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!&amp;!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N="1!A!!!!!!"!!5!"Q!!!1!!TKEI7!!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!5!)!!!!!!!1!&amp;!!=!!!%!!-[J+&amp;A!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D&amp;!#!!!!!!!%!#!!Q`````Q!"!!!!!!$G!!!!#!!31(!!)AN5:'VT2GFM:6*F:A!)!$$`````!"J!1!!"`````Q!"$6"S&lt;X"F=H2Z4G&amp;N:8-!"!"4!"R!1!!"`````Q!$$F"S&lt;X"F=H2Z6G&amp;M&gt;76T!!!31$$`````#5&gt;S&lt;X6Q4G&amp;N:1"9!0(/K3B4!!!!!B:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#2!5!!%!!!!!A!%!!514W*K:7.U182U=GFC&gt;82F=Q!!)%"1!!%!"B:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!"!!=!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:21!A!!!!!!"!!5!!Q!!!1!!!!!!%!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%5!)!!!!!!#!!31(!!)AN5:'VT2GFM:6*F:A!)!$$`````!"J!1!!"`````Q!"$6"S&lt;X"F=H2Z4G&amp;N:8-!"!"4!"R!1!!"`````Q!$$F"S&lt;X"F=H2Z6G&amp;M&gt;76T!!!31$$`````#5&gt;S&lt;X6Q4G&amp;N:1"9!0(/K3B4!!!!!B:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#2!5!!%!!!!!A!%!!514W*K:7.U182U=GFC&gt;82F=Q!!)%"1!!%!"B:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!"!!=!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!$!!1!!!!"!!!!4]!!!!I!!!!!A!!"!!!!!!7!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!8Y!!!-#?*S&gt;5&gt;N/QE!1077BA!AC)CI#&amp;G+-]9'9_!-V*PCG$2DFQ1=,X:K;ED&lt;NFOC&lt;P_00_#X["4J&gt;,B*-*,'4T-\VT-QJA":+OI^7\M9;B2X(Z6VO)Y04,`J1V85IUF,S2O$Z0"!P6_;)BUCCB^I]SQKT\+XJ2J1G4)G1P1S]S)^&lt;U-@H_^NR$U#C-JNV&amp;TC#"WVX0(4.-#R@$Z\Y5*Q,%4C$30#Q021O$H7$JF%8[62RO13YF\D^0X",3_%9^921&amp;;DV#T=++;:ZNC:L.4^QRK&lt;AGG5+%SE3L-%#F!]C*15W&gt;8&amp;'&lt;V:0A^HO)T,[+`/[9B,AHIUCI4/M)Y]#CSQ&lt;+IBB]O&gt;M,NA-H8]S5U,-4"'&lt;:"'CP'C,C7?,&gt;D/)LQW5I;VA\UBW66&lt;SE!!J.69+C)E(AET3YA9U\'+0)L&amp;5;@"--F0Z(@H*\%O:%+KA.FVGV5_*TSFAY3S3.!B)J3?,(,;RATI;]&lt;J5I&amp;!*QQ(Z$6IWB;&lt;UGT)$K.^&gt;K+ZF!!!!!!#*!!%!!A!$!!9!!!"I!!]%!!!!!!]!W!$6!!!!=1!0"!!!!!!0!.A!V1!!!(I!$Q1!!!!!$Q$9!.5!!!#$A!#%!)!!!!]!W!$6!!!!B9!!B!#!!!!0!.A!V1!!!)?!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!%R!4)!!!"35V*$$1I!!UR71U.-1F:8!!!&lt;S!!!"&amp;M!!!!A!!!&lt;K!!!!!!!!!!!!!!!)!!!!$1!!!2)!!!!(5R*1EY!!!!!!!!"&lt;%R75V)!!!!!!!!"A&amp;*55U=!!!!!!!!"F%.$5V1!!!!!!!!"K%R*&gt;GE!!!!!!!!"P%.04F!!!!!!!!!"U&amp;2./$!!!!!!!!!"Z%2'2&amp;-!!!!!!!!"_%R*:(-!!!!!!!!#$&amp;:*1U1!!!!!!!!#)%&gt;$2%E!!!!!!!!#.(:F=H-!!!!%!!!#3&amp;.$5V)!!!!!!!!#L%&gt;$5&amp;)!!!!!!!!#Q%F$4UY!!!!!!!!#V'FD&lt;$1!!!!!!!!#['FD&lt;$A!!!!!!!!#`%R*:H!!!!!!!!!$%%:13')!!!!!!!!$*%:15U5!!!!!!!!$/&amp;:12&amp;!!!!!!!!!$4%R*9G1!!!!!!!!$9%*%3')!!!!!!!!$&gt;%*%5U5!!!!!!!!$C&amp;:*6&amp;-!!!!!!!!$H%253&amp;!!!!!!!!!$M%V6351!!!!!!!!$R%B*5V1!!!!!!!!$W&amp;:$6&amp;!!!!!!!!!$\%:515)!!!!!!!!%!!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!`````Q!!!!!!!!#U!!!!!!!!!!$`````!!!!!!!!!-A!!!!!!!!!!0````]!!!!!!!!!U!!!!!!!!!!!`````Q!!!!!!!!&amp;E!!!!!!!!!!$`````!!!!!!!!!7Q!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!!`````Q!!!!!!!!(M!!!!!!!!!!$`````!!!!!!!!!@Q!!!!!!!!!!0````]!!!!!!!!&amp;?!!!!!!!!!!%`````Q!!!!!!!!71!!!!!!!!!!@`````!!!!!!!!";1!!!!!!!!!#0````]!!!!!!!!&amp;N!!!!!!!!!!*`````Q!!!!!!!!8)!!!!!!!!!!L`````!!!!!!!!"&gt;A!!!!!!!!!!0````]!!!!!!!!&amp;\!!!!!!!!!!!`````Q!!!!!!!!9%!!!!!!!!!!$`````!!!!!!!!"BA!!!!!!!!!!0````]!!!!!!!!'H!!!!!!!!!!!`````Q!!!!!!!!CA!!!!!!!!!!$`````!!!!!!!!$+1!!!!!!!!!!0````]!!!!!!!!.-!!!!!!!!!!!`````Q!!!!!!!"2-!!!!!!!!!!$`````!!!!!!!!&amp;&amp;1!!!!!!!!!!0````]!!!!!!!!58!!!!!!!!!!!`````Q!!!!!!!"2M!!!!!!!!!!$`````!!!!!!!!&amp;.A!!!!!!!!!!0````]!!!!!!!!5Y!!!!!!!!!!!`````Q!!!!!!!"F9!!!!!!!!!!$`````!!!!!!!!'7!!!!!!!!!!!0````]!!!!!!!!:;!!!!!!!!!!!`````Q!!!!!!!"G5!!!!!!!!!)$`````!!!!!!!!'RA!!!!!%F2E&lt;8.';7RF6X*J&gt;'6S,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!!!!%!!1!!!!!!"!!!!!!)!"*!=!!C#V2E&lt;8.';7RF5G6G!!A!-0````]!'E"!!!(`````!!%.5(*P='6S&gt;(F/97VF=Q!%!&amp;-!(%"!!!(`````!!-/5(*P='6S&gt;(F797RV:8-!!"*!-0````]*2X*P&gt;8"/97VF!&amp;A!]=[J+&amp;-!!!!#&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!*%"1!!1!!!!#!!1!"2"09GJF9X2"&gt;(2S;7*V&gt;'6T!!"=!0(/K3B9!!!!!B:5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T%F2E&lt;8.';7RF6X*J&gt;'6S,G.U&lt;!!K1&amp;!!!1!'(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!"Q!!!!5!!!!!!!!!!1!!!!)!!!!$`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!2*';7RF6X*J&gt;'6S,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!"%!A!A!!!!!!!!!!!!#!!!!%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!!"*';7RF6X*J&gt;'6S,GRW9WRB=X-</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!2*';7RF6X*J&gt;'6S,GRW9WRB=X-!5&amp;2)-!!!!#1!!1!%!!!+2GFM:6&gt;S;82F=B*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="TdmsFileWriter.ctl" Type="Class Private Data" URL="TdmsFileWriter.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="Open_Impl.vi" Type="VI" URL="../Open_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*$!!!!$A!Q1(!!(A!!'":5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"L1"9!"A2P='6O"X*F='RB9W5'9X*F982F$G^Q:7YA&lt;X)A9X*F982F%8*F='RB9W5A&lt;X)A9X*F982F)X*F='RB9W5A&lt;X)A9X*F982F)(&gt;J&gt;'AA9W^O:GFS&lt;7&amp;U;7^O!!!*&lt;X"F=G&amp;U;7^O!#^!&amp;A!$#H*F971P&gt;X*J&gt;'5*=G6B:#VP&lt;GRZ#H&gt;S;82F,7^O&lt;(E!"G&amp;D9W6T=Q!!%E!Q`````QBI&lt;X.U4G&amp;N:1!!#U!'!!2Q&lt;X*U!!!.1!-!"X2J&lt;76P&gt;81!"!!!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!=!#!!*%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!-E"Q!"Y!!"A76'2N=U:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!71&amp;!!!Q!(!!A!#1FF=H*P=C"P&gt;81!N!$Q!"Q!!!!"!!)!!Q!%!!5!"A!+!!9!"A!'!!9!"A!'!!9!"A!'!!9!"A!'!!M!"A!'!!9!"A!'!!9!$!-!!2A!!*)!!!!)!!!!#!!!!AA!!!!)!!!!#!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!U(!!!!!1!.!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Close_Impl.vi" Type="VI" URL="../Close_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%6!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!!Z';7RF5G6B:'6S)'^V&gt;!!!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!$"!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!#1!!!!!!!!!!!!!!C1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="WaveformArray_Impl.vi" Type="VI" URL="../WaveformArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!!Z';7RF5G6B:'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!11&amp;1!!QBX98:F:G^S&lt;1!!'E"!!!(`````!!=.6W&amp;W:7:P=GV"=H*B?1!Q1(!!(A!!'":5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!A!#1-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!))!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="Flush.vi" Type="VI" URL="../Flush.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%H!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!"*5:'VT2GFM:6&gt;S;82F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"A76'2N=U:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!%62E&lt;8.';7RF6X*J&gt;'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="SetProperties.vi" Type="VI" URL="../SetProperties.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;J!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!"*5:'VT2GFM:6&gt;S;82F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!"!"4!"R!1!!"`````Q!($F"S&lt;X"F=H2Z6G&amp;M&gt;76T!!!)!$$`````!"J!1!!"`````Q!*$6"S&lt;X"F=H2Z4G&amp;N:8-!.%"Q!"Y!!"A76'2N=U:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!%62E&lt;8.';7RF6X*J&gt;'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!A!#A!,!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!##!!!!AA!!!#3!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="SetGroupName.vi" Type="VI" URL="../SetGroupName.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!"*5:'VT2GFM:6&gt;S;82F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%E!Q`````QF(=G^V=%ZB&lt;75!.%"Q!"Y!!"A76'2N=U:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!%62E&lt;8.';7RF6X*J&gt;'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!AA!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="IsOpen.vi" Type="VI" URL="../IsOpen.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!J!)160='6O0Q!W1(!!(A!!'":5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!36'2N=U:J&lt;'68=GFU:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$2!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!"&amp;5:'VT2GFM:6&gt;S;82F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">50331776</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714386</Property>
		</Item>
		<Item Name="TdmsFileWriter_Create.vi" Type="VI" URL="../TdmsFileWriter_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'7!!!!#Q!M1(!!(A!!'":5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!*&lt;W*K:7.U)'FO!#B!=!!?!!!8&amp;56Y:7.V&gt;'FP&lt;E*B=W5O&lt;(:D&lt;'&amp;T=Q!'5'&amp;S:7ZU!!!31$,`````#%:J&lt;'61982I!!!%!!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!)%"1!!-!"!!&amp;!!94:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!O1(!!(A!!'":5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!+&lt;W*K:7.U)'^V&gt;!!!&amp;E"1!!-!"!!&amp;!!9*:8*S&lt;X)A&lt;X6U!*E!]!!5!!!!!1!#!!-!!Q!(!!-!!Q!$!!-!!Q!$!!-!!Q!)!!-!!Q!$!!-!#1-!!2!!!!I!!!!+!!!!#!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!!."1!6!!!!!!!!!!!!!!!!!!!"!!!!!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%8!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!!JP9GJF9X1A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!!FP9GJF9X1A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!*!!!!!!!!!!!!!!#*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
		<Item Name="WriteChannelProperties.vi" Type="VI" URL="../WriteChannelProperties.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'.!!!!$A!Q1(!!(A!!'":5:'VT2GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A!%!!!!-E"Q!"Y!!"A76'2N=U:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!)!$$`````!"J!1!!"`````Q!$$6"S&lt;X"F=H2Z4G&amp;N:8-!"!"4!"R!1!!"`````Q!&amp;$F"S&lt;X"F=H2Z6G&amp;M&gt;76T!!!51$$`````#U.I97ZO:7R/97VF!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!A!#1!+%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!#!!*!!I*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!%!!%!"A!"!!=!!1!,!!%!!1!"!!Q$!!%)!!#1!!!!!!!!!!!!!!!!!!!!D1!!!!A!!!!!!!!!#!!!!!!!!!!)!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!$1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="GetFileSize.vi" Type="VI" URL="../GetFileSize.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"!!0=WF[:3!I;7YA9HFU:8-J!$:!=!!?!!!9&amp;F2E&lt;8.';7RF6X*J&gt;'6S,GRW9WRB=X-!!"*5:'VT2GFM:6&gt;S;82F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!.%"Q!"Y!!"A76'2N=U:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!%62E&lt;8.';7RF6X*J&gt;'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">34079248</Property>
		</Item>
	</Item>
</LVClass>
