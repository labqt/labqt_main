﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="11008008">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueFrame</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">9464978</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">7667498</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16860</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">2</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeBaseTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">03d867c1-7dbc-466f-a35f-09c84e180c1b</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,&lt;!!!*Q(C=T:3R&lt;B."%)&lt;`!!5.1HE$:&amp;&amp;2U)Q%*9X,6%DOK+&gt;/F^+5?9!UY_K+6)Y,?D=]Q&amp;"9IEBD]1)IIA@Z_'[^NG-HM2MEMHPDO`P`W&gt;H0O[O43HMO0&gt;/H1_VU&lt;].PO&lt;7LXJ&lt;HNDRV,ZW`@,H6VV?\(.`OD#`8:PTJ/PO"]@@[\=I`Q(_I@&gt;P&lt;&gt;PW4EXX_34.KGJ/(@.Q/;3NDYV&gt;X*W0N&lt;^RFRKY`WP*H&gt;`VG5W%WGYXO]7P'&lt;0;A4U9TW_N8_`(ZT:&lt;@.*0*:(P^2[/VT^_=F,;^@[/F@]P&gt;X@_3=4L:W(@/TWD,P?P4&lt;LHX_FPNY0(G/`#Z`=^./J+[?^MG%914JMZK%TX2%TX2%TX2!TX1!TX1!TX1(&gt;X2(&gt;X2(&gt;X2$&gt;X1$&gt;X1$&gt;X13U=8ON#&amp;TKIEES=4*574!EES+%J_%J[%*_&amp;*?(B6QJ0Q*$Q*4]*$CB+?B#@B38A3(I9JY5FY%J[%*_'B6#(*UN(B38AILY!HY!FY!J[!BSE6]!1!Q72"Y;!)'!L-Y#(A#8A#(BY6]!1]!5`!%`"A+_!*?!+?A#@A95B:F3AU85?(BT*S?"Q?B]@B=8AI,9@(Y8&amp;Y("[(B_HE]$A]$I1TI6-="$G$H!4HR?&amp;R?,D*Y8&amp;Y("[(R_("+DPE:75[GK[DQW0Q'$Q'D]&amp;D]&amp;"#"I`"9`!90!90:78Q'$Q'D]&amp;D]$#6$"[$R_!R1)R*G6Z'-7/AE71)"A^8/3V7&gt;CE+C:5OV=/L?CB6$ZPK)6)^(+K&lt;LLK:KJOEOPCKC[K[7+K,I0LH6+&amp;6961H52X=*?K'XTER*=&lt;%/8&amp;'$)A_U3//O[(`/0(GZE&lt;T_6T4[64D]6DHZ_=[/TP49$"1P^^8L^@4]@'R6OU6@&gt;G/OO`33OZ&gt;P&lt;V_.\[]?(0V_LLX]@+#_,\SBGW\'(&lt;^^Y`&amp;]/@\R@$,C]8Q[^.F`(KZ'0\ZM#AZ"Y0R\8$^8MM`AG_DHGBZ8[`28Z"^4)M!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.5</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.CoreWirePen" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!6*0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DEX.$!Z-D1],V:B&lt;$Y.#DQP64-S0AU+0&amp;5T-DY.#DR/97VF0E*B9WNH=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0DAW/41W.D)],V:B&lt;$Y.#DQP64-S0AU+0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z';7RM)&amp;"B&gt;(2F=GY],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$!],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-DQP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$-],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.4QP4G&amp;N:4Y.#DR797Q_-D5V0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$9],UZB&lt;75_$1I]6G&amp;M0D)V.4QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YS.45],V:B&lt;$Y.#DQP64A_$1I],U.M&gt;8.U:8)_$1I]34%W0AU+0%ZB&lt;75_6WFE&gt;'A],UZB&lt;75_$1I]6G&amp;M0D%],V:B&lt;$Y.#DQP34%W0AU+0%680AU+0%ZB&lt;75_47^E:4QP4G&amp;N:4Y.#DR$;'^J9W5_1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z&amp;?'.M&gt;8.J&gt;G5A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"$&lt;X"Z0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X)A28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0EZP&gt;#"#;81A1WRF98)],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;6TY.#DR&amp;4$Y.#DR/97VF0F.U?7RF0#^/97VF0AU+0%.I&lt;WFD:4Z4&lt;WRJ:$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I0#^$;'^J9W5_$1I]1WBP;7.F0E2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%98.I)%2P&gt;#"%&lt;X1],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E:J&lt;'QA5H6M:4QP4G&amp;N:4Y.#DR$;'^J9W5_28:F&lt;C"0:'1],U.I&lt;WFD:4Y.#DR$;'^J9W5_6WFO:'FO:TQP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0%6-0AU+0%ZB&lt;75_27ZE)%.B=(-],UZB&lt;75_$1I]1WBP;7.F0E2F:G&amp;V&lt;(1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2GRB&gt;$QP1WBP;7.F0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],U6-0AU+0#^$&lt;(6T&gt;'6S0AU+!!!!!!</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.EdgeWirePen" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!5Y0%.M&gt;8.U:8)_$1I]4G&amp;N:4Z1:7Y],UZB&lt;75_$1I]4H6N27RU=TYY0#^/&gt;7V&amp;&lt;(2T0AU+0&amp;5T-DY.#DR/97VF0E:P=G6H=G^V&lt;G1A1W^M&lt;X)],UZB&lt;75_$1I]6G&amp;M0D%T-4%Y.TQP6G&amp;M0AU+0#^6-T)_$1I]64-S0AU+0%ZB&lt;75_1G&amp;D;W&gt;S&lt;X6O:#"$&lt;WRP=DQP4G&amp;N:4Y.#DR797Q_.49W.T1R-4QP6G&amp;M0AU+0#^6-T)_$1I]1WRV=X2F=DY.#DR/97VF0E:J&lt;'QA5'&amp;U&gt;'6S&lt;DQP4G&amp;N:4Y.#DR/&gt;7V&amp;&lt;(2T0DA],UZV&lt;56M&gt;(-_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-$QP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!R0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$)],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A-TQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!U0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0&amp;5Y0AU+0%ZB&lt;75_5G^X)$5],UZB&lt;75_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP64A_$1I]64A_$1I]4G&amp;N:4Z3&lt;X=A.DQP4G&amp;N:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^6/$Y.#DR6/$Y.#DR/97VF0F*P&gt;S!X0#^/97VF0AU+0&amp;:B&lt;$YQ0#^797Q_$1I],V5Y0AU+0#^$&lt;(6T&gt;'6S0AU+0%ER.DY.#DR/97VF0F&gt;J:(2I0#^/97VF0AU+0&amp;:B&lt;$YT0#^797Q_$1I],UER.DY.#DR&amp;6TY.#DR/97VF0EVP:'5],UZB&lt;75_$1I]1WBP;7.F0E.P=(E],U.I&lt;WFD:4Y.#DR$;'^J9W5_4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_28BD&lt;(6T;8:F)%^S0#^$;'^J9W5_$1I]1WBP;7.F0E*J&gt;#"$&lt;'6B=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1W^Q?4QP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A4X)],U.I&lt;WFD:4Y.#DR$;'^J9W5_4G^S)%6Y9WRV=WFW:3"0=DQP1WBP;7.F0AU+0%.I&lt;WFD:4Z/&lt;X1A1GFU)%.M:7&amp;S0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP26=_$1I]25Q_$1I]4G&amp;N:4Z4&gt;(FM:4QP4G&amp;N:4Y.#DR$;'^J9W5_5W^M;71],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;$QP1WBP;7.F0AU+0%.I&lt;WFD:4Z%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1],U.I&lt;WFD:4Y.#DR$;'^J9W5_2'&amp;T;#"%&lt;X1A2'^U0#^$;'^J9W5_$1I]6G&amp;M0D!],V:B&lt;$Y.#DQP25Q_$1I]25Q_$1I]4G&amp;N:4Z';7RM)&amp;*V&lt;'5],UZB&lt;75_$1I]1WBP;7.F0E6W:7YA4W2E0#^$;'^J9W5_$1I]1WBP;7.F0F&gt;J&lt;G2J&lt;G=],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DR&amp;4$Y.#DR/97VF0E6O:#"$98"T0#^/97VF0AU+0%.I&lt;WFD:4Z%:7:B&gt;7RU0#^$;'^J9W5_$1I]1WBP;7.F0E:M981],U.I&lt;WFD:4Y.#DR797Q_-$QP6G&amp;M0AU+0#^&amp;4$Y.#DQP1WRV=X2F=DY.#A!!!!!</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"^F5F.31QU+!!.-6E.$4%*76Q!!'SQ!!!1Z!!!!)!!!'QQ!!!!:!!!!!22.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!!!!!!)A2!)!)!$!!!!A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!UIW.D!'NT%/-`1+87B!&amp;HA!!!!Q!!!!1!!!!!!Q/@&gt;%C.-Z&amp;IWVT'4X*[?$5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!!.M?KZS\;K49?%37#].&amp;,&gt;!!!!%$MEH+Y6E7%?C2**HR\$-!E!!!!1\;XBH"^DD*M]6WB\I$/I21!!!"!Q8SQLY:EU-/#!A9(FPE=_!!!"#1!"4&amp;:$1S6.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=TJ.:7VP=HF8=GFU:8)O9X2M!!!!!!!$!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!*735.$!!!!!!%54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!5&amp;2)-!!!!"M!!1!$!!!54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!!!!#!!%!!!!"!!%!!!!!!!5!!!!!!!!!!!!!!!!!!F:*1U-!!!!!!!!"$F&gt;S;82F=F2Z='5O9X2M!&amp;"53$!!!!!6!!%!!Q!!$F&gt;S;82F=F2Z='5O9X2M!!!!!A!#!!!!!1!"!!!!!!!&amp;!!!!!!!!!!!!!!!!!!-!!!!!!!!#!!-!!!!!!"]!!!!C?*RDY'&gt;A%7!19$2A!**-1**$!),B,!!9UA&amp;H!!!!!%9!!!%K?*RD9-!%`Y%!3$%S-$"^!^+M;/*A'M;G.E!W&amp;ZM&gt;O/T&amp;+MY%I2C"-ER0A!Q7E"B5*2N5S2UA`I#OE1W,$1"G&gt;3CF!!!!!!!U!!&amp;73524*5VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T/EVF&lt;7^S?6&gt;S;82F=CZD&gt;'Q!!!!!!!!!!Q!!!1]!!!(!?*R49'2AS$3W-%M!UMQ-%*#=HZ)+IN=!R:SB9I9($I.J'0]Q4#W5&lt;H\$9HCJ_1D,S]@```](]B]DR,N&gt;6$A;+O1:_&amp;P&amp;A!,('VQ9)6+&gt;,#IPQ%K/M"QW0!!CA2RF*IAE5".0NY]+3W?D%T/9*^$:[!:CA@G&gt;D6ZANBO9\1&gt;C-X20")M*72\C&lt;T5#'1N7Q^).FOY.9/G=#'+E&gt;4OQ1&amp;3G74,R4TH!P_X#$J"\_&amp;U0(1&lt;K"!G!@;L-C/,_&lt;I\DDC)*)/=\;MQ!5QY?9-J&amp;A2(6*U$KUK``D'`_-RK!=$W1L]0!S/!*6+9'J%('MD.!0-H)Q-+AQWD-;-8I#,5.0X$W&gt;X&amp;&amp;ZI0C"Q$L[F&lt;)!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$"%!A!A!!!1R-3YQ!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$"%!A!A!!!1R-3YQ!!!!!!Y2!9!6!!!'-4%O-#YR!!!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!CKS\9.KKEF#KL*.AC5K35)F+EV!!!!!!!!!!!@````H`_@`Z`_^`_@_QX`H_Q$@Z`Q!0_@]!$`H`Q$`Z``$`_@````H````Z````_@````H````Z````_@````H````Z````_@``@`H``@`Z````_@````A!!!!!!!!)!X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;C)C)C)C)C)C)C)C)C)C)X9_)DY_0DY`YDY``D`D`C.W0_0_0DY_0DY_)_)_)_0D&gt;DY_0DY_0D`C0C0C0_0_)X9_)DYDY_)_0DYDYDYDY_.W0C)_)_0C0DY_)_)`Y_0D&gt;C)C)C)C)C)C)C)C)C)C)X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;UC)C)C)C)C)C)C)C)C)C,&gt;)C)C)C)C)IAC)C)C)C)CX3)C)C)C)IG:G#)C)C)C)NUC)C)C)IG9C)G9)C)C)C,&gt;)C)C)IG9C)C)C:AC)C)CX3)C)CG9C)C)C)C*EC)C)NUC)C)JG)C)C)C)DZ)C)C,&gt;)C)C+:G9C)C)D`_3)C)CX3)C)CG:G:C)D```EC)C)NUC)C)JG:G:G@```Z)C)C,&gt;)C)C+:G:G:````_3)C)CX3)C)CG:G:G@````EC)C)NUC)C)JG:G:H````Z)C)C,&gt;)C)C+:G:G:````_3)C)CX3)C)CG:G:G@````EC)C)NUC)C)JG:G:H```_:``)C,&gt;)C)C)JG:G:``_:H```]CX3)C)C)CG:G@_:H```]C)NUC)C)C)C+:G:D````S)C,&gt;)C)C)C)C)JD````S)C)CX3)C)C)C)C)C,``S)C)C)NUC)C)C)C)C)C)C)C)C)C,&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;U!!!1!8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R==("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q8&amp;RQ`X"Q=0^Q`X$`=0^Q``^Q=0^Q````=0``=0``=("=8($``X$``X$`=0^Q`X$`=0^Q`X"Q`X"Q`X"Q`X$`=&amp;R==0^Q`X$`=0^Q`X$`=0``=($`=($`=($``X$``X"Q8&amp;RQ`X"Q=0^Q=0^Q`X"Q`X$`=0^Q=0^Q=0^Q=0^Q`X"=8($`=("Q`X"Q`X$`=($`=0^Q`X"Q`X"Q``^Q`X$`=&amp;R==("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8-X.T=X.T=X.T=X.T=X.T=X.T=X.T=X.T=X.T=X.T6R=T=X.T=X.T=X.T=X.T=V-4-X.T=X.T=X.T=X.T=X.8&amp;T.T=X.T=X.T=X.T=V-I*KAI%T.T=X.T=X.T=X.T=V=8-X.T=X.T=X.T=V-I*JQ=("QI+"-T=X.T=X.T=X.T6R=T=X.T=X.T=V-I*JQ=("Q=("Q=+#A4-X.T=X.T=X.8&amp;T.T=X.T=X.I*JQ=("Q=("Q=("Q=(#AI-X.T=X.T=V=8-X.T=X.T=W;GH"Q=("Q=("Q=("Q=.'AT=X.T=X.T6R=T=X.T=X.T:KAI*JQ=("Q=("Q=.(2U:L.T=X.T=X.8&amp;T.T=X.T=X.GK#AI+#;=("Q=.(2U&gt;(2GMX.T=X.T=V=8-X.T=X.T=W;I+#AI+#AGK$2U&gt;(2U&gt;';T=X.T=X.T6R=T=X.T=X.T:KAI+#AI+#AU&gt;(2U&gt;(2U:L.T=X.T=X.8&amp;T.T=X.T=X.GK#AI+#AI+$2U&gt;(2U&gt;(2GMX.T=X.T=V=8-X.T=X.T=W;I+#AI+#AI.(2U&gt;(2U&gt;';T=X.T=X.T6R=T=X.T=X.T:KAI+#AI+#AU&gt;(2U&gt;(2U:L.T=X.T=X.8&amp;T.T=X.T=X.GK#AI+#AI+$2U&gt;(2U&gt;(2GMX.T=X.T=V=8-X.T=X.T=WAI+#AI+#AI.(2U&gt;(2U;#AL+SMT=X.T6R=T=X.T=X.T=W;GK#AI+#AU&gt;(2U;#AGKSML+SML-X.8&amp;T.T=X.T=X.T=X.GK#AI+$2U;#AGKSML+SML-X.T=V=8-X.T=X.T=X.T=X.T:KAI+#A=+SML+SML+T.T=X.T6R=T=X.T=X.T=X.T=X.T=W;=+SML+SML+T.T=X.T=X.8&amp;T.T=X.T=X.T=X.T=X.T=X.T;SML+T.T=X.T=X.T=V=8-X.T=X.T=X.T=X.T=X.T=X.T=X.T=X.T=X.T=X.T6R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8!!!!!)!!Q!!!!!"&amp;1!"2F")5#6.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=TJ.:7VP=HF8=GFU:8)O9X2M!!!!!!!#!!*52%.$!!!!!1Z8=GFU:8*5?8"F,G.U&lt;!"16%AQ!!!!&amp;1!"!!-!!!Z8=GFU:8*5?8"F,G.U&lt;!!!!!)!!!!!!!%!!1!!!!!!"1!!!!!!!!!!!!!!!!!!!!%!!!#(5&amp;2)-!!!!!!!!!!!!!*52%.$!!!!!!!!!2209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!"16%AQ!!!!'Q!"!!-!!"209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!!!!)!!!!!!!%!!1!!!!!!"1!!!!!!!!!!!!!!!!!!!!%!!!!85&amp;2)-!!!!!!!!!!!!!-!!!!!!!=)!!!5:(C=R6B&gt;4&amp;.H'(Z07_!5#ZYC@W6A3TX&amp;0[LY.R69-O$IJD0-C=4.R#D1CIA#;1N)&gt;/*G*]&amp;IJM/ZO-2ESR+788H"R7Z=9L&lt;':$EX[M73K5HHMOR#X7)7H=4N=0:_X_HJ_7FJC4/RBJ.$_&gt;\P@=\\0-`\P5?!YHV=O75'DEP!=%`Q:JM%_1'2!9DZ75B]O(&lt;G(W#+89Q%?^BW\IZFBKG5Q"%1;^A[92Q?Y6,Z"\E"`G;'O8OY.*&gt;TY5\Z%MQ0C'8/L8S=Y[^5]O-Z[J:/K/,/-D/7&gt;XHX5`:MN"_T181*O4L^T!QQ1L8.&amp;P6O\_A,2HHSL&gt;X0OOC7&gt;AEY13Q)]@(&amp;O#/G`JZO;6H/?#W.[J;!7S["K;EJ,=CJ".61'"MQBP%#-'MMST0%&amp;!FC6:C0V^+9@"K$?&gt;;I?14XT!4"4I,-I1M%E=.1D$O3?'13K]:*L[?./S""-2^P:FXMP="NJO#&gt;[,@!!"0&lt;S]L@S;&gt;*P,/6M%$8&amp;C%2PHK';]0\.AH+I[*F(^B)LBQIBEHYGN*A5WGI)$2MJD49B8%/."YG-P0AM9"1,&gt;BCXKK71Y0B3$$E[&gt;`P[4L5%1Z\"E)^1RW2I#@1%?F):?E61&lt;3P*R5A#;F!I!RM="F'^28PB]H*33Q#8L813E&amp;UF@(R:.Q#X0J,/*GM?E#L(MGK6;]+K_&gt;\$XS\1:D?W5XK[+OX[+2&lt;JEH8A^*V=G:E"OF[8YRU/:^#'PY._#5-IP,J[\1)28&lt;=J'9Y#K@A4!:F]EK1JG:=$`!:(-U1Y]/95&lt;W;-?9IRJT*LO;;&amp;$747*/;T51C(YO4@#C;ND'-IOHI$@GG`"#6$8&lt;Z6\A*FX4+$C"0P/CBR-P`QC61/IJ6J75ZUM)1+1N8X&amp;O.7N[&gt;2=OZ#3VT&lt;X=?$(:&amp;GC+25%`H9#19.ME8I&gt;=C&gt;''[Y+X%.WARPW)R)?\?KIK3&amp;-&amp;AMZ6%-KU'G]F`!-B9R;S;Y&lt;W1[K+[6"@:]&amp;]`&amp;C7&lt;CV:BK.Z&amp;Z"077*P&gt;2;P4O-B+873B,HK&gt;8M=U,[UT?%H&amp;:`$3_B&gt;`$'R):ZQQ^%)UAQEWJBCH&amp;[//14B$4(W+=8IRTT%V4S&lt;D.+1;JV@.F44/R9M8T=&gt;!Y_T'!;@]G"Q*%`*&gt;7!VDV$A7V4BDCH(E_`*D0!8W'IUTHNYY]Z#&amp;.\/Q1%S4PSP5A`V`Z]C!G1Q%@&amp;KHG;2J&gt;EFQ.C$;.-^Q',5-?CGM0"87RWE]]QB2-6$](*\Z29*TG./&amp;7MP8H86Z="D+Y8WV]L+46P\7L6N9_7@0HCE\2KPBB-U3Z=EX&gt;H]UYFN2NT_R&lt;9%%Z^&amp;0;!W(TIIZ-"^+I8M/O^L58@FTFNC+#L;F@W"%Q(04U2Y/\ADO$Y;#@6V";WNQ/*(0)=%HG/^60G[B_?K38=Y"$MRR^?J6T)&amp;8JVPBHZGR?G*O&lt;9G(,KEG6][&lt;E'E8[S8&lt;JSYF#KQG6_.3OVN$-[%]P:7C]4!%D10DSZ]@4?@=U81;U+#P,JB['Y.[K5"B'8M&lt;*=,9WT\6^\;AUNNM.2[IK::LP/"]A0$"`E$Z0C=2VIYN4R!J!`)X]G0Z0D*O5(#T)$JTT&lt;-6A\9S&gt;*5-MV62]UAEW"1+&gt;9QEN:$;\VI%M8Q6(W=Q3[+$&amp;I-6VM)C^;F`][4WLEU9N)[0&gt;WB"N=B:)WS?L&gt;`.EW!@DF`L&gt;=6V)J!GW+&lt;P="L0N.Q0&amp;3+J"TAD5ZWY790R!#N&amp;XT4#'S&lt;8J/G?81B_)R^X;?#&gt;]O^1BYE4Y0_=&lt;9Q/[.P2"98/8$IL\Z#A'ZNSIRZ-1_+UMKO%^K2J38`"5CTX_;QN+?:VNP?&amp;?\L\AI%EKS9SNUNQ-#![8%09&amp;ZPZ+QPZ]&lt;7KOSNB+973IT;T1[3:$;H.4-'=BZ35A]^51'QP=WRG;/@$3)P*TI5U@7%W/R@CEDH;76G;T=[)JE^"9WZVB=_0*EWLGQV.F\GZ^#/;9:X_L6"!?=E[/!XIGUO0ILI]&lt;&gt;]Q\LO,DS`5Z+QQ8GM=Z&gt;)9);)AKD2IQ!`6U'@31*IR=$!Z*NH6-3E0^GCRCJ2.#9?39Z)^-3:R_HRJ9Y;49Z)^-3:RRDR0:HO])`BY\9;#,Q5@B,)8@%2@]!.+Q6FK=\49M9#9SWUWV]P1O)^L5_G,'4V'C6N8'^WK$!GP`2_XHEDPDZ=U#HSA\RU+'N)\+F\#+)#HV9?)JJ[0TV=IP+:6P!42`(T\,K):005&amp;2:/D(F,4&gt;&gt;K".&lt;V3&gt;\^#&gt;_`8\J`?.(;)EZBT1=LYU:2&gt;M&amp;'&gt;9(@-5Q2L4YY@&lt;&amp;N2#8E.ZFW18V*5KN[7&amp;J7JNW6M'S*JV45-^M\V1:;]4*-@:=0]DYJ`H"L\+NV#3(T9.F[1L_`/J&lt;^9-KXE.C(S51F+B*;C;^Q7!I4&gt;QG\C,O/L(\Y&amp;FL+8W3?RHZ,`_2@&lt;FIS-XG#776X]ZW[W`$^---HL!!!!"!!!!'=!!!!U!!&amp;#2%B1*5VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T/EVF&lt;7^S?6&gt;S;82F=CZD&gt;'Q!!!!!!!!!!Q!!!'5!!!"V?*RD9'!I&amp;*"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```V=J9O4Y?O1;8.%2(TB4::9]BQ1!:1A:GA!!!!!!!!1!!!!(!!!%^1!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S%1#!#!!!!!%!#!!Q`````Q!"!!!!!!%8!!!!"A!&amp;!!5!!"Z!1!!"`````Q!!%66O=WFH&lt;G6E1HFU:5&amp;S=G&amp;Z!"^!=!!A!!%!!1!31HFU:5&amp;S=G&amp;Z5G6G:8*F&lt;G.F!!"&gt;!0%!!!!!!!!!!B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=QZ8=GFU:8*5?8"F,G.U&lt;!!R1"9!!QB$&lt;X"Z2'&amp;U91R6=W63:7:F=G6O9W5$4G6X!!J8=GFU:8*5?8"F!!"3!0(*MY?D!!!!!B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!A1&amp;!!!A!#!!-14W*K:7.U182U=GFC&gt;82F=Q!!(E"1!!%!""2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!!1!&amp;!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=B%!A!A!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!Q!!!!!!!!!"!!!!!A!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ%1#!#!!!!!%!"1!(!!!"!!$*MY?_!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N="%!A!A!!!!"!!5!"Q!!!1!!S&lt;/(PA!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-2!)!)!!!!!1!)!$$`````!!%!!!!!!2=!!!!'!!5!"1!!(E"!!!(`````!!!267ZT;7&gt;O:72#?82F18*S98E!(U"Q!#!!!1!"!"*#?82F18*S98F3:7:F=G6O9W5!!&amp;U!]1!!!!!!!!!#&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T$F&gt;S;82F=F2Z='5O9X2M!$&amp;!&amp;A!$#%.P=(F%982B$&amp;6T:6*F:G6S:7ZD:1./:8=!#F&gt;S;82F=F2Z='5!!&amp;)!]=GTB[-!!!!#&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#"!5!!#!!)!!R"09GJF9X2"&gt;(2S;7*V&gt;'6T!!!?1&amp;!!!1!%&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!"!!5!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:2%!A!A!!!!"!!5!!Q!!!1!!!!!!"A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%2!)!)!!!!"A!&amp;!!5!!"Z!1!!"`````Q!!%66O=WFH&lt;G6E1HFU:5&amp;S=G&amp;Z!"^!=!!A!!%!!1!31HFU:5&amp;S=G&amp;Z5G6G:8*F&lt;G.F!!"&gt;!0%!!!!!!!!!!B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=QZ8=GFU:8*5?8"F,G.U&lt;!!R1"9!!QB$&lt;X"Z2'&amp;U91R6=W63:7:F=G6O9W5$4G6X!!J8=GFU:8*5?8"F!!"3!0(*MY?D!!!!!B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!A1&amp;!!!A!#!!-14W*K:7.U182U=GFC&gt;82F=Q!!(E"1!!%!""2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!!1!&amp;!!!!!!!#!!!!!!!!!!!!!!1!#A!4!!!!"!!!!*5!!!!I!!!!!A!!"!!!!!!;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&lt;9!!!/W?*S&gt;EMVOUU!5B&lt;^YUD:*1UF,;0F*%Z&gt;7,*#IB""\FS*W`#CCAAU3LDW/AEQ=W:/#&gt;_RY%"["08M?JU]!VW/H69&amp;3F8NF;_&lt;/^:FTTD7QQY)E@=_D^F-#6A]GW8AUU?(DX/C^.06T"NY5FZLEWEFRK#/&gt;[EGAY3X(F/&amp;UH_E03:K`4M&gt;'J\PR52$\7&lt;:3&lt;F`F5\U&lt;G*A(XDKKM:^-]S?_]&gt;M(G4Y"5]`V2VKH`4$E_-?X,V`0"?__/(SP!\.H4$I_H"G&gt;W3N=\S7/J/L]@ARP,/,X=R%\:YI&amp;WDV"KV(@X)^HG&gt;4=*(*NJTN.RU?_U7YI/GA2#G2&gt;="&gt;1F&amp;M?ME3$&gt;\*K3[5AV;$*`=MYU+OU,0^&amp;SVX,L(UBM["1+ZV.&lt;QE6R3-;XG?6$%V:U%F%2Z!56_GQKG:BR$,TG@0IEP.NMAD?@]VNR6JUD3ZNK_S[-J`#SLAVVM6:J\"XM8A*M^,:/E_FQ?5'.SO?N]4V?=\DT]LJS7W&lt;Z=2K^#J&lt;,RZY9'_84KB)SC`(&amp;P_?LH!8%DXZ3FEN,;[)]!UW[4/1[E$3E5?*JL\E&amp;H&gt;E\&lt;"&gt;3+&lt;_#Q$VVE%!!!!!!(=!!1!#!!-!"1!!!&amp;A!$Q1!!!!!$Q$9!.5!!!"B!!]%!!!!!!]!W!$6!!!!;A!0"!!!!!!0!.A!V1!!!(/!!)1!A!!!$Q$9!.5!!!"VA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!"-1"35V*$$1I!!UR71U.-1F:8!!!&lt;,!!!"$E!!!!A!!!&lt;$!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%^#5U=!!!!!!!!"H%.$5U=!!!!!!!!"M%R*&gt;GE!!!!!!!!"R%.04F!!!!!!!!!"W&amp;2./$!!!!!!!!!"\%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!!!!!#+(:F=H-!!!!%!!!#0%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$1!!!!!!!!#S'FD&lt;$A!!!!!!!!#X%.11T)!!!!!!!!#]%R*:H!!!!!!!!!$"%:13')!!!!!!!!$'%:15U5!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!`````Q!!!!!!!!#M!!!!!!!!!!$`````!!!!!!!!!-!!!!!!!!!!!0````]!!!!!!!!!V!!!!!!!!!!!`````Q!!!!!!!!$I!!!!!!!!!!$`````!!!!!!!!!@A!!!!!!!!!!0````]!!!!!!!!#!!!!!!!!!!!!`````Q!!!!!!!!)E!!!!!!!!!!$`````!!!!!!!!!H!!!!!!!!!!!0````]!!!!!!!!#K!!!!!!!!!!%`````Q!!!!!!!!/]!!!!!!!!!!@`````!!!!!!!!!^!!!!!!!!!!#0````]!!!!!!!!$Y!!!!!!!!!!*`````Q!!!!!!!!0U!!!!!!!!!!L`````!!!!!!!!"!1!!!!!!!!!!0````]!!!!!!!!%'!!!!!!!!!!!`````Q!!!!!!!!1M!!!!!!!!!!$`````!!!!!!!!",!!!!!!!!!!!0````]!!!!!!!!'N!!!!!!!!!!!`````Q!!!!!!!!KY!!!!!!!!!!$`````!!!!!!!!#M!!!!!!!!!!!0````]!!!!!!!!,X!!!!!!!!!!!`````Q!!!!!!!",I!!!!!!!!!!$`````!!!!!!!!%P!!!!!!!!!!!0````]!!!!!!!!4+!!!!!!!!!!!`````Q!!!!!!!"/5!!!!!!!!!!$`````!!!!!!!!%ZQ!!!!!!!!!!0````]!!!!!!!!9G!!!!!!!!!!!`````Q!!!!!!!"CA!!!!!!!!!!$`````!!!!!!!!'+A!!!!!!!!!!0````]!!!!!!!!9V!!!!!!!!!#!`````Q!!!!!!!"K1!!!!!"".:7VP=HF8=GFU:8)O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!22.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!!!!%!!1!!!!!!"1!!!!!'!!5!"1!!(E"!!!(`````!!!267ZT;7&gt;O:72#?82F18*S98E!(U"Q!#!!!1!"!"*#?82F18*S98F3:7:F=G6O9W5!!&amp;U!]1!!!!!!!!!#&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T$F&gt;S;82F=F2Z='5O9X2M!$&amp;!&amp;A!$#%.P=(F%982B$&amp;6T:6*F:G6S:7ZD:1./:8=!#F&gt;S;82F=F2Z='5!!&amp;)!]=GTB[-!!!!#&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#"!5!!#!!)!!R"09GJF9X2"&gt;(2S;7*V&gt;'6T!!"9!0(*MY?_!!!!!B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=R".:7VP=HF8=GFU:8)O9X2M!#J!5!!"!!1&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!&amp;!!!!!@````]!!!!!!!)!!!!"&amp;%^V&gt;("V&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!1!)!!!!!!!!!!!!!!!A!!!"*';7RF5G6B:'6S,GRW9WRB=X-!!!!32GFM:6&gt;S;82F=CZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"+!!!!!220&gt;82Q&gt;824&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!+!!"!!1!!!R0&gt;82Q&gt;824&gt;(*F97U54X6U=(6U5X2S:7&amp;N,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="MemoryWriter.ctl" Type="Class Private Data" URL="MemoryWriter.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="WriteToByteArray.vi" Type="VI" URL="../WriteToByteArray.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$M!!!!#!!%!!!!"1!&amp;!!!?1%!!!@````]!!2&amp;6&lt;H.J:WZF:%*Z&gt;'6"=H*B?1!F1(!!)!!"!!)!'%*Z&gt;'6"=H*B?6*F:G6S:7ZD:3!S)%^V&gt;!!!'%"!!!(`````!!%+:'&amp;U93"W97RV:1!!#U!$!!6J&lt;G2F?!!B1(!!)!!"!!)!&amp;5*Z&gt;'6"=H*B?6*F:G6S:7ZD:3"*&lt;A"5!0!!$!!!!!!!!!!$!!!!!!!!!!!!!!!%!!5!"A-!!(A!!!!!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!!!"!!=!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
		</Item>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="SetSize.vi" Type="VI" URL="../SetSize.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$-!!!!"Q!%!!!!"1!&amp;!!!?1%!!!@````]!!2&amp;6&lt;H.J:WZF:%*Z&gt;'6"=H*B?1!D1(!!)!!"!!)!&amp;E*Z&gt;'6"=H*B?6*F:G6S:7ZD:3"0&gt;81!!!N!#A!%5WF[:1!!)5"Q!#!!!1!#!"6#?82F18*S98F3:7:F=G6O9W5A37Y!4A$Q!!M!!!!!!!!!!Q!!!!!!!!!!!!!!"!!&amp;!Q!![!!!!!!!!!!!!!!!!!!!$1I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!A!!!!!!1!'!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">34078736</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="Open_Impl.vi" Type="VI" URL="../Open_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)`!!!!$A!O1(!!(A!!&amp;B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!;U!7!!9%&lt;X"F&lt;A&gt;S:8"M97.F"G.S:7&amp;U:1ZP='6O)'^S)'.S:7&amp;U:2&amp;S:8"M97.F)'^S)'.S:7&amp;U:3.S:8"M97.F)'^S)'.S:7&amp;U:3"X;82I)'.P&lt;G:J=GVB&gt;'FP&lt;A!!#7^Q:8*B&gt;'FP&lt;A!P1"9!!QJS:7&amp;E,X&gt;S;82F#8*F971N&lt;WZM?1JX=GFU:3VP&lt;GRZ!!:B9W.F=X-!!"*!-0````]);'^T&gt;%ZB&lt;75!!!N!"A!%='^S&gt;!!!$5!$!!&gt;U;7VF&lt;X6U!!1!!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!(!!A!#2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!":!5!!$!!=!#!!*#76S=G^S)'^V&gt;!#U!0!!(!!!!!%!!A!$!!1!"1!'!!I!"A!'!!9!"A!'!!9!"A!'!!9!"A!'!!9!#Q!'!!9!"A!'!!9!"A!-!A!"'!!!EA!!!!A!!!!)!!!!#!!!!!A!!!!)!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$1=!!!!"!!U!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118310912</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Close_Impl.vi" Type="VI" URL="../Close_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!"95476N&lt;X*Z6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118310912</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="U8_Impl.vi" Type="VI" URL="../U8_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!#5!&amp;!!*6/!!!,E"Q!"Y!!"95476N&lt;X*Z6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118310928</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Int32_Impl.vi" Type="VI" URL="../Int32_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!#U!$!!6*&lt;H1T-A!O1(!!(A!!&amp;B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">44568592</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="String_Impl.vi" Type="VI" URL="../String_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%L!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%%!Q`````Q:4&gt;(*J&lt;G=!!#Z!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342713872</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="PutBytes_Impl.vi" Type="VI" URL="../PutBytes_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!"1!&amp;!!!A1%!!!@````]!"R.V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!#Z!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!A!#1-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!EA!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118310928</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="ByteArray_Impl.vi" Type="VI" URL="../ByteArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!"1!&amp;!!!A1%!!!@````]!"R.V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!#Z!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!A!#1-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!EA!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118310928</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Double64_Impl.vi" Type="VI" URL="../Double64_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%K!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$U!+!!B%&lt;X6C&lt;'5T-A!!,E"Q!"Y!!"95476N&lt;X*Z6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118310928</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="WaveformArray_Impl.vi" Type="VI" URL="../WaveformArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&amp;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%%"5!!-)&gt;W&amp;W:7:P=GU!!"J!1!!"`````Q!($6&gt;B&gt;G6G&lt;X*N18*S98E!,E"Q!"Y!!"95476N&lt;X*Z6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Unsigned16Array_Impl.vi" Type="VI" URL="../Unsigned16Array_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!"1!'!!!=1%!!!@````]!"Q^6&lt;H.J:WZF:$%W18*S98E!,E"Q!"Y!!"95476N&lt;X*Z6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#3!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1118310928</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="WriterType.ctl" Type="VI" URL="../WriterType.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074266624</Property>
		</Item>
		<Item Name="MemoryWriter_Create.vi" Type="VI" URL="../MemoryWriter_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Q!!!!$Q!K1(!!(A!!&amp;B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!#7^C;G6D&gt;#"J&lt;A!I1(!!(A!!&amp;R6&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X-!"F"B=G6O&gt;!!!"1!&amp;!!!?1%!!!@````]!!B&amp;6&lt;H.J:WZF:%*Z&gt;'6"=H*B?1!@1(!!)!!"!!-!%E*Z&gt;'6"=H*B?6*F:G6S:7ZD:1!!%5!$!!J#&gt;7:G:8*4;8JF!!"&gt;!0%!!!!!!!!!!B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=QZ8=GFU:8*5?8"F,G.U&lt;!!R1"9!!QB$&lt;X"Z2'&amp;U91R6=W63:7:F=G6O9W5$4G6X!!J8=GFU:8*5?8"F!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!(!!A!#2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!1!!!!M1(!!(A!!&amp;B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!#G^C;G6D&gt;#"P&gt;81!!":!5!!$!!=!#!!*#76S=G^S)'^V&gt;!#:!0!!&amp;!!!!!%!"!!&amp;!!9!#A!,!!M!#Q!,!!M!#Q!,!!M!$!!,!!M!#Q!,!!U$!!%1!!!+!!!!#A!!!!A!!!!)!!!!#!!!!!I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!$15!&amp;1!!!!!!!!!!!!!!!!!!!1!!!!!!!!!"!!Y!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
		<Item Name="GetByteArrayReference.vi" Type="VI" URL="../GetByteArrayReference.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;O!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!5!"1!!(E"!!!(`````!!5267ZT;7&gt;O:72#?82F18*S98E!(U"Q!#!!!1!'!"*#?82F18*S98F3:7:F=G6O9W5!!$*!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!1476N&lt;X*Z6X*J&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Q1(!!(A!!&amp;B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$UVF&lt;7^S?6&gt;S;82F=C"J&lt;A"B!0!!$!!$!!1!"Q!)!!1!"!!%!!1!#1!%!!1!#A)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%4!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!7&amp;%VF&lt;7^S?6&gt;S;82F=CZM&gt;G.M98.T!!!+&lt;W*K:7.U)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!&amp;B2.:7VP=HF8=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!#7^C;G6D&gt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
	</Item>
</LVClass>
