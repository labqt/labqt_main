﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">RedFrame</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">9464978</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16762993</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">15517125</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">2</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeBaseTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,4!!!*Q(C=T::"DF*"%)&lt;`95S5R.&amp;Q!AUH-#G7RB5(G*CQ-9&lt;-KKYQLBS88+(C$:#&gt;3T9?I+Z!H"/QV9U_P^=U--!-%R-4J^]L_P(`V&gt;5@X:U8J.+?3)`U`L\70&gt;K+XT4NP9LO_L(NVHZ\.^NO]XAQPGGK`X@DOZN"X7WJL8_5`\\W_'D&lt;^TO&gt;9XZH0"C0/X@ZO#X34M&lt;7L_Z?RM&lt;@OKO-@8_QY]]/`@'WQGQW'^TCVYT:\%[@D0(MK&amp;`NB_?0&gt;`T3\[\`9,$R_:GLN.X^'[T]'_\_`J?-\GRL(ZS@Q9Z\[..OO,@[/_X?Y]V\Y&amp;0THZNU)L6^US12B"/GVGI30&gt;%40&gt;%40&gt;%$0&gt;!$0&gt;!$0&gt;!&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%.X&gt;!.X&gt;!.X&gt;$,B3ZUI1O&gt;65EG4S:+CC9&amp;EG21F(QE0!F0QJ0Q]&amp;5*4]+4]#1]#1]J3HA3HI1HY5FY'+;%*_&amp;*?"+?B)&gt;3B34,B1Z0QE.Z"4Q"4]!4]!1]4+G!*Q!)*AM+"U8!5'!'$Q&amp;0Q"0Q]+C!*_!*?!+?A!&gt;&lt;!5`!%`!%0!%01]KK2+&amp;J,X2Y+#/(R_&amp;R?"Q?BY@3=HA=(I@(Y8&amp;YG%Y/D]0D1$A4/M6"E$0)38#_/$Q/$ZU=(I@(Y8&amp;Y("[MME.?6K;F;3^U?!Q?A]@A-8A-(EL)Y$&amp;Y$"[$R_#BL!Q?A]@A-8A-(K;3Q70Q'$Q'C$%JU]MI:AQUEAT"Y/%OJ]8+,E5BM8**^@#K(EL6Q[:[C&amp;10B_KGKW[G[C;J,L\KIKIOFOICK0YY67B6'.6*6!?XC6LSO3$GR*39%*@%C"A3@;,8$PX(C=PF5IP&amp;1P0Z8.0J6*0*2*?8FRK.2BI/B_LX__LV?FKXFVSL&gt;N+_F^&lt;S7&gt;B&amp;^_XU`/TTF^*XXXU^VX\L@/-@T3NK0'8E+1*R]5T[]%&lt;[_&amp;O[OC7;ZFIP@PX5]R`8/PV/A3P'P&lt;Z:^1']'^82KN_MU2_4)']7!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.18</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"N@5F.31QU+!!.-6E.$4%*76Q!!&amp;OA!!!2X!!!!)!!!&amp;MA!!!!8!!!!!2*';7RF5G6B:'6S,GRW9WRB=X-!!!!!E"1!A!!!-!!!#!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!!+2E'L:FV94K[)!)[NLN1W!!!!$!!!!"!!!!!!J!-P!U22&amp;5O=PS0FUB"WE.1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!'/%]NF$\$*,P9AF&lt;)K&lt;`J)"!!!!`````Q!!!"!DMKY-\AGHFNIP\CIX&amp;9&lt;I!!!!"!!!!!!!!!#/!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!"`Q!!!!%!!1!!!!!!%A!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!A!#!!!!!!!H!!!!)(C=9_"C9'ZAO-!!R)Q-4!V-'E$7"Q9'$A9/!2"G!!"C#A5,!!!!!%E!!!%9?*RD9-!%`Y%!3$%S-$$&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?SQI3"\NY$J*F!=F!V("!JJAN!@!,&gt;((YI01.*$!#1YSD6!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!?)!!!.%?*S.5T&amp;,(%%5@D-:&gt;%*7&gt;M5F8*&amp;#S":(E'$Q#!'PO/!+*VT)39Y1$+3)'B2%5[3YQG*VP7):LADY!Q,Z$W*ZZQJLE3[`)+3ZYIJ!)&amp;8A]LWZ82#*[-$/.W`?_^Z]X]\OP#4;8HDW^!PQDC$S+;,VP9X.+5*-YT&amp;8,$#7=HT33SW_%O-Y(DIG$,RY0SB*N_.DRT1#0WE%+F("Q&amp;;=/[GJ\W40KRQ:D@UM$$1(1-5Y7NXCKJ_WHY&gt;_PIH;/#""(;*:I!)_'&amp;-&gt;JATOW&gt;:?GGO*BS6&lt;Q&gt;Q'')X!Q^I";E/*1-\JVB6#"WE&gt;^Z8.V.2FJ:W?'`&lt;C]R,X4(/`44"VV(Z)\J'S^N@3,+LEZAPC7EL@K8A@XD7+#^/$OY8SLT@I^B;HX?-TNV-"I&gt;N531M?6BF9O&amp;GZKPWYZZZ=H,)U&gt;\H0.NS4-_PGN^7FU&gt;`L`V$G)-+&gt;&gt;T`TD#UH;?F3&amp;OKZ_[A3&amp;SQ]=9K&lt;U_F[&lt;P5V.`A%=25WJ(D8VJC*L&amp;:_AT#L_75,7\]MN/PMO@:R(M?=]I2O\V)[`$-3QV%QU]S`MU@U``%"4Z5%?7CW!VQ"0A:_%]S&lt;IFEZTI@!-J!&amp;4:+U8%';`AIJ8]C7@#P@SWWZ+[]ZZF:D[77Y@$EO`IN`GXSFQQ!!!!!!%Q!!!!FYH'.A9'"E:!!#!!!5!!-!!!!!$B1"A#=!!!9R.#YQ,D%!!!!!!!!-&amp;!#!!!!!"$%U,D!!!!!!$B1"A#=!!!9R.#YQ,D%!!!!!!!!-&amp;!#!!!!!"$%U,D!!!!!!$B1"A#=!!!9R.#YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!W:-W!*5KJ1$:OL9!F3KF!*7L.1!!!!!!!!!!!!!!!!!!!!!!!#Q!!!#$!!!#!-!!#!!Q!!!!-!!'!/!!"Y0A!!@PY!!(`_!!"``A!!@`Y!!(`_!!"``A!!```A!"`^_!!0^_!!!^`!!!"`!!!!0!!!!!!!!!!!!!!!!A$&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;U!!!!!!!!!!!!!!!!!!!$&gt;!!`Q`Q$`!0!0]!`Q`Q!!X1!0!0$Q]!]0$Q]0!0$Q!.U!$`$`!0]0`Q]0$`$`!!$&gt;!!]!]0$Q$Q]0$Q]!]0!!X1!0!0$Q`Q]0$`!0]0$Q!.U!!!!!!!!!!!!!!!!!!!$&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X1!!!!!!!!!!!!!!!!!!!.U!!!!!!!!!%1!!!!!!!!$&gt;!!!!!!!!'[OR!!!!!!!!X1!!!!!!'[!!#\%!!!!!!.U!!!!!'[!!!!!,M1!!!!$&gt;!!!!#[!!!!!!!!OQ!!!!X1!!!!KA!!!!!!!0M!!!!.U!!!!+O[!!!!!0`[!!!!$&gt;!!!!#LO\I!!0``_A!!!!X1!!!!K\O\OL````I!!!!.U!!!!+O\O\P````[!!!!$&gt;!!!!#LO\O\````_A!!!!X1!!!!K\O\O`````I!!!!.U!!!!+O\O\P````[!!!!$&gt;!!!!#LO\O\````_A!!!!X1!!!!O\O\O````\P`]!!.U!!!!!KLO\P``\OP```Q$&gt;!!!!!!#LO\`\OP```Q!!X1!!!!!!!+O\M0````!!!.U!!!!!!!!!I0````!!!!$&gt;!!!!!!!!!!!0``!!!!!!X1!!!!!!!!!!!!!!!!!!!.X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X1!!"!"=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;Q*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1F=8!E*#@``#@``#1H``QE*`QE*``]*#@``#@``#1E*#6R=#1E*`QE*`QH`#@]*#@]*`QH`#@]*`QE*`QH`#1E*8&amp;Q*#1H``QH``QE*``]*````#@]*`QH``QH``QE*#1F=8!E*#@]*#@]*`QH`#1H`#@]*`QH`#@]*#@]*`QE*#6R=#1E*`QE*`QH`#@``#@]*`QH``QE*``]*`QH`#1E*8&amp;Q*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1F=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R="Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=(8&amp;Q("Q=("Q=("Q=("Q=("Q)#"Q=("Q=("Q=("Q=("Q&gt;=8!=("Q=("Q=("Q=("Q*?.&amp;Z?!A=("Q=("Q=("Q=("VR="Q=("Q=("Q=("Q*?.!E*#1F?8A)("Q=("Q=("Q=(8&amp;Q("Q=("Q=("Q*?.!E*#1E*#1E*8FY#"Q=("Q=("Q&gt;=8!=("Q=("Q&gt;?.!E*#1E*#1E*#1E*#6Z?"Q=("Q=("VR="Q=("Q=("T1U#1E*#1E*#1E*#1E*L&amp;Y("Q=("Q=(8&amp;Q("Q=("Q=(.&amp;Z?.!E*#1E*#1E*L+SM.!=("Q=("Q&gt;=8!=("Q=("Q=U8FZ?8D1*#1E*L+SML+QU"Q=("Q=("VR="Q=("Q=("T2?8FZ?8FYU8KSML+SML$1("Q=("Q=(8&amp;Q("Q=("Q=(.&amp;Z?8FZ?8F[ML+SML+SM.!=("Q=("Q&gt;=8!=("Q=("Q=U8FZ?8FZ?8KSML+SML+QU"Q=("Q=("VR="Q=("Q=("T2?8FZ?8FZ?L+SML+SML$1("Q=("Q=(8&amp;Q("Q=("Q=(.&amp;Z?8FZ?8F[ML+SML+SM.!=("Q=("Q&gt;=8!=("Q=("Q=U8FZ?8FZ?8KSML+SML+QU"Q=("Q=("VR="Q=("Q=("VZ?8FZ?8FZ?L+SML+SM8F[ML+Q("Q=(8&amp;Q("Q=("Q=("T1U8FZ?8F[ML+SM8FYUL+SML+SM"Q&gt;=8!=("Q=("Q=("Q=U8FZ?8KSM8FYUL+SML+SM"Q=("VR="Q=("Q=("Q=("Q=(.&amp;Z?8FY*L+SML+SML!=("Q=(8&amp;Q("Q=("Q=("Q=("Q=("T1*L+SML+SML!=("Q=("Q&gt;=8!=("Q=("Q=("Q=("Q=("Q=(L+SML!=("Q=("Q=("VR="Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=(8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=!!!!!A!#!!!!!!#'!!&amp;'5%B1!!!!!1!#6%2$1Q!!!!%54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!5&amp;2)-!!!!"M!!1!$!!!54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!!!!#!!$`!!!!!1!"!!!!!!!3!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!!J5&amp;2)-!!!!!!!!!!!!!-!!!!!"2!!!!T'?*SN6VVI(&amp;550H=S37?48:R.MEWW4&gt;D*&gt;D;.J+(2).5K7/SUW&amp;BC.!WI1:OFM\'&amp;N:(]6%&amp;I*9S2"+*I-;!F7"^-KR9K2F#KU2J81?;FPCB9=+H5&amp;K&amp;#1+-F&gt;89]^]\/TMT_*1^.S,"MTH@/O?@\PHNW!@R,9C/8A?-'%(%&amp;8_QXI&amp;L6#5#K1Y$M4_MEC0XE&amp;J$[-$(A;;&amp;@P-RF3*-"@F6P&amp;4K6+6D';0.&lt;]X\YB\QA8M(1+D'-S;I.O%06'Y,&gt;=FK5TT@*5Z6WVC!UCT-EQTUB2`Y6:L2B,!B;'XU'/UA'C.,#]VKU.XYUI=HU86_(%'9J@1;)CBY9E&gt;.&lt;-3/78G)JO893Z2[Q5Q+G&lt;)/&amp;B15(&amp;,2!L;S.?R&amp;$IA#ECWMPA[F6^/:2/&lt;W.9;I:"ONUW8753/9E\:W#]K&amp;VCCYC&amp;(%P:I^-M4&lt;/W&amp;55&gt;^C!?DH^E"!7LKC`E-"DWG&gt;!A+1/#O;C/5XRQ2\+!ION23*C/YH9B[`\$'D5&gt;'Y1?&amp;KL%OJB&amp;MYQ'HC&lt;BEW5BLW-"J]S*9,$Q]HS0%A=+#U+HYIW\U[/DYYF2K4B)?F1-DY[+DU`=O29@#QBK@'R?#&amp;,GR5^P)./A":E!I%'Y'%'8H.0@"DGZ_&gt;R#0BUI%U)&lt;:$4/6Q&gt;JHY$XMR.887G2[M[UWP'[=7?B.B4I.Q]]#S&gt;9WQHZV8PH9Z[*62P5-RPTK0?[/V2LRCT?-0`A&gt;R'M,'9?V2&lt;5'@(]Q1.UX!-4J52JWS"(%&amp;D0-#L-&amp;U'%U0-#&lt;?A%4/.G&amp;.L#\KV1.!5GS@I@#[2EKUZ3CR:]Y29MN9OG4_;.V$=%$"`B55YZR+XCDRB09G2&lt;`Y(5T$*?+GQ?&gt;FOSVEZ(_HW[PG_.@2=F&gt;6T^&gt;YDS=4DC&lt;C;'-E4,X&lt;&gt;C6UL.Q/0:.^"A]WJ_K$)+_F)N[V)?HS0R^[F4@6Y0'&lt;_#7$C`.&lt;F-4E+B49[87AD(H]0YE47MN&amp;\?4;C0Y==TEL&lt;;%=2'V89.O+9D4ZGT]]&gt;-\XP-:0&gt;IM&gt;-:W\`+DB&lt;T$E4-/$=;E6=]%'"=Q91F93*-JA0#ZQTA(73&gt;JVSTPGIU$E$&gt;KW==W:H:`.8Q&lt;H3TE&amp;2L&gt;#V=.&lt;]'&lt;&lt;$W]QZ(--.)#HW_D&lt;`-0_#%)QT&amp;D&lt;9,&amp;QIO1E[VOE=A4KH.TZW/)]040[&amp;IPPOFN-VHEM`!'UQ;*`8$,,TLK[OYHHR;3887O"F:&amp;GG\_4S_1XY%L=WZGOX?P84@*NQQ=F19UUL',&amp;/4D)65CJC(;1;KD3*PM#E'#*'MY8^1J1G,B6KGG9ON);'_C)/BYO+(E*8%+O0C`;_;!/`0&lt;/65P2`673:&lt;1.V$2=_;M$8KFY6"LRR_FR-V=%1HH[#5=L:5`I'3^4+[:$&lt;[%&amp;I=P@$"M6\4U^$*"&lt;C':2RP8"1J5+PL4`U^`7(8P7-([WQZ,K1`,OM#YH(#WHS':L$C2NX8VS`R6\CL.$+VMG+Y$*7HO9DP+3V%!\^%@8X(HCY%\PA;3P]&amp;F\W,7=T^?0^BK:F(LJA`GV?BS\P4@^*31]^G00V7_N;0U.(RZ]LP.97&amp;,XR(ENO.,6IXI*/W'SHPHJ8Y@XUK&lt;5G7(AN:OO!5%ZB.UJ^W%SYRB8IN7:6B70&amp;Y@?Y\E,B]P@D!FX7^-_+WI"2L^4`U+7&amp;LLV?,.L_ZC$US9J:,E$=A[W=-##E\+[^+/[DHQ#%@=)?=1[X#C[9D=+=M*,[+@@&gt;)L5`B^1O=&gt;^R'_6X)E,D`]4NIBQ!!!!%!!!!31!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!'5!!!"V?*RD9'!I&amp;*"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```V=J9O4Y?O1;8.%2(TB4::9]BQ1!:1A:GA!!!!!!!!1!!!!(!!!$=!!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S&amp;!#!!!!!!!%!#!!Q`````Q!"!!!!!!#3!!!!"!!31(!!!AJ';7RF5G6G&lt;H6N!!!31$,`````#%:J&lt;'61982I!!"+!0(*-Y16!!!!!B*';7RF5G6B:'6S,GRW9WRB=X-54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!'E"1!!)!!!!"#E:J&lt;'63:7&amp;E:8)!!"R!5!!"!!)32GFM:6*F972F=CZM&gt;G.M98.T!!!"!!-!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S&amp;!#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!$!!!!!!!!!!%!!!!#!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!5!)!!!!!!!1!&amp;!!=!!!%!!-ETB"A!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ&amp;!#!!!!!!!%!"1!(!!!"!!$*-Y19!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9R1!A!!!!!!"!!A!-0````]!!1!!!!!!EA!!!!1!%E"Q!!)+2GFM:6*F:GZV&lt;1!!%E!S`````QB';7RF5'&amp;U;!!!3A$RS4/%&amp;1!!!!)32GFM:6*F972F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"J!5!!#!!!!!1J';7RF5G6B:'6S!!!=1&amp;!!!1!#%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!!1!$!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G55!)!!!!!!!1!&amp;!!-!!!%!!!!!!!A!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B&amp;!#!!!!!!!1!%E"Q!!)+2GFM:6*F:GZV&lt;1!!%E!S`````QB';7RF5'&amp;U;!!!3A$RS4/%&amp;1!!!!)32GFM:6*F972F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"J!5!!#!!!!!1J';7RF5G6B:'6S!!!=1&amp;!!!1!#%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!!1!$!!!!!&amp;"53$!!!!!%!!!!!!!!!!!!!!!%!!=!$1!!!!1!!!$#!!!!+!!!!!)!!!1!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&amp;2!!!#QXC=H6&amp;.4Y.!%(VUQ6+M&amp;&lt;'NN6K$&amp;Q]??L"XAT(JQ9N.YQ_1FE5R+!UMD5=0`DH`D@Y#H6VI;PR)ET)*T,[:?&lt;QX#_!)DD&gt;$R2J'-2`T]#F`"#&amp;HH`39%BPZYBY9Y?.N].I#5('+4D`A;4_?4W-`SZL8EQ=_&amp;2&gt;#J.%E&amp;TTL4U5-VROB1B/;`&lt;--8+X"VVXQ7=M*Y%9R&gt;@ZB;HS$*-=J=7BAP=MYTQ20X32U6:]\3[/Z,\A&lt;_-+(11%,!@XL82V9?=1!6&gt;3]+FA9X](U8FAS&amp;A8!ER!WM405K(?4Z5')$&gt;"G95*N%_&gt;L?'Z!?N[C&lt;VUJXW&lt;C/3"_C&gt;:BI`XH4EZ5L\03*5U;***?'EDG,2(J'"+ZCS:;SLG".FF?B&amp;('&lt;W2:W6.2L%N$JR3T;O5[5.K35TK/M5OZ4OJ-\&amp;#_D[[]";I&gt;5.&lt;&amp;)8J3..A8D;[;(Q!!!!!!!(=!!1!#!!-!"1!!!&amp;A!$Q1!!!!!$Q$9!.5!!!"B!!]%!!!!!!]!W!$6!!!!;A!0"!!!!!!0!.A!V1!!!(/!!)1!A!!!$Q$9!.5!!!"VA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!"-1"35V*$$1I!!UR71U.-1F:8!!!7[!!!"(=!!!!A!!!7S!!!!!!!!!!!!!!!)!!!!$1!!!2I!!!!(ER*1EY!!!!!!!!"?%R75V)!!!!!!!!"D&amp;*55U=!!!!!!!!"I%.$5V1!!!!!!!!"N%R*&gt;GE!!!!!!!!"S%.04F!!!!!!!!!"X&amp;2./$!!!!!!!!!"]%2'2&amp;-!!!!!!!!#"%R*:(-!!!!!!!!#'&amp;:*1U1!!!!!!!!#,%&gt;$2%E!!!!!!!!#1(:F=H-!!!!%!!!#6&amp;.$5V)!!!!!!!!#O%&gt;$5&amp;)!!!!!!!!#T%F$4UY!!!!!!!!#Y'FD&lt;$1!!!!!!!!#^'FD&lt;$A!!!!!!!!$#%.11T)!!!!!!!!$(%R*:H!!!!!!!!!$-%:13')!!!!!!!!$2%:15U5!!!!!!!!$7&amp;:12&amp;!!!!!!!!!$&lt;%R*9G1!!!!!!!!$A%*%3')!!!!!!!!$F%*%5U5!!!!!!!!$K&amp;:*6&amp;-!!!!!!!!$P%253&amp;!!!!!!!!!$U%V6351!!!!!!!!$Z%B*5V1!!!!!!!!$_&amp;:$6&amp;!!!!!!!!!%$%:515)!!!!!!!!%)!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(!!!!!!!!!!!`````Q!!!!!!!!#Q!!!!!!!!!!$`````!!!!!!!!!-1!!!!!!!!!!0````]!!!!!!!!!T!!!!!!!!!!!`````Q!!!!!!!!&amp;A!!!!!!!!!!$`````!!!!!!!!!7A!!!!!!!!!!0````]!!!!!!!!"F!!!!!!!!!!!`````Q!!!!!!!!(E!!!!!!!!!!$`````!!!!!!!!!@1!!!!!!!!!!0````]!!!!!!!!$X!!!!!!!!!!%`````Q!!!!!!!!0U!!!!!!!!!!@`````!!!!!!!!"!A!!!!!!!!!#0````]!!!!!!!!%'!!!!!!!!!!*`````Q!!!!!!!!1M!!!!!!!!!!L`````!!!!!!!!"$Q!!!!!!!!!!0````]!!!!!!!!%5!!!!!!!!!!!`````Q!!!!!!!!2I!!!!!!!!!!$`````!!!!!!!!"(Q!!!!!!!!!!0````]!!!!!!!!&amp;!!!!!!!!!!!!`````Q!!!!!!!!=%!!!!!!!!!!$`````!!!!!!!!#QA!!!!!!!!!!0````]!!!!!!!!,%!!!!!!!!!!!`````Q!!!!!!!!O=!!!!!!!!!!$`````!!!!!!!!%,!!!!!!!!!!!0````]!!!!!!!!1O!!!!!!!!!!!`````Q!!!!!!!"$!!!!!!!!!!!$`````!!!!!!!!%.!!!!!!!!!!!0````]!!!!!!!!20!!!!!!!!!!!`````Q!!!!!!!"&amp;%!!!!!!!!!!$`````!!!!!!!!&amp;,A!!!!!!!!!!0````]!!!!!!!!5Q!!!!!!!!!!!`````Q!!!!!!!"4)!!!!!!!!!!$`````!!!!!!!!&amp;01!!!!!!!!!A0````]!!!!!!!!74!!!!!!/2GFM:6*F972F=CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!2*';7RF5G6B:'6S,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!1!"!!!!!!!3!!!!!!1!%E"Q!!)+2GFM:6*F:GZV&lt;1!!%E!S`````QB';7RF5'&amp;U;!!!3A$RS4/%&amp;1!!!!)32GFM:6*F972F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"J!5!!#!!!!!1J';7RF5G6B:'6S!!"5!0(*-Y19!!!!!B*';7RF5G6B:'6S,GRW9WRB=X-/2GFM:6*F972F=CZD&gt;'Q!+E"1!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!"`````A!!!!"16%AQ!!!!"!!!!!!!!!!!!B&amp;*&lt;H"V&gt;&amp;.U=G6B&lt;3ZM&gt;GRJ9B.*&lt;H"V&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"%!A!A!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"9!!!!!B&amp;*&lt;H"V&gt;&amp;.U=G6B&lt;3ZM&gt;GRJ9B.*&lt;H"V&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T5&amp;2)-!!!!#9!!1!%!!!,37ZQ&gt;824&gt;(*F97U437ZQ&gt;824&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="FileReader.ctl" Type="Class Private Data" URL="FileReader.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="Open_Impl.vi" Type="VI" URL="../Open_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(+!!!!$1!M1(!!(A!!&amp;"*';7RF5G6B:'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!!1!!!!O1(!!(A!!&amp;"*';7RF5G6B:'6S,GRW9WRB=X-!!!Z';7RF5G6B:'6S)'^V&gt;!!!;U!7!!9%&lt;X"F&lt;A&gt;S:8"M97.F"G.S:7&amp;U:1ZP='6O)'^S)'.S:7&amp;U:2&amp;S:8"M97.F)'^S)'.S:7&amp;U:3.S:8"M97.F)'^S)'.S:7&amp;U:3"X;82I)'.P&lt;G:J=GVB&gt;'FP&lt;A!!#7^Q:8*B&gt;'FP&lt;A!,1!9!"("P=H1!!!V!!Q!(&gt;'FN:7^V&gt;!!91$$`````$F*F&lt;7^U:3"":'2S:8.T!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!(!!A!#2.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!":!5!!$!!=!#!!*#76S=G^S)'^V&gt;!"M!0!!%!!!!!%!!1!"!!)!!Q!%!!5!"A!"!!%!#A!"!!%!!1!,!Q!"#!!!EA!!!!!!!!!!!!!!!!!!!)U!!!!)!!!!#1!!!!A!!!!*!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!$1M!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">50331776</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Close_Impl.vi" Type="VI" URL="../Close_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%8!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!*!!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8397312</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="U8_Impl.vi" Type="VI" URL="../U8_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%A!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"1!#64A!!#Z!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Int32_Impl.vi" Type="VI" URL="../Int32_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%C!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!!Q!&amp;37ZU-T)!,E"Q!"Y!!"132GFM:6*F972F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,%"Q!"Y!!"132GFM:6*F972F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">268972048</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="String_Impl.vi" Type="VI" URL="../String_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%H!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!O1(!!(A!!&amp;"*';7RF5G6B:'6S,GRW9WRB=X-!!!Z';7RF5G6B:'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!M1(!!(A!!&amp;"*';7RF5G6B:'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="ByteArray_Impl.vi" Type="VI" URL="../ByteArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!5!"1!!)%"!!!(`````!!54&gt;7ZT;7&gt;O:71A9HFU:3"B=H*B?1!O1(!!(A!!&amp;"*';7RF5G6B:'6S,GRW9WRB=X-!!!Z';7RF5G6B:'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!,1!-!"7.P&gt;7ZU!#R!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*!!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Double32_Impl.vi" Type="VI" URL="../Double32_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)2'^V9GRF-T)!!#Z!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="WaveformArray_Impl.vi" Type="VI" URL="../WaveformArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!6!!$#(&gt;B&gt;G6G&lt;X*N!!!;1%!!!@````]!"1V898:F:G^S&lt;5&amp;S=G&amp;Z!#Z!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!!Q!&amp;9W^V&lt;H1!,%"Q!"Y!!"132GFM:6*F972F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!E!#A-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">268972560</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="FileReader_Create.vi" Type="VI" URL="../FileReader_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!',!!!!$!!I1(!!(A!!&amp;"*';7RF5G6B:'6S,GRW9WRB=X-!!!FP9GJF9X1A;7Y!+%"Q!"Y!!"=628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T!!:198*F&lt;H1!!"*!-P````])2GFM:6"B&gt;'A!!"*!)1V1=G6Q:7ZE:724;8JF!!1!!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!A1&amp;!!!Q!&amp;!!9!"R.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#J!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!#G^C;G6D&gt;#"P&gt;81!!":!5!!$!!5!"A!(#76S=G^S)'^V&gt;!#%!0!!&amp;!!!!!%!!A!$!!1!#!!%!!1!"!!%!!1!"!!%!!1!#1!%!!1!"!!%!!I$!!%1!!#3!!!!#A!!!!A!!!!)!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!$15!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%0!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!5%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!#G^C;G6D&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!"132GFM:6*F972F=CZM&gt;G.M98.T!!!*&lt;W*K:7.U)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!#1!!!!!!!!!!!!!!C1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117786640</Property>
		</Item>
	</Item>
</LVClass>
