﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="11008008">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">RedFrame</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">9464978</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16757617</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16773906</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">2</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeBaseTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">68069181-10ba-41d6-bc29-30d3e7915fe8</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,@!!!*Q(C=T:9`&lt;F."%-;`/#$R*!4+#5#O%"837&amp;3)SF&gt;QA91MKLF#K!BFLD!X!-N)[&gt;VQA+'EN-A*X%)$D^^&lt;&lt;`QPC&gt;UAE&gt;W-^\XPGZX^?8&gt;F23LNA820&lt;Q_V:G`$&lt;RH;JG5IU6Q^&gt;E0RS_/S.WWT^&lt;C=X[\HN_XK&gt;47`;:L^]ZNVD=V3V4`!@[D&gt;X^NW`6ZPH^]&lt;$]&lt;DXGU_&lt;I?UF&lt;(WK\O4M@,8\D*DVR^M_&gt;0L`HB&gt;94K&gt;$G\Q;]:U?KN0RHC[V[`WX@0(7XY:N`&gt;`-&amp;DZ@-VFWP&lt;Z$:&lt;_BLN\`C7DG;\N;`&gt;HM/6?^WE&lt;\IX_6DNYP@E&gt;_.D_ZS9&gt;3&gt;X9NEE%Y93JM^J%4`2%4`2%4`2!$`2!$`2!$X2(&gt;X2(&gt;X2(&gt;X2$.X2$.X2$.`43U95O&gt;+'T+]HCS5**U;2!EAS+EI_%*_&amp;*?")?8J8Q*$Q*4]+4]*#CB#@B38A3HI3(;5JY%J[%*_&amp;*?#B63,*U&gt;(A3(MILY!FY!J[!*_"B315]!5#Q7&amp;!Y+!+'!D.Y#(A#HI#(2Q5]!5`!%`!%0.A+?!+?A#@A#8C95H9F#EX8U?'BD"Q?B]@B=8A=(EL,Y8&amp;Y("[(R_&amp;B/4E]$I]$Y3TI&amp;!&gt;"TC1HQ8FR?"Q?"DE]$I`$Y`!Y0&amp;DFB,TM4%@4&gt;82Y$"[$R_!R?!Q?3MDA-8A-(I0(Y+'M$"[$R_!R?!Q?FJ,"9`!90!;)M3D,SSBG4$33$-(AY;`=&amp;COH&amp;)8%3J@KZ67^F+K84@53K6Y/V5.805T61V,&gt;@.6.6&gt;UMV5V1`8+KU+IQKIOI4OY3N?"T4MS)#8&amp;/H")D9EDUC:.O[D^/8#Q7GM`HGMVGGEQG/D]`V_HJK5;DE9&lt;$I@L^PEZ/4H46HN+8\;D\8&lt;K3_RE8,T_0HDX`^PWCT^D`]O;4&gt;FPP+``2P+$'1W9?)R$P(EHP8UM@`EBH.U4&lt;8OL*\V^[`0.3RT]I=-;]6ZN6\]"PIXJ;DKM^_AOR6X0`!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.8</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"^J5F.31QU+!!.-6E.$4%*76Q!!'T!!!!1Z!!!!)!!!'R!!!!!:!!!!!22.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!!!!!!)A2!)!)!$!!!!A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!A$"C`LR(UE/:Y7?204CS/!!!!!Q!!!!1!!!!!"YQ=.+@NFF&amp;M:!'`7@5.X@5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#`UZ%TE"]J19=&lt;SBJS95U3!!!!%",ZY`XS?\PO)WP@O)TK-BU!!!!1XCSI%SUW,'790EGOI$MG*1!!!"!Q8SQLY:EU-/#!A9(FPE=_!!!"#1!"4&amp;:$1S6.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=TJ.:7VP=HF3:7&amp;E:8)O9X2M!!!!!!!$!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!*735.$!!!!!!%54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!5&amp;2)-!!!!"M!!1!$!!!54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!!!!#!!%!!!!"!!%!!!!!!!A!!!!!!!!!!!!!!!!!!F:*1U-!!!!!!!!"$F*F972F=F2Z='5O9X2M!&amp;"53$!!!!!6!!%!!Q!!$F*F972F=F2Z='5O9X2M!!!!!A!#!!!!!1!"!!!!!!!)!!!!!!!!!!!!!!!!!!-!!!!!!!!#!!-!!!!!!#%!!!!C?*RDY'&gt;A%7!19,2A!**-"AQ#(!)1$'="!"GC!7]!!!!!!!"&amp;!!!"*(C=9W$!"0_"!%AR-D!Q@103L'DC9"L'JD:!.B?&lt;(&lt;DMR3\/#"2D?A*EM!!R%V1.'U3/[1Y1@U$8Q9&lt;&amp;&lt;!#AECCD!!!!!!!!.!!"6EF%5S6.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=TJ.:7VP=HF3:7&amp;E:8)O9X2M!!!!!!!!!!-!!!%0!!!"Q(C=5W"E9-AUND",!.,-$"#1H*_3#K,8!-7=I7+'"Q[$;2D`-%QNF'Z_QW*YK@E)S]P(````"`)@)]3\861Y'CLE'@B&lt;R9!#RRN='#&amp;3H3QK,]"+DL!=.DQ!)I%=:3;)*&amp;!44\?0#ENHIR-TG#@1W?A'9I(ZH9V?9,9&lt;G/U(9D.U4Q3,#6E?YG]V!BE,6M03$:&lt;O$7$JH!BCJ(5\M%"5JFES]5]ZQ,`NQA[1?`B&gt;$RU'[A1*A(WKT)DC`G[/YYYC#3$H/WL-!&amp;-/(G$+29%2V3&gt;![N+P`YRP`D-;A(!^E+`$Q-DA#63G"K2"RL)T1$T*S-$#I-.IT'D&amp;[!CV$4^Q^H&gt;R2?;$YA=![_J7S!!!!!!/%1'!&amp;1!!"D%R,D!O-1!!!!!!!!Q2!)!)!!!%-4%O-!!!!!!/%1'!&amp;1!!"D%R,D!O-1!!!!!!!!Q2!)!)!!!%-4%O-!!!!!!/%1'!&amp;1!!"D%R,D!O-1!!!!!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!)MS:M$;J63AKT&gt;7Q)KF6+#+N7;A!!!!!!!!!!!!!!!!!!!!!!!,!!!!)-!!!)!Q!!)!$!!!!!Q!!9!Y!!(A_!!"_`A!!@`Y!!(`_!!"``A!!@`Y!!(`_!!$``_!!(`XY!!`XY!!$X]!!!(]!!!!]!!!!!!!!!!!!!!!#!.X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X1!!!!!!!!!!!!!!!!!!!.U0!!]0]!`Q$Q$`!0]0]!$&gt;$`$`$Q]0!0$Q]0$Q$Q]!X1]0$Q`Q$`$`]0$Q`Q`Q!.U0!!]0$Q]!]0$Q]0!0$Q$&gt;$Q!0$Q]0]0$Q`Q$`$Q]!X1!!!!!!!!!!!!!!!!!!!.X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;%2%2%2%2%2%2%2%2%2%2X2%2%2%2%2%2%2%2%2%2%&gt;U2%2%2%2%&lt;K\%2%2%2%2(&gt;%2%2%2%&lt;I!!,M2%2%2%2X2%2%2%&lt;I!!!!!OR%2%2%&gt;U2%2%&lt;I!!!!!!!#\%2%2(&gt;%2%2'K!!!!!!!!_R%2%2X2%2%2K\I!!!!!``I2%2%&gt;U2%2%;O\OA!!```[%2%2(&gt;%2%2'LO\O[P```_B%2%2X2%2%2K\O\O`````I2%2%&gt;U2%2%;O\O\P````[%2%2(&gt;%2%2'LO\O\````_B%2%2X2%2%2K\O\O`````I2%2%&gt;U2%2%;O\O\P````[%2%2(&gt;%2%2'\O\O\````O``R%2X2%2%2'KO\O```O[````%&gt;U2%2%2%;O\P`O[````%2(&gt;%2%2%2%2K\OQ````]2%2X2%2%2%2%2'A````]2%2%&gt;U2%2%2%2%2%2``]2%2%2(&gt;%2%2%2%2%2%2%2%2%2%2X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;!!!%!&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8!E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#6R=#@]*#1H`#@``#1H``QE*`QE*``]*#@``#@``#1E*8&amp;Q*``]*``]*`QH`#@]*#@]*`QH`#@]*`QE*`QH`#1F=8!H`#@]*`QH``QE*``]*````#@]*`QH``QH``QE*#6R=#@]*#1H`#@]*`QH`#1H`#@]*`QH`#@]*#@]*`QE*8&amp;Q*`QE*#@]*`QH`#@``#@]*`QH``QE*``]*`QH`#1F=8!E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#1E*#6R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;Q&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"16=8!5&amp;"15&amp;"15&amp;"15&amp;"15&amp;!A)&amp;"15&amp;"15&amp;"15&amp;"15&amp;"6R="15&amp;"15&amp;"15&amp;"15&amp;!FYU8FY#"15&amp;"15&amp;"15&amp;"15&amp;8&amp;Q&amp;"15&amp;"15&amp;"15&amp;!FYU#1E*#6Z?!A5&amp;"15&amp;"15&amp;"16=8!5&amp;"15&amp;"15&amp;!FYU#1E*#1E*#1F?8A)&amp;"15&amp;"15&amp;"6R="15&amp;"15&amp;"6YU#1E*#1E*#1E*#1E*8FY&amp;"15&amp;"15&amp;8&amp;Q&amp;"15&amp;"15&amp;.$1*#1E*#1E*#1E*#1GM8A5&amp;"15&amp;"16=8!5&amp;"15&amp;"15U8FYU#1E*#1E*#1GML+QU"15&amp;"15&amp;"6R="15&amp;"15&amp;"42?8FZ?.!E*#1GML+SML$1&amp;"15&amp;"15&amp;8&amp;Q&amp;"15&amp;"15&amp;.&amp;Z?8FZ?8D2?L+SML+SM.!5&amp;"15&amp;"16=8!5&amp;"15&amp;"15U8FZ?8FZ?8KSML+SML+QU"15&amp;"15&amp;"6R="15&amp;"15&amp;"42?8FZ?8FZ?L+SML+SML$1&amp;"15&amp;"15&amp;8&amp;Q&amp;"15&amp;"15&amp;.&amp;Z?8FZ?8F[ML+SML+SM.!5&amp;"15&amp;"16=8!5&amp;"15&amp;"15U8FZ?8FZ?8KSML+SML+QU"15&amp;"15&amp;"6R="15&amp;"15&amp;"42?8FZ?8FZ?L+SML+SML$1&amp;"15&amp;"15&amp;8&amp;Q&amp;"15&amp;"15&amp;8FZ?8FZ?8F[ML+SML+R?8KSML!5&amp;"16=8!5&amp;"15&amp;"15&amp;.$2?8FZ?8KSML+R?8D3ML+SML+Q&amp;"6R="15&amp;"15&amp;"15&amp;"42?8FZ?L+R?8D3ML+SML+Q&amp;"15&amp;8&amp;Q&amp;"15&amp;"15&amp;"15&amp;"15U8FZ?8AGML+SML+SM"15&amp;"16=8!5&amp;"15&amp;"15&amp;"15&amp;"15&amp;.!GML+SML+SM"15&amp;"15&amp;"6R="15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"17ML+SM"15&amp;"15&amp;"15&amp;8&amp;Q&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"15&amp;"16=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;Q!!!!#!!-!!!!!!25!!5:13&amp;!F476N&lt;X*Z5G6B:'6S,GRW9WRB=X-[476N&lt;X*Z5G6B:'6S,G.U&lt;!!!!!!!!A!#6%2$1Q!!!!%54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!5&amp;2)-!!!!"M!!1!$!!!54W*K:7.U182U=GFC&gt;82F=SZD&gt;'Q!!!!#!!!!!!!"!!%!!!!!!!A!!!!!!!!!!!!!!!!!!!!"!!!!&amp;V"53$!!!!!!!!!!!!!#6%2$1Q!!!!!!!!%/5G6B:'6S6(FQ:3ZD&gt;'Q!5&amp;2)-!!!!"5!!1!$!!!/5G6B:'6S6(FQ:3ZD&gt;'Q!!!!#!!!!!!!"!!%!!!!!!!A!!!!!!!!!!!!!!!!!!!!"!!!"!&amp;"53$!!!!!!!!!!!!!$!!!!!!!'`Q!!&amp;&amp;^YH-69&lt;7R4:21_&lt;^&gt;VN[0&lt;&lt;P&gt;&gt;X'B8&lt;M?1$3@A&amp;]R%W!5$YI)&lt;#SIB7.9#SX$$&gt;AQ8#"CN*&amp;0Y14!945AU*P/(U@V9&amp;$6I%#K*;7+!(S)@M7#)52E@):G-Q&gt;XVP0@\XH&lt;N2"*+&gt;H08P?=^TTXH?:\X8!"+V\+6NAH9*1"B2`&amp;GJ1$ZI11"C$=QI(T9&gt;H)(3+G(#,#/;7@0WS:)F1#O5++7;?1(Y!9O&amp;9_,C_!@MJW^B%M&gt;L!&gt;XSB?A+*3I=+`AECQX6-5.Z+J&lt;OK';X5=G&lt;#^SXFP-PFA0:I.9(&lt;W[']A%%,\'&lt;I`Z6Q7\QT'/@ONM9$T3FEY"7$Z2%/'3MX"(4(V-WN)WB`BN4?K7A&amp;P7Q@$QM"\EFI.K*2B09ATR!Z$ZNDE:9ILZ2(753^:,-@F3$/;:L_&lt;BP2-(+(9;:!UNY2-MBG,=[]IDUVAV4HAG&lt;&gt;RG!5KZZ",'QVQ+H=MN?#&amp;W'!C1_(J'`%Z]B];\7WA8J,8&amp;W)D!1M+WY8W&lt;!*7RB/U6M..=O6!+A`#JV!;\WI&lt;JN!X,J$9Y_1%7^$Y=S.Q(HQXY'NY?^V=X&lt;^E7\1V(@$U&lt;@2V&lt;AN'I&lt;WOEMS`9'`;&amp;ALX"V#Y^R#=]4^!+U)131;!#\(!)&gt;BMLXA/$AY.9",TKI6597M%FN&lt;A3X0JD?%OL?ECP(MWK6[];KR&gt;Y#1)P!T_W?B/N9W#BT5$&gt;#JW[0K3OG\5C-V(8@X_ISQ&lt;EJO(@A+MDC#JAL..-*.EO#ZNB"_S"P2G9S=F"/JNR0=$\M#.$4!"D&gt;BP:D$%\-':P&gt;D&lt;8JL#:RFL9&lt;'UE^G/7VA_:UX:#:%\(4IKHR"(+\!HR)H(!*Q:GB\"0G-]H&gt;6[]#R_!&lt;#EZ;F`GK&amp;TGB\QLT'2?H98-$I8-LO@$L`:%_FP$Q6!Y9O%OYKZ(X0R9Q80+.[CPT[C_7$O@^+Z1'5EL9.,9ZR28CUFDYF5!%5MY*6C=(V*F^%7KD/TYLQ?,EEV'1R9:U5^5&lt;^PE-JK82E9ZEIRMEIS_JV=#OJC'47*3]:H%^/8^0Q?_3K?=+(2",)-+$K=IJQOD&gt;E)U1]T8+=LJQDQ\V4S:F0..KH+[V&amp;S;=AY?0'A^"\\.I*T@R&gt;N5/=@%-R#%.S8FW"4F%&amp;#6YR'P1T/UG*2$(/G6-QX&lt;5$?&amp;.KTOXWJNQW;"&amp;"L9IEFGD5"+1AG\LBA7IR['^2+?0"6072L&amp;X%!Y"%KTQ%GHFIM#K=+=(G2:PO'9S]-K6=*L;MV&amp;NV4TU[&gt;09]X(R]@F(7-V])&lt;&gt;&amp;O0I.]['7'^A&lt;O.':&gt;M#A63DEF!5,I-)=[%)SG(N&amp;(;VK\P'Z^9ST4V&lt;_XE],VXNU8"L?'-Y%O\O#"@B,\)BL9FUYB'L*(9*R)?*(_?3.CFRIW:U,H"BMC.(DG!SP,K^-A8)2)YP\N78_+1F.@4+_B7G&gt;D"_OHXK5EL#'HIV,X6[&gt;42_O1QZ%BI@I7B='&amp;^Z\WAW4"X."B-;JU"G7OS.)('G)]0-^C:VR'RPFYXW&amp;J&lt;NT6\LA^I;M&gt;90\CM)(ZR8Z/^TF&lt;"W&gt;$U_)86!`"7FZ9%.*CL$54\B&gt;FDH+Y,#-BF,BPGK?%F`&lt;XBR*",MVXC2;HE`])H+2\EEQ3S+C::#$CS!G?J48`;FWN=R$(K-3Q&lt;VI(LM72-MG]TSJAFQ8$ZWN/+[%=BC7'EU/&lt;X05LF(Z%:+9G".H9)@=&lt;.&amp;ZD/M(!85"-^;Z*0'1%]A_+?YJ%=(\R&lt;`A%:-L)#`.NEIX7XUJ@@E&gt;DKE?&lt;F6A*`2*ZO-9"9J"Z:4&lt;?D*..ZU%W:DO@&gt;HN=KYX^X?(?X=V"U/;6WV.(/6!'&gt;##:?H$QVS#4=UARN9I+K\#G:,5()66Y/TV.8[6&amp;?4-?&gt;B3SIB9#EA_MQ58=UFQ,F5/2&gt;+[1OTS&lt;E1FUR2TP,3&lt;(*'./&gt;F.&amp;;L+\RX.'GM&lt;D)U(2:TA1O):LO"`TF1)05F[_TUG^&amp;=/G87Z?H\`IH\LO'3-X1[SRWP.U^T;94QFYSISM3""KC"&lt;AM(UES#@WO4EF/&gt;F0*AH2YL5^G3=%3&lt;F*T+J-1;][7.O;J.3EZF5G,.?59H?\RL_(DNJI,0BA"%MB@]OL(AG_7#-Z,-57)X1QE(O]R;,Z.RD_K$[8_=1?RJ:R#Y2&gt;5[T[R7?6JY_P_I&gt;3S^0B\-+!#XD&gt;YBI[(?-@U"D!*Y7IUDGI6=MEBOY6'^YG7)ZOSZ#YBGWZ[0*$3Z[C%VVKA@7'/0'/\H'OY&lt;^0N&lt;J]Q/=1&gt;TFK3-(YOT%`;OA&lt;#NUW4#/L8RAWEL,K/PQJQ(]MO+S^8&lt;]O)+^&lt;;#;5-E,1&lt;$9-[@W-&lt;1&amp;WL[)W_9`X&lt;J4]0P&lt;EGX%*10U]&lt;R9K9&amp;\&amp;)%P&amp;O!-L[Z_#C\H/:HFD.,W50YUI&gt;PA?8-)79U`IPW`X\RF6JE\#1:=@2R(XK:SH]"1$H:9!!!!!!%!!!!:Q!!!$1!!5*%3&amp;!F476N&lt;X*Z5G6B:'6S,GRW9WRB=X-[476N&lt;X*Z5G6B:'6S,G.U&lt;!!!!!!!!!!$!!!!:1!!!(6YH'.A9#A5E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````6SFCZ0B[Z"J=U2%@/&amp;.FFDS("!"F#"G;!!!!!!!!"!!!!!=!!!4\!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)2!)!)!!!!!1!)!$$`````!!%!!!!!!2E!!!!'!!5!"1!!(E"!!!(`````!!!267ZT;7&gt;O:72#?82F18*S98E!(U"Q!#!!!1!"!"*#?82F18*S98F3:7:F=G6O9W5!!'-!]1!!!!!!!!!#&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T$F*F972F=F2Z='5O9X2M!$&gt;!&amp;A!$#%.P=(F%982B$&amp;6T:6*F:G6S:7ZD:1^6=W6.:7VP=HF8=GFU:8)!"&amp;2Z='5!!%Y!]=GTCWQ!!!!#&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"R!5!!#!!)!!QR.:7VP=HF3:7&amp;E:8)!!"Z!5!!"!!15476N&lt;X*Z5G6B:'6S,GRW9WRB=X-!!!%!"1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)2!)!)!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!-!!!!!!!!!!1!!!!)!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N="%!A!A!!!!"!!5!"Q!!!1!!S&lt;/,&lt;A!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!2!)!)!!!!!1!&amp;!!=!!!%!!-GTCWY!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D%1#!#!!!!!%!#!!Q`````Q!"!!!!!!%:!!!!"A!&amp;!!5!!"Z!1!!"`````Q!!%66O=WFH&lt;G6E1HFU:5&amp;S=G&amp;Z!"^!=!!A!!%!!1!31HFU:5&amp;S=G&amp;Z5G6G:8*F&lt;G.F!!"D!0%!!!!!!!!!!B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=QZ3:7&amp;E:8*5?8"F,G.U&lt;!!X1"9!!QB$&lt;X"Z2'&amp;U91R6=W63:7:F=G6O9W5068.F476N&lt;X*Z6X*J&gt;'6S!!25?8"F!!"/!0(*MYNM!!!!!B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!=1&amp;!!!A!#!!--476N&lt;X*Z5G6B:'6S!!!?1&amp;!!!1!%&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!"!!5!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:2%!A!A!!!!"!!5!!Q!!!1!!!!!!"A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%2!)!)!!!!"A!&amp;!!5!!"Z!1!!"`````Q!!%66O=WFH&lt;G6E1HFU:5&amp;S=G&amp;Z!"^!=!!A!!%!!1!31HFU:5&amp;S=G&amp;Z5G6G:8*F&lt;G.F!!"D!0%!!!!!!!!!!B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=QZ3:7&amp;E:8*5?8"F,G.U&lt;!!X1"9!!QB$&lt;X"Z2'&amp;U91R6=W63:7:F=G6O9W5068.F476N&lt;X*Z6X*J&gt;'6S!!25?8"F!!"/!0(*MYNM!!!!!B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!=1&amp;!!!A!#!!--476N&lt;X*Z5G6B:'6S!!!?1&amp;!!!1!%&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!"!!5!!!!!!!!!!!!!!!!!!!1!#A!4!!!!"!!!!3-!!!!I!!!!!A!!"!!!!!!8!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&lt;Q!!!19?*S&gt;5MF/QT!1@7G;UE*I#Z3S5\-?E/A&amp;);YJ)'YMKE"Q*#1/+AJ.Z&lt;A6P8(H=`A+0I=PA)H&gt;MAJ"]3C/07/`G@@'!.:AE7(2=7#]U!$'TJJRY\L*`&gt;WOZ$5BX#YK4AM-"NHYG\0/!SZYU_/!BW@IE3I&gt;]NN)5.$VO;C'(3^UYTCPN[@&gt;&amp;K^[-M3/5Y;:X9N;X8V8OP::T.`!#L42%/?C)&lt;F!/LE&amp;V0(]^0A1`JCC&gt;(RVQTV:EV)ULNK3RSI2=U[1)D/,8]0!U&lt;]1Z`O)^M=\Q)6#;`[)6PTE4*!W#-G!N&lt;!8NG-CSK+!K:/M*2I&gt;6X,GET99BE_1;=+V9%*PM95MW37N&lt;0)E"?8IPT7YKN-^.P98.OOKNN&amp;@;`-3PH1SZQT"$-*L:*V\-[J,\?"2A#)BG3BC$/.GWQ]QAPZ,QP;!LS;($/$]KWNZ*6)*E\!6M\)J\`S?&gt;"/9)GV4C=#::+,+N,:J(.!"BBH-^OK=5\JL[Y`PHP@)P$,&gt;-Q-,06F`&lt;\GHMBN)WK[,J!?(4@S^P]2D#*1\4@=SN"QB#1K9RC)KR-CCDR%GI_A3?3J9RAKN5VB.[-.["8XM^CI!!!"X!!%!!A!$!!5!!!"9!!]%!!!!!!]!W!$6!!!!91!0"!!!!!!0!.A!V1!!!'I!$Q1!!!!!$Q$9!.5!!!"TA!#%!)!!!!]!W!$6!!!!&gt;9!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!4%!5F.31QU+!!.-6E.$4%*76Q!!'T!!!!1Z!!!!)!!!'R!!!!!!!!!!!!!!!#!!!!!U!!!%+!!!!"R-35*/!!!!!!!!!7"-6F.3!!!!!!!!!8236&amp;.(!!!!!!!!!9B01F.(!!!!!!!!!:R$1V.(!!!!!!!!!&lt;"-38:J!!!!!!!!!=2$4UZ1!!!!!!!!!&gt;B544AQ!!!!!!!!!?R%2E24!!!!!!!!!A"-372T!!!!!!!!!B2735.%!!!!!!!!!CBW:8*T!!!!"!!!!DR(1V"3!!!!!!!!!K"*1U^/!!!!!!!!!L2J9WQU!!!!!!!!!MBJ9WQY!!!!!!!!!NR$5%-S!!!!!!!!!P"-37:Q!!!!!!!!!Q2'5%BC!!!!!!!!!RB'5&amp;.&amp;!!!!!!!!!SR-37*E!!!!!!!!!U"#2%BC!!!!!!!!!V2#2&amp;.&amp;!!!!!!!!!WB73624!!!!!!!!!XR%6%B1!!!!!!!!!Z".65F%!!!!!!!!![2)36.5!!!!!!!!!\B71V21!!!!!!!!!]R'6%&amp;#!!!!!!!!!_!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!0````]!!!!!!!!!L!!!!!!!!!!!`````Q!!!!!!!!$!!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!(Y!!!!!!!!!!$`````!!!!!!!!!A!!!!!!!!!!!0````]!!!!!!!!#+!!!!!!!!!!!`````Q!!!!!!!!*U!!!!!!!!!!$`````!!!!!!!!!KQ!!!!!!!!!"0````]!!!!!!!!$Q!!!!!!!!!!(`````Q!!!!!!!!05!!!!!!!!!!D`````!!!!!!!!!_1!!!!!!!!!#@````]!!!!!!!!$_!!!!!!!!!!+`````Q!!!!!!!!1)!!!!!!!!!!$`````!!!!!!!!""Q!!!!!!!!!!0````]!!!!!!!!%-!!!!!!!!!!!`````Q!!!!!!!!3U!!!!!!!!!!$`````!!!!!!!!"LA!!!!!!!!!!0````]!!!!!!!!+P!!!!!!!!!!!`````Q!!!!!!!!L%!!!!!!!!!!$`````!!!!!!!!#_!!!!!!!!!!!0````]!!!!!!!!3Z!!!!!!!!!!!`````Q!!!!!!!",M!!!!!!!!!!$`````!!!!!!!!%S1!!!!!!!!!!0````]!!!!!!!!4E!!!!!!!!!!!`````Q!!!!!!!"/9!!!!!!!!!!$`````!!!!!!!!'*A!!!!!!!!!!0````]!!!!!!!!9I!!!!!!!!!!!`````Q!!!!!!!"CI!!!!!!!!!!$`````!!!!!!!!'.1!!!!!!!!!A0````]!!!!!!!!;F!!!!!!1476N&lt;X*Z5G6B:'6S,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!22.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!!!!%!!1!!!!!!#!!!!!!'!!5!"1!!(E"!!!(`````!!!267ZT;7&gt;O:72#?82F18*S98E!(U"Q!#!!!1!"!"*#?82F18*S98F3:7:F=G6O9W5!!'-!]1!!!!!!!!!#&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T$F*F972F=F2Z='5O9X2M!$&gt;!&amp;A!$#%.P=(F%982B$&amp;6T:6*F:G6S:7ZD:1^6=W6.:7VP=HF8=GFU:8)!"&amp;2Z='5!!%Y!]=GTCWQ!!!!#&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!"R!5!!#!!)!!QR.:7VP=HF3:7&amp;E:8)!!&amp;A!]=GTCWY!!!!#&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T%%VF&lt;7^S?6*F972F=CZD&gt;'Q!+E"1!!%!""V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!5!!!!"`````A!!!!!!!!!!!!!!!B&amp;*&lt;H"V&gt;&amp;.U=G6B&lt;3ZM&gt;GRJ9B.*&lt;H"V&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"%!A!A!!!!!!!!!!!!"!!!!%E:J&lt;'63:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"9!!!!!B&amp;*&lt;H"V&gt;&amp;.U=G6B&lt;3ZM&gt;GRJ9B.*&lt;H"V&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T5&amp;2)-!!!!#9!!1!%!!!,37ZQ&gt;824&gt;(*F97U437ZQ&gt;824&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="MemoryReader.ctl" Type="Class Private Data" URL="MemoryReader.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ReadByte.vi" Type="VI" URL="../ReadByte.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#-!!!!"A!,1!5!"%*Z&gt;'5!!"F!!Q!41X6S=G6O&gt;&amp;*F9721&lt;X.J&gt;'FP&lt;A!&amp;!!5!!"Z!1!!"`````Q!#%66O=WFH&lt;G6E1HFU:5&amp;S=G&amp;Z!"^!=!!A!!%!!Q!31HFU:5&amp;S=G&amp;Z5G6G:8*F&lt;G.F!!!?!0!!!Q!!!!%!"!-!!"!!!!E!!!!)!!!!#!!!!!!"!!5!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">44040208</Property>
		</Item>
		<Item Name="Read4Bytes.vi" Type="VI" URL="../Read4Bytes.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#4!!!!"A!&amp;!!5!!"*!1!!"`````Q!!"5&amp;S=G&amp;Z!"F!!Q!41X6S=G6O&gt;&amp;*F9721&lt;X.J&gt;'FP&lt;A!?1%!!!@````]!!"&amp;6&lt;H.J:WZF:%*Z&gt;'6"=H*B?1!@1(!!)!!"!!-!%E*Z&gt;'6"=H*B?6*F:G6S:7ZD:1!!(A$Q!!-!!1!#!!1$!!!1!!!*!!!!#!!!!!A!!!!!!1!&amp;!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">44040208</Property>
		</Item>
		<Item Name="Read8Bytes.vi" Type="VI" URL="../Read8Bytes.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#4!!!!"A!&amp;!!5!!"*!1!!"`````Q!!"5&amp;S=G&amp;Z!"F!!Q!41X6S=G6O&gt;&amp;*F9721&lt;X.J&gt;'FP&lt;A!?1%!!!@````]!!"&amp;6&lt;H.J:WZF:%*Z&gt;'6"=H*B?1!@1(!!)!!"!!-!%E*Z&gt;'6"=H*B?6*F:G6S:7ZD:1!!(A$Q!!-!!1!#!!1$!!!1!!!*!!!!#!!!!!A!!!!!!1!&amp;!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">44040208</Property>
		</Item>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="ToBytes_Impl.vi" Type="VI" URL="../ToBytes_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!5!"1!!%E"!!!(`````!!5&amp;9HFU:8-!-E"Q!"Y!!"95476N&lt;X*Z5G6B:'6S,GRW9WRB=X-!!"".:7VP=HF3:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$"!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!0476N&lt;X*Z5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!"!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Open_Impl.vi" Type="VI" URL="../Open_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(3!!!!$1!O1(!!(A!!&amp;B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!'N!&amp;A!'"'^Q:7Y(=G6Q&lt;'&amp;D:1:D=G6B&gt;'5/&lt;X"F&lt;C"P=C"D=G6B&gt;'52=G6Q&lt;'&amp;D:3"P=C"D=G6B&gt;'5D=G6Q&lt;'&amp;D:3"P=C"D=G6B&gt;'5A&gt;WFU;#"D&lt;WZG;8*N982J&lt;WY!!!FP='6S982J&lt;WY!#U!'!!2Q&lt;X*U!!!21!-!#H2J&lt;76P&gt;81A&lt;8-!!"B!-0````]/5G6N&lt;X2F)%&amp;E:(*F=X-!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!=!#!!*%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!&amp;E"1!!-!"Q!)!!E*:8*S&lt;X)A&lt;X6U!'Q!]!!1!!!!!1!"!!%!!A!$!!1!"1!'!!%!!1!+!!%!!1!"!!M#!!%)!!#3!!!!!!!!!!!!!!!!!!!!D1!!!!A!!!!*!!!!#!!!!!E!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!!!!!!.#Q!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Close_Impl.vi" Type="VI" URL="../Close_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$"!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!"95476N&lt;X*Z5G6B:'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="U8_Impl.vi" Type="VI" URL="../U8_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%E!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"1!#64A!!$"!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!"95476N&lt;X*Z5G6B:'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Int32_Impl.vi" Type="VI" URL="../Int32_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!!Q!&amp;37ZU-T)!-%"Q!"Y!!"95476N&lt;X*Z5G6B:'6S,GRW9WRB=X-!!!Z';7RF5G6B:'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!O1(!!(A!!&amp;B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">268972048</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="String_Impl.vi" Type="VI" URL="../String_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%L!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!Q1(!!(A!!&amp;B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#Z!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Unsigned16Array_Impl.vi" Type="VI" URL="../Unsigned16Array_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!5!"A!!(%"!!!(`````!!5067ZT;7&gt;O:71R.E&amp;S=G&amp;Z!$"!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!#U!$!!6D&lt;X6O&gt;!!O1(!!(A!!&amp;B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*)!!!!!!1!,!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="ByteArray_Impl.vi" Type="VI" URL="../ByteArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!5!"1!!)%"!!!(`````!!54&gt;7ZT;7&gt;O:71A9HFU:3"B=H*B?1!Q1(!!(A!!&amp;B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!!Q!&amp;9W^V&lt;H1!,E"Q!"Y!!"95476N&lt;X*Z5G6B:'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#3!!!!!!%!#Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Double32_Impl.vi" Type="VI" URL="../Double32_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%K!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)2'^V9GRF-T)!!$"!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!/2GFM:6*F972F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,E"Q!"Y!!"95476N&lt;X*Z5G6B:'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143248</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="WaveformArray_Impl.vi" Type="VI" URL="../WaveformArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;3!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!6!!$#(&gt;B&gt;G6G&lt;X*N!!!=1%!!!@````]!"1^6&lt;H.J:WZF:$%W18*S98E!-%"Q!"Y!!"95476N&lt;X*Z5G6B:'6S,GRW9WRB=X-!!!Z';7RF5G6B:'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!,1!-!"7.P&gt;7ZU!#Z!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!E!#A-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="ReaderType.ctl" Type="VI" URL="../ReaderType.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="MemoryReader_Create.vi" Type="VI" URL="../MemoryReader_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*@!!!!$Q!Q1(!!(A!!&amp;B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!$UVF&lt;7^S?6*F972F=C"J&lt;A!I1(!!(A!!&amp;R6&amp;?'6D&gt;82J&lt;WZ#98.F,GRW9WRB=X-!"F"B=G6O&gt;!!!,E"Q!"Y!!"95476N&lt;X*Z6X*J&gt;'6S,GRW9WRB=X-!!!R.:7VP=HF8=GFU:8)!!!5!"1!!(E"!!!(`````!!-267ZT;7&gt;O:72#?82F18*S98E!(U"Q!#!!!1!%!"*#?82F18*S98F3:7:F=G6O9W5!!'-!]1!!!!!!!!!#&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T$F*F972F=F2Z='5O9X2M!$&gt;!&amp;A!$#%.P=(F%982B$&amp;6T:6*F:G6S:7ZD:1^6=W6.:7VP=HF8=GFU:8)!"&amp;2Z='5!!!R!)1:T&gt;'&amp;U&gt;8-!!!N!!Q!%9W^E:1!!%%!Q`````Q:T&lt;X6S9W5!!#"!5!!$!!=!#!!*%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!"!!!!$*!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!1476N&lt;X*Z5G6B:'6S)'^V&gt;!!!&amp;E"1!!-!"Q!)!!E*:8*S&lt;X)A&lt;X6U!*E!]!!5!!!!!1!#!!5!"A!+!!M!#Q!,!!M!#Q!,!!M!#Q!-!!M!#Q!,!!M!$1-!!2!!!!I!!!!+!!!!#!!!!!A!!!!)!!!!#A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!!."1!6!!!!!!!!!!!!!!!!!!!"!!!!!!!!!!%!$A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">44044304</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%4!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#R!=!!?!!!7&amp;%VF&lt;7^S?6*F972F=CZM&gt;G.M98.T!!!+&lt;W*K:7.U)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!K1(!!(A!!&amp;B2.:7VP=HF3:7&amp;E:8)O&lt;(:D&lt;'&amp;T=Q!!#7^C;G6D&gt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!E!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
	</Item>
</LVClass>
