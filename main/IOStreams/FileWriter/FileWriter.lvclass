﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="14008000">
	<Property Name="EndevoGOOP_ClassItemIcon" Type="Str">BlueFrame</Property>
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">9464978</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">7641720</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">11316396</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">2</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Str"></Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeBaseTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!+W!!!*Q(C=T:3^D2MR%)8@W1&lt;MT&amp;!&amp;.N4#J!;=K!!FSC[?&amp;B2M9)&gt;K96I1V)'3+W";%/!+")=8,(&lt;^,58^XUE/$0B)T@[].RR_)IG63PME@&gt;$DP&gt;&lt;=&lt;)_0@&gt;]XN@.5&lt;`M9`+&lt;:_]X_UOR%@M0YZHR]_:5YD$]:&gt;TX_2&lt;`P^`Y^`HPN_7;\^+@47`ZUV;Z7U^&gt;]X!(J,/0I6`=CY_!@X6X'J&gt;_?_&gt;WVPTJ7[,KO@='P'6XXKE`'KLPJ6`PN_;MTPTS?LX`&lt;(PTB&lt;^;-U`VL&gt;`[*?\H`R7B72`PK`,2H\L6@31]P^]`HH=:XY'@`HZPU)!XXPE]C##&gt;-A^5H?K)H?K)H?K)(?K!(?K!(?K!\OK-\OK-\OK-&lt;OK%&lt;OK%&lt;OK'8DCZUI1O&gt;65EG4S:+CC9&amp;EG21F&amp;Q3HI1HY5FY?&amp;8#E`!E0!F0QE/+%J[%*_&amp;*?")?BCHB38A3HI1HY;&amp;5)=H3U?&amp;*?#CPA#@A#8A#HI#(+28Q"!$":%(BI!A9#MTA)?!*?!)?(B8Q"$Q"4]!4]'!LY!FY!J[!*_"B3&amp;G6+$2$2Y?(-H*Y("[(R_&amp;R?#ANB]@B=8A=(I?([?4Q/$Q/B$/B5RQ%/9/="/@&amp;Y8&amp;YO-HB=8A=(I@(Y=%K/_2F:1;;I;0$9`!90!;0Q70Q5%)'D]&amp;D]"A]"A^F:@!90!;0Q70Q-*5-(I0(Y$&amp;!D%G:8E9R9[#2:!A'$\^S7KTM5B13+VWKBV@V5+I?.N6$J(IY6$&gt;&gt;&gt;4.6.UFV]658687R6"&gt;"^=_J1KP#K%[C/HB)V*&lt;LBFA43W*"T)E:-3('R'A9_I]4N^ON.JO.VOOVFMOF&amp;IO&amp;ZP/Z:L/:*J/*RO/R2K/2^OUL@&gt;=?BO`3]0#D\\P81K@N`:0U_&lt;@UP;V,`V@RCQG?^98\R`Z*$]03@4ON_A;_D8KHX@WQ2H]!XO+&gt;'1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.7</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"N,5F.31QU+!!.-6E.$4%*76Q!!&amp;P1!!!28!!!!)!!!&amp;N1!!!!8!!!!!2*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!!!E"1!A!!!-!!!#!!!!!!!!!1!!Q!]!,Q!(U#!!A!!!!!"!!%!"P````]!!!!!!!!!!!!!!!#S[&lt;#GKA]F4Z16-I*P38WH!!!!$!!!!"!!!!!!MMNA3IT=N5[_WL#YJJ9/&amp;^1&gt;D.G0!,)%[9!*G/TY1HY!!"!!!!!!!&amp;?-`0"S&lt;HZ+K#U^3%GM=&amp;Y"!!!!`````Q!!!"!DMKY-\AGHFNIP\CIX&amp;9&lt;I!!!!"!!!!!!!!!#/!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF$1Q!!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!"`Q!!!!%!!1!!!!!!"Q!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!A!#!!!!!!!H!!!!)(C=9_"C9'ZAO-!!R)Q-4!V-'E$7"Q9'$A9/!2"G!!"C#A5,!!!!!%E!!!%9?*RD9-!%`Y%!3$%S-$$&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?SQI3"\NY$J*F!=F!V("!JJAN!@!,&gt;((YI01.*$!#1YSD6!!!!!!!!$!!"6EF%5Q!!!!!!!Q!!!?)!!!.%?*S.5T&amp;,(%%5@D-:&gt;%*7&gt;M5F8*&amp;#S":(E'$Q#!'PO/!+*VT)39Y1$+3)'B2%5[3YQG*VP7):LADY!Q,Z$W*ZZQJLE3[`)+3ZYIJ!)&amp;8A]LWZ82#*[-$/.W`?_^Z]X]\OP#4;8HDW^!PQDC$S+;,VP9X.+5*-YT&amp;8,$#7=HT33SW_%O-Y(DIG$,RY0SB*N_.DRT1#0WE%+F("Q&amp;;=/[GJ\W40KRQ:D@UM$$1(1-5Y7NXCKJ_WHY&gt;_PIH;/#""(;*:I!)_'&amp;-&gt;JATOW&gt;:?GGO*BS6&lt;Q&gt;Q'')X!Q^I";E/*1-\JVB6#"WE&gt;^Z8.V.2FJ:W?'`&lt;C]R,X4(/`44"VV(Z)\J'S^N@3,+LEZAPC7EL@K8A@XD7+#^/$OY8SLT@I^B;HX?-TNV-"I&gt;N531M?6BF9O&amp;GZKPWYZZZ=H,)U&gt;\H0.NS4-_PGN^7FU&gt;`L`V$G)-+&gt;&gt;T`TD#UH;?F3&amp;OKZ_[A3&amp;SQ]=9K&lt;U_F[&lt;P5V.`A%=25WJ(D8VJC*L&amp;:_AT#L_75,7\]MN/PMO@:R(M?=]I2O\V)[`$-3QV%QU]S`MU@U``%"4Z5%?7CW!VQ"0A:_%]S&lt;IFEZTI@!-J!&amp;4:+U8%';`AIJ8]C7@#P@SWWZ+[]ZZF:D[77Y@$EO`IN`GXSFQQ!!!!!!%Q!!!!FYH'.A9'"E:!!#!!!5!!-!!!!!$B1"A#=!!!9R.#YQ,D%!!!!!!!!-&amp;!#!!!!!"$%U,D!!!!!!$B1"A#=!!!9R.#YQ,D%!!!!!!!!-&amp;!#!!!!!"$%U,D!!!!!!$B1"A#=!!!9R.#YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!@````H````Z````_@````H````Z````_@````A!!!!!!!!!!!!!!!!!$Q!!!$`!!!$`]!!$``Q!!``]!!0``!!$``Q!!``]!!0``!!$``Q!!``]!!0``!!$``Q!!```A!(``_!!@`_!!"``!!!(`!!!!0!!!!!!!!!!!!!!!!A$&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;``X^`@X``&gt;`@`^``X``&gt;X@X&gt;`@X^`&gt;`@X@X@X&gt;`&gt;`&gt;X`X@X^`@`^X^X^X`X@`^X&gt;`&gt;X@X^X^X^`&gt;`&gt;`&gt;X^X^X@X&gt;X^`&gt;`&gt;`@X@X@`^`&gt;`&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X=T-T-T-T-T-T-T-T-T-T.X-T-T-T-T-T-T-T-T-T-T&gt;T-T-T-T-T``]T-T-T-T-X=T-T-T-T`X&gt;X`T-T-T-T.X-T-T-T`X&gt;X&gt;X@`-T-T-T&gt;T-T-T`X&gt;X&gt;X&gt;X&gt;`]T-T-X=T-T-`^X&gt;X&gt;X&gt;X@`-T-T.X-T-T0``X&gt;X&gt;X@``T-T-T&gt;T-T-T````&gt;X@```]T-T-X=T-T-```````````-T-T.X-T-T0``````````T-T-T&gt;T-T-T``````````]T-T-X=T-T-```````````-T-T.X-T-T0``````````T-T-T&gt;T-T-T``````````]T-T-X=T-T-`````````````-T.X-T-T-`````````````]T&gt;T-T-T-T``````````]T-X=T-T-T-T0```@````T-T.X-T-T-T-T-`@````T-T-T&gt;T-T-T-T-T-T0``T-T-T-X=T-T-T-T-T-T-T-T-T-T.X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X&gt;X1!!"!"=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN=80```XP`?`^\`XP```^\?`^\````?````XP```^\?VR=`XN\?`^\`XP`?`^\?`^\`XN\`XN\`XN\?`^\?`^\8&amp;T``XN\`XP`?`^\````?XP`?XP`?XP``XN\````?XN=80^\?XN\`XP`?XP`?XP`?`^\?`^\?`^\?XP`?XP`?VR=`XN\?XP`?`^\?`^\?`^\`XN\`XN\````?`^\?`^\8&amp;R\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN\?XN=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY8&amp;TY_0DY_0DY_0DY_0DY_&amp;"1_0DY_0DY_0DY_0DY_0B=80DY_0DY_0DY_0DY_&amp;#M`+SM50DY_0DY_0DY_0DY_&amp;R=_0DY_0DY_0DY_&amp;#M`(N\?XOML&amp;$Y_0DY_0DY_0DY8&amp;TY_0DY_0DY_&amp;#M`(N\?XN\?XN\L+R1_0DY_0DY_0B=80DY_0DY_0CM`(N\?XN\?XN\?XN\?[SM_0DY_0DY_&amp;R=_0DY_0DY_0T]?XN\?XN\?XN\?XN\`KTY_0DY_0DY8&amp;TY_0DY_0DY`+SM`(N\?XN\?XN\`P\_`0DY_0DY_0B=80DY_0DY_0D]L+SML0R\?XN\`P\_`P\]_0DY_0DY_&amp;R=_0DY_0DY_0SML+SML+T]L0\_`P\_`PTY_0DY_0DY8&amp;TY_0DY_0DY`+SML+SML+T_`P\_`P\_`0DY_0DY_0B=80DY_0DY_0D]L+SML+SML0\_`P\_`P\]_0DY_0DY_&amp;R=_0DY_0DY_0SML+SML+SM`P\_`P\_`PTY_0DY_0DY8&amp;TY_0DY_0DY`+SML+SML+T_`P\_`P\_`0DY_0DY_0B=80DY_0DY_0D]L+SML+SML0\_`P\_`P\]_0DY_0DY_&amp;R=_0DY_0DY_+SML+SML+SM`P\_`P\_L+SML+TY_0DY8&amp;TY_0DY_0DY_0T]L+SML+T_`P\_L+T]L+SML+SM_0B=80DY_0DY_0DY_0D]L+SML0\_L+T]L+SML+SM_0DY_&amp;R=_0DY_0DY_0DY_0DY`+SML+R\L+SML+SML0DY_0DY8&amp;TY_0DY_0DY_0DY_0DY_0R\L+SML+SML0DY_0DY_0B=80DY_0DY_0DY_0DY_0DY_0DYL+SML0DY_0DY_0DY_&amp;R=_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY_0DY8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=8&amp;R=!!!!BA!"2F")5!!!!!%!!F2%1U-!!!!"&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!&amp;"53$!!!!!&lt;!!%!!Q!!&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!!!!!A!!`Q!!!!%!!1!!!!!!"Q!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!&amp;V"53$!!!!!!!!!!!!!$!!!!!!5&lt;!!!-S(C=L6&gt;&gt;;"R6&amp;$ZX-EEHS3\/*FG4&lt;2.WMJV.!]H3V&amp;";UY,64IO.)9GG15X1:JP&gt;.+FJIMGG#E)L=9QE%%7,!3V^D4Y)@=C$)"3+,I)-3(V2M/B3K3V#B9$'FN4:]&gt;Q\/TMT_Z=].#($MDH@/???\`POW18Q*-5',AU8&gt;#$C"L\IU;%KJB'!:%3!T%`,!IC$Z#'1OA$2Y26B5,T*J5GD$J[9VC*U+)OQDN('N]9B_*?]+&gt;\#U!IRA-GK&gt;(AMJN8\OO75+&amp;^NF"@,L;Q_;"+83:J\31\?&amp;Z&lt;6;3Q);CN^_C)E$52JZHEVV"_&gt;CKMS@&lt;=S)A29SEI&gt;2%8TTMCJ0:A23X`$5H*N*-1&gt;NF)#JGS&amp;N&lt;5V'_1T13WMD9/))3%!UMGVF=$5+&amp;L4L*RK:ZAKBM%[H69&gt;*:C_2(OHI&amp;RIL;+*#%8=7ZED5[S&amp;UY]5R)XL5#?HHB%#QKX9,]4\P0I6%#$*5Y*RT6CC?&amp;]P:9(&amp;VC!2Y3YC$O$L!2U;6)U&lt;!:\7+I=[7)(0'1W]2=./3M.R2E/FMCC#T=/FUDR)(#D.#J]-.2W&gt;H*N.R'?E[4&amp;J&gt;$)[/SO^0D.R0JK)3\&amp;I)JL0UCZ&amp;#RSA%[!&amp;G5#A(HB9BA_=%Z_'V&gt;66(!)_&lt;7AD1OPF6":8C[E`AI_T5Y`:U[.6\?EVY@4#,U.Y#*1(*]`1/9;\/,&gt;[[WXV3KB?HZD&lt;H%O^I5?D8D&amp;M]I&lt;`!\G69'.BZ[BWI]YOZ!A;FO!]8#YB4NE%W9,'?)$X9;E%*IS9CUZ")W9*-:?X&amp;H2,HK!J.E@1O6QC*8OSF*CSZAER:;X?-(YU\K'YQ7P]"N@A3Y?Y9]A4VJ-9_=:`M!A,D*=SCZ=W3]\+V7#X7]^$7_CZ)K.HM?`UW@BIYOF%9G&lt;C^&amp;QC0JMD9?S^(8N8(HC@S\S$.IP%N"'26V,"&lt;EO8&gt;!AOJ_WFL@7[H'&lt;]"7$A&amp;,@5D"S#@#.VZ"O*R^^4/*/ND,1PRUDU:^2GL&lt;C2HCBAJ$,,3"QT5A^\DNBWWO_SE^7CSUY((PUS/&amp;D)/`-Q&lt;.^L"8TQ:*ZXBB%V#@-F-&amp;VZXBH'/J.7H6,?/:4PH7'L6N9\+SMLO=PA=((PI+!W[',YQPA:^M+HT$M=QQUD+&lt;D!4?(^;@Q.@JBD,/SQ7"ALOAMCW`3/=(RC-NY@49TH])(*T[$E5$X6LGP@#[UQ9JX8],(T&lt;GZOYHHR;3:8G_%&gt;:&amp;GG\W4T?819R\W._&gt;L-8DUUXUZ==4*5G^0S"=W4EX3:F!S;"[G##F7C,T!JBICB4''0%++*CY5;BJ%.L;;BF5'&lt;QQF&amp;][-,C&gt;H(&gt;7NDN),(GNF'-@L0&amp;FBH\2$&lt;QI6^/LQ7USI#A,@.A)/J7BD$U]]T3DFL3O?Q2)W=]DO.\I.':T^M5,T\^$2%9C'O1?FX]Q&gt;6,04/^E0`W(\I&lt;&gt;@YU1J4DAP*=]3]E(C]E":?J4HMO$?=&amp;^@PY&lt;=Z-\3]:;(-NY[6F`AA,[H.B--0O#&amp;0`]FH/\!,HL&lt;#\_&lt;FSP6-JE']X^#UT%.@'`]9&gt;[(4@=PX&amp;@815VF@@V,;2&amp;852#`%R[&lt;GTO6@;`W+VL$@F"N.,2I0I1.W7;FP\]O`HVYUVQ1,L]&amp;M%@"H&amp;8;PW-@.)=?YP0XGL#JQL$D]8M&gt;&gt;+.T]&lt;E[A[ZL_G6%\-/K^OO]\X\V`Z].#U&gt;:X"W&amp;!6IR3!?)R&lt;/7C$H\F;-VV]14^$##=%)[*6X#LY)*Z8,AC&lt;#2`SH[\30:EE?I.]CPZ1@YM+$4]$R.\G]Y!!!!!"!!!!%E!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"F!!!!&gt;8C=9W"A+"3190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT```^8+7,E_(LE'FT2%2]Y5W770)=%!'5)':I!!!!!!!!%!!!!"Q!!!Y)!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=B1!A!!!!!!"!!A!-0````]!!1!!!!!!G!!!!!1!%E"Q!!)+2GFM:6*F:GZV&lt;1!!%E!S`````QB';7RF5'&amp;U;!!!5!$RS4/$^Q!!!!)32GFM:6&gt;S;82F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#"!5!!#!!!!!2"09GJF9X2"&gt;(2S;7*V&gt;'6T!!!=1&amp;!!!1!#%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!!1!$!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=B1!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!Q!!!!!!!!!"!!!!!A!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ&amp;!#!!!!!!!%!"1!(!!!"!!$*-Y0Z!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N="1!A!!!!!!"!!5!"Q!!!1!!S4/$_1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-5!)!!!!!!!1!)!$$`````!!%!!!!!!*A!!!!%!"*!=!!##E:J&lt;'63:7:O&gt;7U!!"*!-P````])2GFM:6"B&gt;'A!!&amp;!!]=ETA`=!!!!#%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!A1&amp;!!!A!!!!%14W*K:7.U182U=GFC&gt;82F=Q!!(%"1!!%!!B*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!%!!Q!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF&amp;!#!!!!!!!%!"1!$!!!"!!!!!!!)!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U921!A!!!!!!%!"*!=!!##E:J&lt;'63:7:O&gt;7U!!"*!-P````])2GFM:6"B&gt;'A!!&amp;!!]=ETA`=!!!!#%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=R209GJF9X2"&gt;(2S;7*V&gt;'6T,G.U&lt;!!A1&amp;!!!A!!!!%14W*K:7.U182U=GFC&gt;82F=Q!!(%"1!!%!!B*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!%!!Q!!!!"16%AQ!!!!"!!!!!!!!!!!!!!!!!!%!!=!$1!!!!1!!!"R!!!!+!!!!!)!!!1!!!!!%A!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&amp;(!!!#@XC=H:",3].1%)7`0'I@VFJL@62&lt;C"M8,LKQ?YE)X6K+Y.;U3413&lt;5FOCEP"8_?`U:7YUEE;521N?!=O&gt;]\-H(PG!"U;^B3^UA^#&lt;_DZN]E.ABS_S3GFW-"26T$A_&lt;(X]!,ID21^DQ,F2&gt;VQ.A[&gt;/'[?DK[^M4J7+AJ'C@,C\FC&amp;709!83;U_P=SH'6]L\`QV&lt;Z!+&gt;/"-'HIH:-QC17T*L[6^6H4+*AZSL.=2TE5*+DASI^0G*)9?5K0*=JW%=-0,SH:^]:EK/;!.`'J#\N"C4)6)X&amp;^[25`*-]]Y/A@G^&gt;).[_S1D648D05H3N;5H3:6&gt;J`/,/@4;QNX&amp;8G#S*6,AU2?S&amp;U*HXZQG+&gt;JLT4W*#/DT$T_)F]6D;TG*OGM:7,773]#@FS[:4*HCD1R&gt;)#2?IUW+;6VX&lt;EV7+8&gt;CI;`2VI5YFZ!!!!!(=!!1!#!!-!"1!!!&amp;A!$Q1!!!!!$Q$9!.5!!!"B!!]%!!!!!!]!W!$6!!!!;A!0"!!!!!!0!.A!V1!!!(/!!)1!A!!!$Q$9!.5!!!"VA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!"-1"35V*$$1I!!UR71U.-1F:8!!!7^!!!"&amp;=!!!!A!!!7V!!!!!!!!!!!!!!!)!!!!$1!!!2)!!!!(5R*1EY!!!!!!!!"&lt;%R75V)!!!!!!!!"A&amp;*55U=!!!!!!!!"F%.$5V1!!!!!!!!"K%R*&gt;GE!!!!!!!!"P%.04F!!!!!!!!!"U&amp;2./$!!!!!!!!!"Z%2'2&amp;-!!!!!!!!"_%R*:(-!!!!!!!!#$&amp;:*1U1!!!!!!!!#)%&gt;$2%E!!!!!!!!#.(:F=H-!!!!%!!!#3&amp;.$5V)!!!!!!!!#L%&gt;$5&amp;)!!!!!!!!#Q%F$4UY!!!!!!!!#V'FD&lt;$1!!!!!!!!#['FD&lt;$A!!!!!!!!#`%R*:H!!!!!!!!!$%%:13')!!!!!!!!$*%:15U5!!!!!!!!$/&amp;:12&amp;!!!!!!!!!$4%R*9G1!!!!!!!!$9%*%3')!!!!!!!!$&gt;%*%5U5!!!!!!!!$C&amp;:*6&amp;-!!!!!!!!$H%253&amp;!!!!!!!!!$M%V6351!!!!!!!!$R%B*5V1!!!!!!!!$W&amp;:$6&amp;!!!!!!!!!$\%:515)!!!!!!!!%!!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!(!!!!!!!!!!!`````Q!!!!!!!!#Q!!!!!!!!!!$`````!!!!!!!!!-1!!!!!!!!!!0````]!!!!!!!!!T!!!!!!!!!!!`````Q!!!!!!!!&amp;A!!!!!!!!!!$`````!!!!!!!!!7A!!!!!!!!!!0````]!!!!!!!!"F!!!!!!!!!!!`````Q!!!!!!!!(E!!!!!!!!!!$`````!!!!!!!!!@1!!!!!!!!!!0````]!!!!!!!!$X!!!!!!!!!!%`````Q!!!!!!!!0U!!!!!!!!!!@`````!!!!!!!!"!A!!!!!!!!!#0````]!!!!!!!!%'!!!!!!!!!!*`````Q!!!!!!!!1M!!!!!!!!!!L`````!!!!!!!!"$Q!!!!!!!!!!0````]!!!!!!!!%5!!!!!!!!!!!`````Q!!!!!!!!2I!!!!!!!!!!$`````!!!!!!!!"(Q!!!!!!!!!!0````]!!!!!!!!&amp;!!!!!!!!!!!!`````Q!!!!!!!!=%!!!!!!!!!!$`````!!!!!!!!#QA!!!!!!!!!!0````]!!!!!!!!,F!!!!!!!!!!!`````Q!!!!!!!"#U!!!!!!!!!!$`````!!!!!!!!%,Q!!!!!!!!!!0````]!!!!!!!!1R!!!!!!!!!!!`````Q!!!!!!!"$5!!!!!!!!!!$`````!!!!!!!!%5!!!!!!!!!!!0````]!!!!!!!!23!!!!!!!!!!!`````Q!!!!!!!"41!!!!!!!!!!$`````!!!!!!!!&amp;.A!!!!!!!!!!0````]!!!!!!!!5Y!!!!!!!!!!!`````Q!!!!!!!"5-!!!!!!!!!)$`````!!!!!!!!&amp;FA!!!!!$E:J&lt;'68=GFU:8)O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2*';7RF6X*J&gt;'6S,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!1!"!!!!!!!(!!!!!!1!%E"Q!!)+2GFM:6*F:GZV&lt;1!!%E!S`````QB';7RF5'&amp;U;!!!5!$RS4/$^Q!!!!)32GFM:6&gt;S;82F=CZM&gt;G.M98.T&amp;%^C;G6D&gt;%&amp;U&gt;(*J9H6U:8-O9X2M!#"!5!!#!!!!!2"09GJF9X2"&gt;(2S;7*V&gt;'6T!!"5!0(*-Y0Z!!!!!B*';7RF6X*J&gt;'6S,GRW9WRB=X-/2GFM:6&gt;S;82F=CZD&gt;'Q!+E"1!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!"`````Q!!!!"16%AQ!!!!"!!!!!!!!!!!!!!"&amp;%^V&gt;("V&gt;&amp;.U=G6B&lt;3ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!1!)!!!!!!!!!!!!!!!1!!!"*';7RF5G6B:'6S,GRW9WRB=X-</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"+!!!!!220&gt;82Q&gt;824&gt;(*F97UO&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!+!!"!!1!!!R0&gt;82Q&gt;824&gt;(*F97U54X6U=(6U5X2S:7&amp;N,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="FileWriter.ctl" Type="Class Private Data" URL="FileWriter.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ObjectAttributes.ctl" Type="VI" URL="../ObjectAttributes.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="protected" Type="Folder">
		<Item Name="Open_Impl.vi" Type="VI" URL="../Open_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)R!!!!$A!M1(!!(A!!&amp;"*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!'N!&amp;A!'"'^Q:7Y(=G6Q&lt;'&amp;D:1:D=G6B&gt;'5/&lt;X"F&lt;C"P=C"D=G6B&gt;'52=G6Q&lt;'&amp;D:3"P=C"D=G6B&gt;'5D=G6Q&lt;'&amp;D:3"P=C"D=G6B&gt;'5A&gt;WFU;#"D&lt;WZG;8*N982J&lt;WY!!!FP='6S982J&lt;WY!,U!7!!-+=G6B:#^X=GFU:1FS:7&amp;E,7^O&lt;(E+&gt;X*J&gt;'5N&lt;WZM?1!'97.D:8.T!!!31$$`````#'BP=X2/97VF!!!,1!9!"("P=H1!!!V!!Q!(&gt;'FN:7^V&gt;!!%!!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!&amp;E"1!!-!"Q!)!!E):8*S&lt;X)A;7Y!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!71&amp;!!!Q!(!!A!#1FF=H*P=C"P&gt;81!N!$Q!"Q!!!!"!!)!!Q!%!!5!"A!+!!9!"A!'!!9!"A!'!!9!"A!'!!9!"A!'!!M!"A!'!!9!"A!'!!9!$!-!!2A!!*)!!!!)!!!!#!!!!AA!!!!)!!!!#!!!!!!!!!!+!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!U(!!!!!1!.!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Close_Impl.vi" Type="VI" URL="../Close_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%8!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="U8_Impl.vi" Type="VI" URL="../U8_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%A!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"1!#64A!!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Int32_Impl.vi" Type="VI" URL="../Int32_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%C!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!!Q!&amp;37ZU-T)!,%"Q!"Y!!"132GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!.2GFM:6*F972F=C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!+!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">44040208</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="String_Impl.vi" Type="VI" URL="../String_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%H!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!""!-0````]'5X2S;7ZH!!!M1(!!(A!!&amp;"*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="ByteArray_Impl.vi" Type="VI" URL="../ByteArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!5!"1!!)%"!!!(`````!!=4&gt;7ZT;7&gt;O:71A9HFU:3"B=H*B?1!M1(!!(A!!&amp;"*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF5G6B:'6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#!!*!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!AI!!!#1!!!!!!%!#A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="Double64_Impl.vi" Type="VI" URL="../Double64_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%G!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)2'^V9GRF-T)!!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
		<Item Name="WaveformArray_Impl.vi" Type="VI" URL="../WaveformArray_Impl.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;"!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'63:7&amp;E:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!""!6!!$#(&gt;B&gt;G6G&lt;X*N!!!;1%!!!@````]!"QV898:F:G^S&lt;5&amp;S=G&amp;Z!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'63:7&amp;E:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782544</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		</Item>
	</Item>
	<Item Name="public" Type="Folder">
		<Item Name="SetGroupName.vi" Type="VI" URL="../SetGroupName.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'68=GFU:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-0````]*2X*P&gt;8"/97VF!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'68=GFU:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!##!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="SetProperties.vi" Type="VI" URL="../SetProperties.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'68=GFU:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!1!5Q!=1%!!!@````]!"QZ1=G^Q:8*U?6:B&lt;(6F=Q!!#!!Q`````Q!;1%!!!@````]!#1V1=G^Q:8*U?5ZB&lt;76T!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'68=GFU:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!#!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!))!!!##!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1117782528</Property>
		</Item>
		<Item Name="IsOpen.vi" Type="VI" URL="../IsOpen.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B';7RF4X"F&lt;A!!,E"Q!"Y!!"132GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!/2GFM:6&gt;S;82F=C"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!,%"Q!"Y!!"132GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!.2GFM:6&gt;S;82F=C"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714386</Property>
		</Item>
		<Item Name="FileExists.vi" Type="VI" URL="../FileExists.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"R!)2:G;7RF)'^S)':P&lt;'2F=C"F?'FT&gt;(-`!!!O1(!!(A!!&amp;"*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!Z';7RF6X*J&gt;'6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!M1(!!(A!!&amp;"*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!V';7RF6X*J&gt;'6S)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
		<Item Name="GetFilePath.vi" Type="VI" URL="../GetFilePath.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-P````])2GFM:6"B&gt;'A!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'68=GFU:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'68=GFU:8)A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="SetFilePath.vi" Type="VI" URL="../SetFilePath.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'68=GFU:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-P````])2GFM:6"B&gt;'A!!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'68=GFU:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="GetFileSize.vi" Type="VI" URL="../GetFileSize.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%M!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"!!0=WF[:3!I;7YA9HFU:8-J!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'68=GFU:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'68=GFU:8)A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342714384</Property>
		</Item>
		<Item Name="GetFileReference.vi" Type="VI" URL="../GetFileReference.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!=!!##E:J&lt;'63:7:O&gt;7U!!#Z!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$E:J&lt;'68=GFU:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!#R!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!$5:J&lt;'68=GFU:8)A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="FileWriter_Create.vi" Type="VI" URL="../FileWriter_Create.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;`!!!!$!!I1(!!(A!!&amp;"*';7RF6X*J&gt;'6S,GRW9WRB=X-!!!FP9GJF9X1A;7Y!+%"Q!"Y!!"=628BF9X6U;7^O1G&amp;T:3ZM&gt;G.M98.T!!:198*F&lt;H1!!"*!-P````])2GFM:6"B&gt;'A!!""!)1N1=G6Q:7ZE5WF[:1!%!!!!$%!B"H.U982V=Q!!#U!$!!2D&lt;W2F!!!11$$`````"H.P&gt;8*D:1!!&amp;E"1!!-!"1!'!!=):8*S&lt;X)A;7Y!!#J!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!#G^C;G6D&gt;#"P&gt;81!!":!5!!$!!5!"A!(#76S=G^S)'^V&gt;!#%!0!!&amp;!!!!!%!!A!$!!1!#!!%!!1!"!!%!!1!"!!%!!1!#1!%!!1!"!!%!!I$!!%1!!#3!!!!#A!!!!A!!!!)!!!!!!!!!!I!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!$15!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="Destroy.vi" Type="VI" URL="../Destroy.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%0!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!#J!=!!?!!!5%E:J&lt;'68=GFU:8)O&lt;(:D&lt;'&amp;T=Q!!#G^C;G6D&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!+%"Q!"Y!!"132GFM:6&gt;S;82F=CZM&gt;G.M98.T!!!*&lt;W*K:7.U)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!#1!!!!!!!!!!!!!!C1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">44044304</Property>
		</Item>
	</Item>
</LVClass>
